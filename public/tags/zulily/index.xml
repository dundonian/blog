<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Zulily on Residual Analysis - Jason Gowans blog</title>
    <link>http://jasongowans.net/tags/zulily/index.xml</link>
    <description>Recent content in Zulily on Residual Analysis - Jason Gowans blog</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <atom:link href="http://jasongowans.net/tags/zulily/index.xml" rel="self" type="application/rss+xml" />
    
    <item>
      <title>Zulily - Out of Site, Out of Mind?</title>
      <link>http://jasongowans.net/post/zulily-out-of-site-out-of-mind/</link>
      <pubDate>Fri, 13 Feb 2015 06:32:07 +0000</pubDate>
      
      <guid>http://jasongowans.net/post/zulily-out-of-site-out-of-mind/</guid>
      <description>&lt;p&gt;A few days ago, Zulily released its &lt;a href=&#34;http://investor.zulily.com/releasedetail.cfm?ReleaseID=895974&#34;&gt;Fourth Quarter and 2014 Full Year Financial Results&lt;/a&gt;.&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Q4 net sales were up 52% year over year to $391.3 million&lt;/li&gt;
&lt;li&gt;Q4 EBITDA $20.3 million&lt;/li&gt;
&lt;li&gt;Active customers were up 54% to 4.9 million * Active is defined as bought &amp;gt;=1 in the trailing 12 months&lt;/li&gt;
&lt;li&gt;Average Order Value of $58 (up 3% YoY)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;Problem is, they missed expectations on both top line revenue and earnings per share. On the latter metric, they missed estimates by more than 40%. During the earnings call, they cited increased churn among more recent customer cohorts. That same day, &lt;a href=&#34;http://www.geekwire.com/2015/wobbly-business-execution-recent-months-zulily-cfo-marc-stolzman-announces-departure-following-q4-earnings-miss/&#34;&gt;they announced their CFO was departing&lt;/a&gt; amid admissions of wobbly execution.&lt;/p&gt;

&lt;blockquote&gt;&#34;We have been growing so fast that, at times, we’ve let execution be more wobbly than it should be...&#34; - Mark Vadon, Co-Founder Zulily&lt;/blockquote&gt;

&lt;p&gt;Gelatinous execution aside, all of this got me thinking. Arguably, the Zulily storefront is not actually Zulily.com. Instead, their storefront is actually a daily customer email. In that construct, you can think of someone&amp;rsquo;s Gmail inbox as being analogous to a mall or downtown core with the promotions tab the least trafficked bit of downtown. Not on the main street. Still downtown but a few blocks away such that serendipitous footfall is the exception rather than the rule.&lt;/p&gt;

&lt;p&gt;As we&amp;rsquo;re seeing the great fragmentation of online retail play out with a proliferation of category challengers competing against traditional multi-category retailers and brands going direct to consumer, one effect is that we&amp;rsquo;re being saturated with more communication, email especially. One way to compete against the noise is to personalize the communication in the hope of being more relevant, but if Zulily is seeing more churn among customer cohorts, there&amp;rsquo;s just that much less data on a given customer with which to personalize and there&amp;rsquo;s only so much cute algorithms can accomplish.&lt;/p&gt;

&lt;p&gt;In such a competitive environment, it&amp;rsquo;s little wonder we&amp;rsquo;re seeing more retailers and brands that got their start online, now find ways to have a presence in those physical downtown shopping centers either by opening their own stores, having pop-up stores like Warby Parker&amp;rsquo;s &lt;a href=&#34;http://blog.warbyparker.com/bulletin-from-the-bus-seattle-part-1/&#34;&gt;bus&lt;/a&gt;, or partnering with larger retailers a la &lt;a href=&#34;http://press.nordstrom.com/phoenix.zhtml?c=211996&amp;amp;p=irol-newsarticle&amp;amp;ID=1682596&#34;&gt;Bonobos&lt;/a&gt;.&lt;/p&gt;

&lt;p&gt;Downtown Seattle is and always will be composed of (&lt;a href=&#34;http://www.businessinsider.com/wikipedia-comprised-of-bryan-henderson-wikignome-2015-2&#34;&gt;not comprised of&lt;/a&gt;) not much more than four dense blocks of retail with a less trafficked outer perimeter. When people are out for lunch, walking to work, going to the movies, or grabbing a coffee, they simply cannot ignore the Sephora at Westlake, Banana Republic on 5th, Nike on Pike Street, etc. It&amp;rsquo;s stating the obvious, but those storefronts occupy space that no other retailer can occupy simultaneously. That&amp;rsquo;s not true of our email inbox, however. It can expand to accommodate more or less an infinite number of promotional emails and thus, for an online-only retailer to capture our attention on a consistent basis via their email storefront, it&amp;rsquo;s asking a lot.&lt;/p&gt;

&lt;p&gt;Might we eventually see Zulily roll the dice and try to establish a physical presence, or is this just a temporary case of out of site, out of mind?&lt;/p&gt;
</description>
    </item>
    
  </channel>
</rss>