<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Cloud Computing on Residual Analysis - Jason Gowans blog</title>
    <link>http://jasongowans.net/tags/cloud-computing/index.xml</link>
    <description>Recent content in Cloud Computing on Residual Analysis - Jason Gowans blog</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <atom:link href="http://jasongowans.net/tags/cloud-computing/index.xml" rel="self" type="application/rss+xml" />
    
    <item>
      <title>Old Habits Die Hard</title>
      <link>http://jasongowans.net/post/old-habits-die-hard/</link>
      <pubDate>Sun, 08 Feb 2015 07:17:09 +0000</pubDate>
      
      <guid>http://jasongowans.net/post/old-habits-die-hard/</guid>
      <description>&lt;p&gt;Last Fall, I attended the &lt;a href=&#34;https://reinvent.awsevents.com/&#34;&gt;AWS Re:Invent&lt;/a&gt; conference along with 13,000 other techies from around the world. During the event, Amazon went to great lengths to showcase the very large enterprises and government organizations that have adopted cloud computing.&lt;/p&gt;

&lt;p&gt;Indeed, a good proportion of attendees were on the infrastructure teams of those large corporations, responsible, in part, for figuring out this cloud computing thing, and ultimately transitioning their company&amp;rsquo;s infrastructure onto AWS.&lt;/p&gt;

&lt;p&gt;Undoubtedly these corporations will benefit in the long-run from elastic computing resources, but what could throttle those benefits are old processes that are being ported over to the new environment. For example, as an application developer in a large corporation, the typical process for starting a new project is to submit a ticket, wait for an infrastructure engineer / architect to be assigned to your project, discuss your needs, do some capacity planning, provision a virtual machine or buy some servers, maybe give you root access, but more likely, have you tell them what you need installed, they&amp;rsquo;ll take care of it for you, and eventually your environment is ready to go after a few back and forths. I&amp;rsquo;ve personally seen this end to end process take more than 100 days&amp;hellip;&lt;/p&gt;

&lt;p&gt;The cloud is supposed to end that inefficient process though, right? Well, not necessarily. While it&amp;rsquo;s true that the corporation will probably reduce their infrastructure costs by moving to the cloud, it&amp;rsquo;s not a guarantee that the newly empowered application developer will be able to move so much faster. I mean, there are only so many devs that know how to set up a cloud computing environment.&lt;/p&gt;

&lt;p&gt;&lt;a href=&#34;https://jasongowansdotnet.files.wordpress.com/2015/02/aws-console.png&#34;&gt;&lt;img src=&#34;https://jasongowansdotnet.files.wordpress.com/2015/02/aws-console.png?w=584&#34; alt=&#34;aws-console&#34; /&gt;&lt;/a&gt;&lt;/p&gt;

&lt;p&gt;For example, expecting a front-end developer whose skills lie with HTML, CSS, and JavaScript to now learn the myriad AWS tools to deploy their apps is simply not realistic. In fact, as Amazon continues to innovate, it&amp;rsquo;s likely the role of AWS architect will become ever more specialized - the AWS security architect, the AWS networking architect, the AWS analytics architect, the AWS database architect, the AWS storage architect, etc.&lt;/p&gt;

&lt;p&gt;So now, we&amp;rsquo;re right back to where we started where an application developer needs an environment, but they don&amp;rsquo;t know the whole AWS stack, and they&amp;rsquo;re now working with their company&amp;rsquo;s AWS architect whose job now is to provision AWS resources instead of the old on-prem resources they used to do. Granted, ordering the server has been cut out of the equation but the flow could be better. Much better.&lt;/p&gt;

&lt;p&gt;Over the next few years, we&amp;rsquo;re going to see more startups focus on specialized areas of the typical enterprise IT department to provide simplified, abstracted AWS-based services that will be composed of multiple AWS tools. Of course, AWS themselves are doing this to some extent but right now their major opportunities lie simply in getting these huge enterprises onto the cloud. As they further penetrate the mid-market, those abstracted services will become even more important where the &amp;ldquo;IT department&amp;rdquo; can sometimes be just a handful of folks.&lt;/p&gt;

&lt;p&gt;In the meantime, corporate IT infrastructure teams would be wise to think not only about ditching their on-prem servers, but how the developers within their organization will actually provision and use those cloud services. At &lt;a href=&#34;http://www.aerobatic.com/&#34;&gt;Aerobatic&lt;/a&gt;, the company I co-founded with &lt;a href=&#34;https://github.com/dvonlehman&#34;&gt;this guy&lt;/a&gt;, we spend a lot of time thinking about how we can make the lives of front-end developers easy, but that&amp;rsquo;s only half the story. The other half is thinking about how to make the lives of the infrastructure architect easy too, such that, in the end, everyone&amp;rsquo;s happy, and those build-deploy-test cycles can keep pace with the business.&lt;/p&gt;
</description>
    </item>
    
  </channel>
</rss>