<!DOCTYPE html>
<html lang="en-us">
<head>
<meta charset="utf-8">
<meta name="generator" content="Hugo 0.18" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="//fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css">
<link href='//fonts.googleapis.com/css?family=Raleway:400,300,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.4/styles/github.min.css">
<link rel="stylesheet" href="/css/normalize.css">
<link rel="stylesheet" href="/css/skeleton.css">
<link rel="stylesheet" href="/css/custom.css">
<link rel="alternate" href="/index.xml" type="application/rss+xml" title="Residual Analysis - Jason Gowans blog">
<title>The Paradox of Modern Web App Deployment - Residual Analysis - Jason Gowans blog</title>


<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="/js/site.js"></script>
</head>
<body>




	<div class="navbar-spacer"></div>
    <nav class="navbar">
      <div class="container">
        <ul class="navbar-list-left">
          <li class="navbar-item">
          	<div class="header-logo">
				<a href="/"><img class="header-logo" src="/images/jg.jpg" width="60" height="60" alt="Residual Analysis - Jason Gowans blog"></a>
			</div>
		  </li>
		  <li class="navbar-item navbar-title"><h1>Residual Analysis - Jason Gowans Blog</h1></li>
		</ul>
		<ul class="navbar-list-right">
          <li class="navbar-item"><a class="navbar-link" href="/page/about">About</a></li>
          <li class="navbar-item"><a class="navbar-link" href="/">Home</a></li>
        </ul>
      </div>
    </nav>
    <div class="container">



	<main role="main">
		<article itemscope itemtype="http://schema.org/BlogPosting">
			<h1 class="entry-title" itemprop="headline">The Paradox of Modern Web App Deployment</h1>
			<span class="entry-meta"><time itemprop="datePublished" datetime="2015-02-14">February 14, 2015</time></span>
			<section itemprop="entry-text">
				<p>In the world of web development, such is the pace of innovation that barely a week goes by without an announcement of a new JavaScript framework. At the time of writing, there are more than 1,500 job descriptions in the U.S. on LinkedIn containing the word “AngularJS” and 750 containing the word “Ember.” Major tech companies have been at the vanguard of the single page app adoption curve, but increasingly, we’re now seeing large corporations embrace this trend be it Angular, Ember, or more recently React.</p>

<p>So, it comes as something of a surprise that while we’re seeing widespread adoption of modern web app development, there’s not more innovation when it comes to deployment of those web apps.</p>

<p>Indeed, the paradox of deploying these web apps is that most deployment options today are both overkill and inadequate at the same time. They’re overkill in terms of both price and complexity since many still entail provisioning an entire virtual machine for what amounts to mostly serving static content, and they’re inadequate with regards to the features endemic to this style of development. I would contend that this holds true whether one&rsquo;s talking about deploying internally within an enterprise, or externally using a third party service.</p>

<p>For public deployment, Heroku remains a default choice for many web developers. However, while that choice made perfect sense 5 years ago when everyone was building end to end rails apps with Postgres backends, that’s increasingly not the situation for most web apps being built today. Indeed, with the availability of BaaS such as Firebase and Parse, as well as the proliferation of developer APIs for everything, a modern front-end developer can now build full-featured apps without ever needing to roll any server-side code of their own.</p>

<p>So, if we don’t need VMs for our web apps, what are the choices? Well, more enterprising developers can deploy to an <a href="http://docs.aws.amazon.com/gettingstarted/latest/swh/website-hosting-intro.html">AWS S3 bucket</a> or <a href="https://pages.github.com/">Github Pages</a> can be a good choice for quick public projects, and best of all it’s free. However, with Single Page Apps on S3 or Github Pages, there’s a common set of features that devs need that either aren’t available or come at a cost of increased complexity such as SEO snaphots, API proxy, OAuth, blue/green deployments, and more.</p>

<p>For public deployments, <a href="www.aerobatic.com">Aerobatic</a> (the company where I&rsquo;m a co-founder) and <a href="www.divshot.com">Divshot</a> are legitimately focused on these web apps and provide many of those relevant features. For the enterprise though, what choice do they have?</p>

<p>Instead of VMs or scripts to provision infrastructure on AWS for these web apps, would it not be better to rethink the whole application deployment scenario entirely? Meteor is onto something when they imagine a <a href="https://www.meteor.com/blog/2012/07/25/meteors-new-112-million-development-budget">multi-tenant deployment platform</a> that can run inside an enterprise.</p>

<blockquote>"Galaxy will be a product that the operations department at a large company might buy. It'll be an enterprise-grade, multi-tenant hosting environment for Meteor apps. In other words, it'll let you run a private, centrally controlled "meteor deploy"-like service for your company, on your own hardware. You'll be able to manage how your apps are distributed across your datacenters, perform capacity planning, and enforce controls and policies that are appropriate to your organization. Google and Facebook have these tools — why shouldn't your organization?"</blockquote>

<p>However, this presumably would depend on a developer using the full Meteor stack and the reality is that most companies are a heterogeneous mashup of lots of different javascript, services, APIs, and databases. Seems that enterprises would want to have a deployment platform that can play well with whatever frameworks and databases its developers are using. The whole point is that web apps are supposed to be composed of services where the backend can be developed independent of the front-end. <a href="http://www.nczonline.net/blog/2013/10/07/node-js-and-the-new-web-front-end/">Nicholas Zakas talked about that very notion back in 2013</a>.</p>

<p>On Meteor, perhaps Tom Dale said it best in this tweet:</p>

<blockquote>"Meteor as an idea is good, but thinking 1 org can build the best view layer, best data layer, best pkg manager, etc takes a lot of hubris.""
 — Tom Dale (@tomdale) [August 29, 2014](<a href="https://twitter.com/tomdale/status/505175048643440641">https://twitter.com/tomdale/status/505175048643440641</a>)
</blockquote>

<p>And again in a <a href="http://tomdale.net/2015/02/youre-missing-the-point-of-server-side-rendered-javascript-apps/">recent blog post</a>:</p>

<blockquote>"Unfortunately, when most people think of client-JavaScript-running-on-the-server, they think of technologies like Meteor, which _do_ ask you to write both your API server (responsible for things like authentication and authorization) and your client app in the same codebase using the same framework. Personally, I think this approach is a complexity and productivity disaster, but that deserves its own blog post."</blockquote>

<p>So, if not Meteor, then what or who? The company that can upend the world of enterprise application deployment and make it easy for everyone&hellip;well that&rsquo;s one worth building, don&rsquo;t you think? That&rsquo;s the dream we&rsquo;re chasing here at <a href="www.aerobatic.com">Aerobatic</a>. More on that soon.</p>

			</section>
		</article>
		<div id="disqus_thread"></div>
<script type="text/javascript">
    var disqus_shortname = 'jasongowans';
    var disqus_identifier = 'http:\/\/jasongowans.net\/post\/the-paradox-of-modern-web-app-deployment\/';
    var disqus_title = 'The Paradox of Modern Web App Deployment';
    var disqus_url = 'http:\/\/jasongowans.net\/post\/the-paradox-of-modern-web-app-deployment\/';

    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
	</main>


	<footer role="contentinfo">
		<div class="hr"></div>
		<div class="copyright">Copyright &copy; <a href="//twitter.com/jasongowans">Jason Gowans</a> </div>
	</footer>

</div>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-64063024-1', 'auto');
	ga('send', 'pageview');
</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.4/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

</body>
</html>
