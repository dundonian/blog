<!DOCTYPE html>
<html lang="en-us">
<head>
<meta charset="utf-8">
<meta name="generator" content="Hugo 0.18" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="//fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css">
<link href='//fonts.googleapis.com/css?family=Raleway:400,300,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.4/styles/github.min.css">
<link rel="stylesheet" href="/css/normalize.css">
<link rel="stylesheet" href="/css/skeleton.css">
<link rel="stylesheet" href="/css/custom.css">
<link rel="alternate" href="/index.xml" type="application/rss+xml" title="Residual Analysis - Jason Gowans blog">
<title>Measuring Brand Health in 2016 - Residual Analysis - Jason Gowans blog</title>


<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="/js/site.js"></script>
</head>
<body>




	<div class="navbar-spacer"></div>
    <nav class="navbar">
      <div class="container">
        <ul class="navbar-list-left">
          <li class="navbar-item">
          	<div class="header-logo">
				<a href="/"><img class="header-logo" src="/images/jg.jpg" width="60" height="60" alt="Residual Analysis - Jason Gowans blog"></a>
			</div>
		  </li>
		  <li class="navbar-item navbar-title"><h1>Residual Analysis - Jason Gowans Blog</h1></li>
		</ul>
		<ul class="navbar-list-right">
          <li class="navbar-item"><a class="navbar-link" href="/page/about">About</a></li>
          <li class="navbar-item"><a class="navbar-link" href="/">Home</a></li>
        </ul>
      </div>
    </nav>
    <div class="container">



	<main role="main">
		<article itemscope itemtype="http://schema.org/BlogPosting">
			<h1 class="entry-title" itemprop="headline">Measuring Brand Health in 2016</h1>
			<span class="entry-meta"><time itemprop="datePublished" datetime="2016-12-30">December 30, 2016</time></span>
			<section itemprop="entry-text">
				

<p>For the past three years, I&rsquo;ve been perfectly spoiled working on personalization on Nordstrom.com. Everything we built could be easily A/B tested, and impact quantified and understood.</p>

<p>Now that I&rsquo;m focused entirely on Marketing, I&rsquo;ve found myself having to think not just about direct response tactics, but also longer term branding campaigns. Seeking to measure our &ldquo;Brand Health,&rdquo; I naively thought we could just continuously measure this stuff, run some geo tests et voila, I&rsquo;d come out the other end having some pithy quantitative perspective like, &ldquo;&hellip;every 1% lift in brand health translates to a .1% lift in EBIT,&rdquo; or something to that effect.</p>

<p>Well, alas, measuring brand health in 2016 isn&rsquo;t quite so simple, at least as far as my research has thus far revealed. So, below are some thoughts on how to approach measuring brand health. Feedback welcomed!</p>

<h2 id="what-should-we-measure">What should we measure?</h2>

<p>Some might say that <a href="https://www.netpromoter.com/know/">Net Promoter Score (NPS)</a> is the definitive way to measure brand health. Its simplicity to calculate has no doubt contributed to its mass adoption, but it only considers existing customers. I need to measure prospects as well as existing customers.</p>

<p>That brings us to consider other metrics like Brand Awareness. My mate, <a href="https://twitter.com/willmcinnes">Will McInnes</a>, and his team over at Brandwatch wrote a <a href="https://www.brandwatch.com/blog/marketing-how-to-measure-brand-awareness/">good piece on measuring brand awareness</a>. In the back of my mind, I was also thinking about the McKinsey piece on the <a href="http://www.mckinsey.com/business-functions/marketing-and-sales/our-insights/the-consumer-decision-journey">Consumer Decision Journey</a>. Building on this, and in conjunction with an excellent  book by <a href="http://www.marketinglawsofgrowth.com/">Byron Sharp</a> of the <a href="https://www.marketingscience.info/">Ehrenberg-Bass Institute for Marketing Science</a>, called <a href="https://global.oup.com/academic/product/how-brands-grow-part-2-9780195596267?cc=us&amp;lang=en&amp;">How Brands Grow: Part II</a>, I started to think about a measurement framework.</p>

<p>First, some background though on How Brands Grow - The <a href="http://www.marketinglawsofgrowth.com/blog_files/3444403041d5fa8c9c07a9ae636b39b9-39.html">law of double jeopardy</a> provides clear evidence that larger share brands have more customers who buy them more often than smaller share brands. Research also shows that a brand’s heaviest customers are already likely to be heavy buyers of the category - growing the existing base has only so much upside. Instead, major growth needs to come from increased market penetration and this strategy is dependent on building mental availability and physical availability, two constructs discusssed at length in How Brands Grow.</p>

<h3 id="mental-availability">Mental Availability</h3>

<p>To be bought, a brand must first be thought of. The breadth (how many) and strength (how strong) of the brand’s links to relevant cues determine the chance of this happening. Cues come from common experiences that buyers in the category share. The cues are useful category entry points (CEPs). Attaching the brand to these memory structures will increase the chance the brand will come to mind in buying situations. An example of a CEP at Nordstrom might be, “Where the gifts are.”</p>

<h3 id="building-physical-availability">Building Physical Availability</h3>

<p>There are three components of physical availability;</p>

<ul>
<li>Presence - A retailer&rsquo;s stores and online and mobile offerings obviously provide a physical presence.</li>
<li>Relevance - Is our brand buyable? Two tasks can help here: mapping category variants to check coverage across key buying occasions, and uncovering any barriers to purchase to make the brand easier to buy.</li>
<li>Prominence – Is our brand easy to spot? Easy to buy also means being easy to find.</li>
</ul>

<p>Putting it all together, I came up the following measurement framework:</p>

<h2 id="brand-health-measurement-framework">Brand Health Measurement Framework</h2>

<p><a href="/images/brand-measurement-framework.png"><img src="/images/brand-measurement-framework.png" alt="" /></a></p>

<p>The thought here is that we should be measuring brand health in each phase of the consumer journey.</p>

<h3 id="initial-consideration">Initial Consideration</h3>

<p>Just as with the <a href="/post/mediafragmentation/">fragmentation of media</a>, the exploding choice of retail shows that there is an inherent bias to go with already popular choices because a) it’s already part of most people’s retail repertoire, b) it offers a sufficiently high quality experience, and c) there’s a social aspect to doing things that others are doing that offers opportunity for conversation around common topics.</p>

<p>In our growth strategy, increasing market share relies on driving purchase behavior among non-buyers and light buyers. To increase market share, building mental availability starts with measuring unaided awareness.</p>

<p>Next, for those that are aware of the retailer, we want to understand to what extent the consumer would consider buying from the retailer in question.</p>

<p>From there, we can then explore additional measurements that go into more depth around our Category Entry Points (CEP) and their relation to awareness and consideration. (see pg 76 of How Brands Grow, Part II)</p>

<ul>
<li>Mental Market Share – Percentage of CEP associations of the total CEP associations for our brand and competitors.</li>
<li>Mental Penetration – The percentage of category buyers who link the brand with at least one CEP.</li>
<li>Network Size – How many CEPs the brand is linked to in the minds of those aware.</li>
</ul>

<h3 id="active-evaluation">Active Evaluation</h3>

<p>In this stage, consumers are actively researching, and the number of brands under consideration may expand. Per the McKinsey study, two-thirds of activities involves the consumer pulling information such as reviews and word of mouth (WOM) recommendations.</p>

<p>People usually give WOM about brands they’ve experienced first hand and when it meets the following conditions: 1) have formed a confident opinion about the brand, 2) have seen the brand’s advertising, and 3) have had an unusual brand experience / story to share.</p>

<p>WOM levels tend to be highly correlated with market share. So, as we execute our growth strategy, we would expect to see WOM grow too. With that in mind, here are the measures we should be tracking in the Active Evaluation phase:</p>

<ul>
<li>Direct Load – A <a href="http://marketingland.com/amazon-is-the-starting-point-for-44-percent-of-consumers-searching-for-products-is-search-losing-then-145647">recent survey</a> found that 44% of consumers start their product searches directly on Amazon, 34% start with traditional search engines, and 21% start on a retailer site. As a retailer grows its brand awareness, one would expect to see direct load increase.</li>
<li>Search volume data – You can get indexed search data via Google Trends and other sources.</li>
<li>Social Listening – <a href="https://www.brandwatch.com/blog/marketing-how-to-measure-brand-awareness/">Per Brandwatch</a>; volume of mentions, reach, engagement, and share of voice</li>
</ul>

<h3 id="moment-of-purchase">Moment of Purchase</h3>

<p>Obviously a retailer would look at the usual operating metrics such as demand, conversion, and average order value. However, with respect to the brand, this is our opportunity understand customer perception along two dimensions. The first, is the functional aspects of a retailer&rsquo;s core offering; price, selection, convenience, experience, etc. The emotional dimension would attempt to measure how much consumers actually like shopping with you.</p>

<p>From that, we can then seek to estimate an expected market share based on how much consumers like the experience of shopping with a retailer compared to our actual market share. We&rsquo;d then end up with something like the following 2 x 2 matrix:</p>

<p><a href="/images/brand-2x2.png"><img src="/images/brand-2x2.png" alt="" /></a></p>

<h3 id="post-purchase">Post Purchase</h3>

<p>If the customer has had a positive experience with the brand, we would expect to see growing engagement and advocacy. As such, we could be measuring the following:</p>

<ul>
<li>Engagement - You&rsquo;d ideally want to see repeat buyers grow over time.</li>
<li>App downloads – If a retailer has a mobile app, a more engaged customer base should show growth of app downloads as a percentage of total customers.</li>
<li>Loyalty – If the retailer has a loyalty program, are the number of rewards sign ups as a percentage of first time purchases growing over time?</li>
<li>Net Promoter Score</li>
</ul>

<h2 id="how-should-we-measure">How Should We Measure?</h2>

<p>So, if those are the metrics, what options do we have for actually measuring all of those KPIs? Forrester wrote a paper earlier this year called, <a href="https://www.forrester.com/go?objectid=RES101701">The Emerging Revolution In Brand And Experience Measurement</a>. Forrester doesn&rsquo;t permit sharing the article, but they do note that, &ldquo;The brand measurement vendor landscape has polarized between traditional, strategic brand advisors and increasingly sophisticated social listening providers.&rdquo;</p>

<p>In the brand advisory category, they further delineate between generalists and specialists. Among the generalists are GfK, Interbrand, Ipsos, Milward Brown, and Landor Associates. In the specialist camp are firms such as BAV Consulting, Brand Keys, Lippincott, Prophet, and Siegel+Gale.</p>

<p>In the social listening category, they list traditional players (their terminology), including Brandwatch, Sprinklr, and Netbase. The sub-category of branding experts (again, Forrester&rsquo;s terminology) includes Networked Insights and Brand VO2.</p>

<p>Not sure why, but Forrester neglected to mention a few other options like <a href="https://www.google.com/analytics/surveys">Google</a>, <a href="https://www.surveymonkey.com/mp/audience/">SurveyMonkey</a>, and <a href="http://www.brandindex.com/">YouGov</a>. An ideal provider would be one that;</p>

<ul>
<li>Permits continuous measurement of the KPIs with ability to easily slice and dice across a number of dimensions.</li>
<li>Provides ability to set multiple screening questions e.g. income, gender, age, etc.</li>
<li>Enables in-depth studies / profiles around the functional and emotional aspects of the brand.</li>
<li>Has a developer API to pull down the raw data programmatically.</li>
<li>Allows survey respondent data to flow into a <a href="http://digiday.com/platforms/what-is-a-dmp-data-management-platform/">Data Management Platform (DMP)</a> where I can then do further modeling and targeting.</li>
</ul>

<p>I have my own thoughts on which provider comes closest to checking all the boxes, but I&rsquo;ll leave it to you to decide what works best for you.</p>

			</section>
		</article>
		<div id="disqus_thread"></div>
<script type="text/javascript">
    var disqus_shortname = 'jasongowans';
    var disqus_identifier = 'http:\/\/jasongowans.net\/post\/2016-12-30-brand-health\/';
    var disqus_title = 'Measuring Brand Health in 2016';
    var disqus_url = 'http:\/\/jasongowans.net\/post\/2016-12-30-brand-health\/';

    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
	</main>


	<footer role="contentinfo">
		<div class="hr"></div>
		<div class="copyright">Copyright &copy; <a href="//twitter.com/jasongowans">Jason Gowans</a> </div>
	</footer>

</div>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-64063024-1', 'auto');
	ga('send', 'pageview');
</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.4/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

</body>
</html>
