<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Work on Residual Analysis - Jason Gowans blog</title>
    <link>http://jasongowans.net/categories/work/index.xml</link>
    <description>Recent content in Work on Residual Analysis - Jason Gowans blog</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <atom:link href="http://jasongowans.net/categories/work/index.xml" rel="self" type="application/rss+xml" />
    
    <item>
      <title>Time To Level Up</title>
      <link>http://jasongowans.net/post/time-to-level-up/</link>
      <pubDate>Fri, 08 Aug 2014 03:35:08 +0000</pubDate>
      
      <guid>http://jasongowans.net/post/time-to-level-up/</guid>
      <description>&lt;p&gt;Earlier this year, I had the good fortune to meet &lt;a href=&#34;https://twitter.com/paynovation&#34;&gt;Marc Strigel,&lt;/a&gt; COO of &lt;a href=&#34;https://soundcloud.com/&#34;&gt;Soundcloud&lt;/a&gt;. Marc&amp;rsquo;s one of those rare folks that immediately strikes you as perpetually curious, always deconstructing and reframing what he thinks he knows, stress testing those constructs in conversations with others, giving you a lot to think about&amp;hellip;During one of our conversations, he described the culture at Soundcloud, and one of those aspects stuck with me throughout the summer - the notion of &lt;strong&gt;leveling up&lt;/strong&gt;.&lt;/p&gt;

&lt;p&gt;From their &lt;a href=&#34;http://soundcloud.com/jobs&#34;&gt;site&lt;/a&gt;, here&amp;rsquo;s how Soundcloud describes it:&lt;/p&gt;

&lt;blockquote&gt;&#34;We use a term to describe how we’re constantly seeking to improve: “Level Up”. It’s about being self-motivated and challenging each other at every moment.&#34;&lt;/blockquote&gt;

&lt;p&gt;In America at least, we lionize people who push themselves to the absolute limit. Indeed, I used to be one of those people myself. In college, I ran division 1 track, and the mind games I played to convince myself that I was stronger than my opponents, willing to push myself past the point of exhaustion to prevail, were, in hindsight, the product of unadulterated obsession. Over time, this mentality comes to define who you perceive yourself to be and how you outwardly project yourself. The willingness to endure pain becomes a fact of life and as your ability progresses, so too does your pain threshold, and the addictive nature of it just takes over and the cycle repeats ad infinitum.&lt;/p&gt;

&lt;p&gt;&amp;hellip;But, that was 20 years ago. As I ran along the Ship Canal in Seattle last night, I ran with nothing but love and happiness. Did I push myself to the limit? Not even close. Indeed, I think now, I&amp;rsquo;m not looking to push any single part of my life to the obsessive limit. Instead, I&amp;rsquo;m trying to push all parts of my life to the point of happiness, or equilibrium, and that goes for my professional life too.&lt;/p&gt;

&lt;p&gt;This week, I found happiness in figuring out how to write some site scraping code of all things. I found happiness in getting back the results of a formative product my team has been working on and seeing the enthusiasm it generated within the organization. I found happiness in a really interesting &lt;a href=&#34;http://www.aerobatic.io/#!/&#34;&gt;project&lt;/a&gt; I&amp;rsquo;ve been contributing to with a friend. I found happiness in writing.&lt;/p&gt;

&lt;p&gt;At the time I spoke with Marc about Leveling Up, I mostly thought about the pressure for individuals of an organization to constantly push themselves to the limit and the potential for that to negatively manifest itself. Soundcloud is right though - the key really is &lt;em&gt;self-motivation&lt;/em&gt; as opposed to external, organizational pressure or an internal fear of failure. The role of a leader in such an environment is not to create a culture of negative up-or-out energy, but to create the positive conditions that allow and encourage everyone to push themselves to the point of happiness. And real happiness comes not with rote repetition, stagnation, or fear, but with enlightened growth in all areas of your life that are important to you.&lt;/p&gt;

&lt;p&gt;This weekend, I&amp;rsquo;ll be out there again teaching myself to fish for trout in some mountain lake, fumbling around with lures, casting bubbles, and leader lines - stuff I honestly had no clue about 3 weeks ago, but my kids will surely appreciate it, and that makes it important to me. Time to Level Up!&lt;/p&gt;

&lt;p&gt;&lt;a href=&#34;http://jasongowansdotnet.files.wordpress.com/2014/08/jason-at-lsu.jpg&#34;&gt;&lt;img src=&#34;http://jasongowansdotnet.files.wordpress.com/2014/08/jason-at-lsu.jpg&#34; alt=&#34;Jason-at-LSU&#34; /&gt;&lt;/a&gt;&lt;/p&gt;
</description>
    </item>
    
  </channel>
</rss>