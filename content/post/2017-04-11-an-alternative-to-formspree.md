---
author: Jason Gowans
comments: true
date: 2017-04-11
layout: post
title: An Alternative to Formspree Using Aerobatic form-submit
---

_Disclaimer: I'm a co-founder of [Aerobatic](//www.aerobatic.com), a web publishing service for static generators and modern web apps._

A couple of years ago, I built a website for a [construction business](https://www.rhinoco.biz/). It was a simple informational site with a contact form. I decided on [Jekyll](https://jekyllrb.com/) to build it, using the [agency](http://jekyllthemes.org/themes/agency/) theme, and, of course, hosted the site on Aerobatic.

One of the issues with that agency theme though is that the contact form is built with PHP (see [GitHub repo](https://github.com/y7kim/agency-jekyll-theme/blob/gh-pages/mail/contact_me.php)). To have the site be completely static, I needed a solution that used only HTML and JavaScript.

## Using Formspree to Submit Forms
For the longest time, I happily used [formspreee](https://formspree.io/). In that scenario, I tweaked the `contact_me.js` file where, instead of posting the form contents to my php file, I posted the contents to formspree, like so:

```javascript
$.ajax({
    url: "//formspree.io/email@email.com",
    type: "POST",
    data: {
        name: name,
        email: email,
        message: message,
        _gotcha: _gotcha,
        _subject: _subject
    },
```

## Using Aerobatic to Submit Forms

With the new [form-submit](https://www.aerobatic.com/docs/plugins/form-submit/) plugin from Aerobatic, I now had the opportunity to consolidate everything into one provider. Also, it comes with the added bonus of 1) using Google's latest recaptcha to prevent spam, 2) form contents can be viewed in the Aerobatic admin dashboard, and 3) form contents can be posted to a webhook.

To get everything working, I needed to make changes to three files: `aerobatic.yml`, `contact_me.js`, and my `contact.html` partial / include.

### Update aerobatic.yml

```yaml
id: <your website id>
deploy:
  ignore: []
  directory: .
plugins:
  - name: form-submit
    method: post
    path: /contact-us
    options:
      formName: contact-us
      recaptchaSecret: $RECAPTCHA_SECRET_KEY
      targets:
        - name: email
          subject: New Construction Business Enquiry
          recipients:
            - email@email.com
  - name: webpage
```
In the YAML file, we can see that we're using an environment variable called `RECAPTCHA_SECRET_KEY`. To set yours, you'd first of all register for Recaptcha with Google at https://www.google.com/recaptcha/admin. Then, at the command prompt, enter `aero env --name RECAPTCHA_SECRET_KEY --value <YOUR_SECRET_KEY>`.

### Update contact.html

The main thing going on here is that we're using the Google Recaptcha...

```html
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Contact Us</h2>
                <h3 class="section-subheading text-muted">Please email <a href="mailto:email@email.com">email@email.com</a> or fill out the form below.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form name="sentMessage" id="contactForm" novalidate>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Phone" id="phone">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Your Email" id="email">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-xl">
                                <i class="fa fa-paper-plane"></i>
                                Send Message
                            </button>
                        </div>
                        <!-- Register for your recaptcha at https://www.google.com/recaptcha/admin -->
                          <div class="g-recaptcha"
                            data-sitekey="6LeezhsUAAAAAB3Oey_Im2ycxQ4yU_5BRObuRA22"
                            data-callback="recaptchaOnSubmit"
                            data-size="invisible">
                          </div>
                    </div>
                </form>
                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
            </div>
        </div>
    </div>
</section>
```

### Update contact_me.js

Two things here. The first is that we're handling the Recpatcha response. The second is that we're posting to Aerobatic now, instead of formspree. As you can see below, the agency theme uses [jqBoostrapValidation](https://reactiveraven.github.io/jqBootstrapValidation/).

```javascript
$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            grecaptcha.execute();
            event.preventDefault(); // prevent default submit behaviour
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});

function recaptchaOnSubmit(token) {
    // get values from FORM
    var name = $("input#name").val();
    var email = $("input#email").val();
    var phone = $("input#phone").val();
    var message = $("textarea#message").val();
    var firstName = name; // For Success/Failure Message
    // Check for white space in name for Success/Fail message
    if (firstName.indexOf(' ') >= 0) {
        firstName = name.split(' ').slice(0, -1).join(' ');
    }
    $.ajax({
        url: "/contact-us",
        method: "POST",
        data: {
            name: name,
            email: email,
            phone: phone,
            message: message,
            "g-recaptcha-response": $("#g-recaptcha-response").val()
        },
        dataType: "json",
        cache: false,
        success: function() {
            // Success message
            $('#success').html("<div class='alert alert-success'>");
            $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
            $('#success > .alert-success')
                .append("<strong>Thanks for your message. We'll be in touch shortly!</strong>");
            $('#success > .alert-success')
                .append('</div>');

            //clear all fields
            $('#contactForm').trigger("reset");
        },
        error: function() {
            // Fail message
            $('#success').html("<div class='alert alert-danger'>");
            $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
            $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", please fill out the form before submitting.");
            $('#success > .alert-danger').append('</div>');
            //clear all fields
            $('#contactForm').trigger("reset");
        },
    })
}
```

With that, we're done updating our code, and now, on submit, we get en email sent to the recipient:

<img src="/images/aerobatic-forms-email.png" alt="Aerobatic forms email" style="width: 400px;"/>

The other thing I can do is view all of the form submissions in the Aerobatic dashboard. This is handy because, in this instance, the form submissions are being sent to the owner of the construction company. However, if for any reason he's not getting the emails or there's some unexplained issue, I can always come to the Aerobatic admin dashboard to see if the forms have been successfully submitted.

<img src="/images/aerobatic-forms-dashboard.png" alt="Aerobatic forms email" />

So, that's it. While I was generally quite happy with the formspree service, being able to consolidate services was a win as was the use of recaptcha and having the flexibility of posting the forms to email or webhooks. In this scenario, I'm using email, but I could just as easily have posted the form to Slack or Google Sheets as explained in this [blog post](https://www.aerobatic.com/blog/form-submit-zapier-google-spreadsheet/).

