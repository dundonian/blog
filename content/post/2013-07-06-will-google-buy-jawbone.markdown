---
author: Jason Gowans
comments: true
date: 2013-07-06 06:10:24+00:00
layout: post
slug: will-google-buy-jawbone
title: Will Google Buy Jawbone?
wordpress_id: 534
categories:
- Advertising
- retail
tags:
- ad targeting
- google
- jawbone
- machine learning
- psychology
- quantified self
- retail
- wearables
---

[![Image](http://jasongowansdotnet.files.wordpress.com/2013/07/screen-shot-2013-07-05-at-10-51-58-pm.png?w=454)](http://jasongowansdotnet.files.wordpress.com/2013/07/screen-shot-2013-07-05-at-10-51-58-pm.png)

In 1999, Paco Underhill wrote a book called [Why We Buy](http://www.amazon.com/Why-We-Buy-Science-Shopping/dp/0739341928) that attempted to examine the psychology of shopping based on lots of observational studies. Well, here we are in 2013 and I just bought a [Jawbone Up](https://jawbone.com/up) for myself, my wife, and our 5 year old daughter today. The primary fascination is simply in understanding more about our movement and our rest as a family, but that data set could also be a trove of ad targeting information.

There's little doubt that we're just at the very beginning of the wearable computing trend but Jawbone seems to be onto something potentially very very big here. In fact, I'd not be at all surprised to see Google buy Jawbone in the very near future. Why? Well, first it was demographics, then it was purchase data, then it was browsing behavior and inferred tastes and preferences, then it was mobile / location data, and very soon, it'll be wearables data. The big ad platforms like Facebook, Google, and MSN are constantly striving to improve their ad targeting and wearables is a brave new frontier (I also can't quite see Google Glass becoming a mainstream item because of its form factor - pardon the pun).

It's easy to imagine a Jawbone enabled Google ad buy that allows a retailer to buy ads when a user is most receptive to an ad. Is it when she just woke up, or just before she goes to sleep, or just finished a run, or perhaps right after her kids have gone to sleep but she hasn't yet? Likely, the answer is that it depends - the kind of answer that lends itself to machine learning on an unprecedented scale. Exactly the kind of task that a company like Google could credibly tackle. So many questions remain unanswered at the moment, but one thing is for sure, and that's by the time we're done, the original Why We Buy will be augmented by billions of data points from tens of millions of people who'll willingly participate in the biggest anthropological study of human behavior since time immemorial, and Jawbone is well-placed to be a major enabler of this new type of ad targeting.

Whether it's the Nike Fuelband being used to inform targeted ads to the effect of, "Most shoes need to be replaced at 400 miles. You've run 360 miles and are on track to need a new pair in 2 weeks. Click to Buy Now" or some variant thereof, it seems inevitable that the big ad platforms are going to leap into this space in the very near future in a major way. Google might be wise to buy sooner than later if that Google Glass thing doesn't pan out ;-)

Update 7/30/13

LinkedIn Data Scientist, Monica Rogati, is going to lead Data Science at Jawbone...

https://twitter.com/mrogati/status/358243375318708224


