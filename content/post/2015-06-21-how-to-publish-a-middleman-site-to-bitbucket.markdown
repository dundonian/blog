---
author: Jason Gowans
comments: true
date: 2015-06-21
layout: post
title: How To Publish A Middleman Site To Bitbucket
---
Last week, I wrote a post on how to [migrate a blog from Wordpress to Jekyll](/2015/06/12/migrate-a-blog-from-wordpress-to-jekyll-and-host-it-on-bitbucket/), and this set me down the path of checking out other static site generators. One such generator is [Middleman](https://middlemanapp.com/).

## Step 1: Install Middleman
Depending on your system settings, you may need to prepend this with <code>sudo</code>
{{< highlight ruby >}}
$ gem install middleman
{{< /highlight >}}

## Step 2: Create Middleman Site
{{<  highlight ruby >}}
$ middleman init middleman-starter
{{<  /highlight >}}

## Step 3: Create Build Assets
First <code>cd</code> into the project directory you just created, in my case <code>cd middleman-starter</code>. Then, create your build assets as follows:
{{<  highlight ruby >}}
$ bundle exec middleman build
{{< /highlight >}}

## Step 4: Serve Your Site
To see the site you've created, first <code>cd</code> into the <code>build</code> directory and then you can serve up the site localhost by typing:

{{<  highlight ruby >}}
$ python -m SimpleHTTPServer 8000
{{<  /highlight >}}

Now your site can be seen at [http://localhost:8000/](http://localhost:8000/)

From here, you can get fancy and use one of the [community-driven templates](https://directory.middlemanapp.com/#/templates/all), or roll your own. Either way, once you're done editing, hosting via Bitbucket is straightforward:

## Step 5: Create Bitbucket Repo
Whether you use SourceTree or work from the command line, make sure that once you've created your repo and you're ready to commit your assets, you first edit your <code>.gitignore</code> file. Because we're hosting our site via Bitbucket, we'll actually want to commit our <code>build</code> folder. So, make sure you edit or delete that line entirely, e.g. <code>#/build</code>.

## Step 6: Install the Aerobatic Add-On
This is a one-time step in Bitbucket. If you've previously installed the Aerobatic add-on for Bitbucket, skip this step. If not, from the Bitbucket UI, click on your avatar in the upper right corner, select manage account, and then in the left nav, you'll see the add-ons sub menu.

[![](/images/new-add-ons.png)](/images/new-add-ons.png)

## Step 7: Publish your Site
From your repo summary page, click the Aerobatic Hosting link. Make sure that you select the sub-folder checkbox option and you tell Aerobatic where your code is, in this case, <code>/build</code>

[![](/images/link-repo-middleman.png)](/images/link-repo.png)

Once linked, you'll get a dialog informing you that your app has been created and to push your code to publish your first version. And that's it, you're done! You now have a Middleman generated site with CDN, SSL, and custom domain capabilities if you like.

Our example site is now live at [https://middleman-starter.aerobatic.io/](https://middleman-starter.aerobatic.io/) and, in case you're interested, the repo can be found at [https://bitbucket.org/aerobatic/middleman-starter/src/](https://bitbucket.org/aerobatic/middleman-starter/src)