---
author: Jason Gowans
comments: true
date: 2013-07-30
layout: post
slug: high-frequency-clienteling-in-action
title: High Frequency Clienteling In Action
wordpress_id: 634
categories:
- retail
---

Recently, I wrote a [post](/2013/06/24/is-the-future-of-retail-in-finance/) comparing the future of retail to that of finance, and specifically, high-frequency trading. A few days later, I came across a post entitled [The Consumer of the Future Will Be an Algorithm](http://www.linkedin.com/today/post/article/20130708113252-17102372-the-consumer-of-the-future-will-be-an-algorithm) that garnered a lot of attention. Then, just yesterday, I came across a [great example](http://www.bbc.co.uk/news/technology-23462687) of exactly what I'm talking about - an enterprising software engineer wrote a piece of code to pre-empt other would-be diners to score hard-to-obtain tables at popular SF restaurants.

In my earlier post, I wrote:


<blockquote>"...instead of high frequency trading, perhaps it’s only a matter of time before we see High Frequency Clienteling (HFC)? Instead of retailers interacting directly with customers, it’s the customer app that contains a customer’s parameterized rules talking with one or more retailer apps in real-time to make buying decisions on behalf of the customer."</blockquote>


It sure looks like the future is now, and this geek's app might be a foreshadowing of this type of automated clienteling going mainstream over the next 12 months. I wouldn't bet against it...
