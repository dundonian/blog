---
author: Jason Gowans
comments: true
date: 2012-05-16 05:10:00+00:00
layout: post
slug: the-value-of-ephemeral-connections
title: The Value of Ephemeral Connections
---

The big news this week is Facebook's $100B+ IPO. Some deride it as a [passing fad](http://mashable.com/2012/05/15/facebook-fad/) - not much more than a glorified address book. Others see its Social Graph as the next great monopoly - a dynastic operating system of a social generation. Whatever the case, 900M users gives Facebook almost limitless options and a better than 50/50 chance of succeeding in anything it decides to pursue.  
  
Indeed, with the [recent tie up of Bing + Facebook](http://mashable.com/2012/01/10/google-launches-social-search/), we're now seeing the first credible social search product. Of course, Google's been at it for a while with [Search Plus Your World](http://mashable.com/2012/01/10/google-launches-social-search/) but it doesn't have the scale of 900M Facebook users behind it.  
  
For example, now when I go to Bing and search for Maui hotels, I now see 3 things: 1) Organic search results - in this case mauihotels.com shows up first; 2) Paid search - Ritz Carlton shows up top; 3) The option to ask my friends on Facebook, or alternatively, where there's a match, Bing suggests friends who may know based on Facebook profile & timeline info.  
  
All of that taken together is much more powerful than simply a paid search result from Ritz Carlton for example, right? However, the success of Facebook enabled social search hinges on two things 1) Our continued sharing of every aspect of our life and 2) The assumption that we are like our friends.  
  
I'm not about to argue with [Harvard researchers on the first assumption](http://online.wsj.com/article/SB10001424052702304451104577390392329291890.html). However, the second assumption suggests room for further opportunity. That is, while I of course have things in common with my friends, there also many differences - enough to suggest that simply knowing who my friends are is not always enough to infer what I'm likely to consider relevant.  
  
So, if enduring relationships can't always guarantee relevant content, might there be value in ephemeral connections? After all, it happens all the time in the offline world - random encounters at restaurants, in bars, at the movies, in the supermarket, at the park with kids etc. result in conversations with strangers that influence whether or not you buy a product, watch that movie, or go to that bar. These are not friends in the classic sense. Rather, they're people in passing that you struck up a conversation with and trusted enough to listen to what they had to say and valued their opinion, but you're not necessarily likely to add them to your Facebook friend list any time soon.  
  
With that in mind, social search will become much more powerful when it progresses beyond simply mining my friends' likes and comments to actually putting me in touch with potential trusted sources wherever they reside be they friend or stranger. Add in a layer of anonymity so I don't need to become Facebook friends with every ephemeral connection, and now we have a product that truly taps into the potential of the Social Graph.  
  
That vision is very much at the forefront of where we're taking [Härnu ](http://www.harnu.com/)- a recognition that sometimes seeking out a stranger's opinion or getting a local's perspective can be much more valuable than those available from existing friends.  


  

