---
author: Jason Gowans
comments: true
date: 2013-09-27 05:48:51+00:00
layout: post
slug: joining-a-startup-think-slow
title: Joining A Startup - Think Slow
wordpress_id: 791
tags:
- conditional probability
- evaluation
- Startups
- valuation
---

[![Image](http://jasongowansdotnet.files.wordpress.com/2013/09/seashell.png?w=276)](http://xkcd.com/1236/)

I've recently been reading [Thinking, Fast and Slow](http://www.amazon.com/Thinking-Fast-Slow-Daniel-Kahneman/dp/0374533555) by [Daniel Kahneman](http://www.princeton.edu/~kahneman), a book that's filled with many rich examples of the biases in our thinking and examines the concept of judgment through many illustrative experiments. I'll perhaps write a longer post on the book at some point, but the book was particularly relevant in the context of a conversation I had with a friend recently.

This friend has spent the past year trying to build his own startup, and ultimately never did find true product-market fit. So, he recently took the difficult decision to shut his service down and begin looking for a job. Over coffee, we talked about the various opportunities he was evaluating, among which were a number of startups, and it struck me how much of our decision to accept a given job offer is intuition based - something Kahneman would call System 1 thinking.

When I joined my first startup in 2009, I left an enterprise software company where, by most measures, I was making a good living. However, having always wanted to start my own company, I figured a good first step would be to work for a startup and experience what it was like to build a company from the ground up. Over 3.5 years, I of course learned a lot, and while the company did quite well, it was by no means the billion dollar entity upon which I'd originally evaluated it.

In fact, my original evaluation was along the lines of;



	
  1. Do I agree with the founders' thesis of the market they're going after?

	
  2. Do I believe the founders & existing team have the ability to capture a significant percentage of that future market?

	
  3. Is the equity they're offering me potentially life changing?*

	
  4. Is the salary they're offering me enough to support my family?

	
  5. Is there an opportunity for me to grow?


*On the equity, I of course was painted the $1B potential future valuation by the founder, and that's his prerogative. In fact, if he didn't believe the potential was huge, then why do it in the first place, right? However, I figured that even discounting his enthusiasm by 75%, I'd still make out pretty good. Superficial evaluation, I know...

Having now been through the startup wringer a couple of times and yet to experience massive success, here's how I might consider future startup opportunities;

	
  * Only 35% of small businesses survive for five years

	
  * 7% of [Y-Combinator](http://www.ycombinator.com) companies over the past five years have sold or are worth > $40MM ([37 out of 511](http://www.businessinsider.com/startup-odds-of-success-2013-5))

	
  * Kahneman cites a survey of American entrepreneurs who estimated the average chance of success for a business like theirs at 60% (81% of those entrepreneurs estimated their odds of success at 7 out of 10 or higher, and 33% of those entrepreneurs estimated their chance of failing was zero!!)


Given that, let's assume that you're thinking of joining a startup in a senior role and you're being offered 1% equity (for simplicity's sake). Now, while it might be tempting to think that your opportunity is the $1B exit the founding CEO insisted was possible * 1% equity = $10MM, Kahneman's book offers an abundance of evidence of people's over-confident estimates. Don't let yourself be one of his statistics! Even taking the optimistic Y-Combinator outcome, would you be willing to accept a 7% chance of making $400K?

However, as [Henry Blodget](http://en.wikipedia.org/wiki/Henry_Blodget) astutely points out in his [article](http://www.businessinsider.com/startup-odds-of-success-2013-5), that 7% number is only part of the picture. Those are the companies that were actually accepted to Y-Combinator. The Y-Combintor acceptance rate is [said to be 3%](http://techcrunch.com/2011/08/23/y-combinator-demo-day-the-ultimate-roundup/). So, your odds of success are now more like 3% * 7% = 0.2%

So, are you willing to leave your current job for a 0.2% shot at $400K? Oh, and keep in mind that's a future $400K. The [average successful, VC funded exit](http://www.angelblog.net/Venture_Capital_Exit_Times.html) can take more than 10 years. So, that $400K is actually worth more like $185K in today's dollars assuming an 8% annual return over the next 10 years...

The journey can of course be rewarding in a professional sense, but so too should the financial outcome. Personally, I concluded a while back that the only game worth playing in the startup world these days is one where the startup is my own...
