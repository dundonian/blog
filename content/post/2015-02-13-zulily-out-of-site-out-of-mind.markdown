---
author: Jason Gowans
comments: true
date: 2015-02-13 06:32:07+00:00
layout: post
slug: zulily-out-of-site-out-of-mind
title: Zulily - Out of Site, Out of Mind?
wordpress_id: 1074
tags:
- retail
- zulily
---

A few days ago, Zulily released its [Fourth Quarter and 2014 Full Year Financial Results](http://investor.zulily.com/releasedetail.cfm?ReleaseID=895974).
	
  * Q4 net sales were up 52% year over year to $391.3 million
  * Q4 EBITDA $20.3 million
  * Active customers were up 54% to 4.9 million * Active is defined as bought >=1 in the trailing 12 months
  * Average Order Value of $58 (up 3% YoY)

Problem is, they missed expectations on both top line revenue and earnings per share. On the latter metric, they missed estimates by more than 40%. During the earnings call, they cited increased churn among more recent customer cohorts. That same day, [they announced their CFO was departing](http://www.geekwire.com/2015/wobbly-business-execution-recent-months-zulily-cfo-marc-stolzman-announces-departure-following-q4-earnings-miss/) amid admissions of wobbly execution.


<blockquote>"We have been growing so fast that, at times, we’ve let execution be more wobbly than it should be..." - Mark Vadon, Co-Founder Zulily</blockquote>


Gelatinous execution aside, all of this got me thinking. Arguably, the Zulily storefront is not actually Zulily.com. Instead, their storefront is actually a daily customer email. In that construct, you can think of someone's Gmail inbox as being analogous to a mall or downtown core with the promotions tab the least trafficked bit of downtown. Not on the main street. Still downtown but a few blocks away such that serendipitous footfall is the exception rather than the rule.

As we're seeing the great fragmentation of online retail play out with a proliferation of category challengers competing against traditional multi-category retailers and brands going direct to consumer, one effect is that we're being saturated with more communication, email especially. One way to compete against the noise is to personalize the communication in the hope of being more relevant, but if Zulily is seeing more churn among customer cohorts, there's just that much less data on a given customer with which to personalize and there's only so much cute algorithms can accomplish.

In such a competitive environment, it's little wonder we're seeing more retailers and brands that got their start online, now find ways to have a presence in those physical downtown shopping centers either by opening their own stores, having pop-up stores like Warby Parker's [bus](http://blog.warbyparker.com/bulletin-from-the-bus-seattle-part-1/), or partnering with larger retailers a la [Bonobos](http://press.nordstrom.com/phoenix.zhtml?c=211996&p=irol-newsarticle&ID=1682596).

Downtown Seattle is and always will be composed of ([not comprised of](http://www.businessinsider.com/wikipedia-comprised-of-bryan-henderson-wikignome-2015-2)) not much more than four dense blocks of retail with a less trafficked outer perimeter. When people are out for lunch, walking to work, going to the movies, or grabbing a coffee, they simply cannot ignore the Sephora at Westlake, Banana Republic on 5th, Nike on Pike Street, etc. It's stating the obvious, but those storefronts occupy space that no other retailer can occupy simultaneously. That's not true of our email inbox, however. It can expand to accommodate more or less an infinite number of promotional emails and thus, for an online-only retailer to capture our attention on a consistent basis via their email storefront, it's asking a lot.

Might we eventually see Zulily roll the dice and try to establish a physical presence, or is this just a temporary case of out of site, out of mind?
