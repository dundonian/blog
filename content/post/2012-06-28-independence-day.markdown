---
author: Jason Gowans
comments: true
date: 2012-06-28 16:54:00+00:00
layout: post
slug: independence-day
title: Independence Day
---

For me, Independence Day fell on June 27, 2012. This is the day that I finally overcame a lifetime of self-doubt and walked away from the succor of someone else's paycheck. As Vice President of Business Development for a Seattle tech company I had a pretty comfortable life and I chose to trade all of that security for a chance to pursue starting my own company (with a few friends) - a process that I ought to have started 15 years ago.  


  


Over the years, I've had tons of ideas - some reasonably plausible, others less so. In every instance though I've stopped short of actually pursuing them, satisfied instead with the vanity of people telling me that's a good idea. There comes a point though (some call it a middle-age crisis) where you ask yourself what you're all about. What are you actually teaching your kids? Are you really happy? Does your work reflect the values that are important to you?

  


In truth, I've always asked myself those questions, but the seniority of the positions I held grew over time as did the paychecks and perks, and I got lazy. We got married, we had kids, we bought a house, etc. You can easily convince yourself that it's never the best timing to take a leap into the unknown. Over time, the frequency and amplitude with which I questioned myself lessened to the point where I was basically along for the ride in the near-inevitable corporate machinery of big company America.

  


However, earlier this year, along came the catalyzing event that began the metamorphosis from indolent Microserf to fledgling entrepreneur. A trip to a dermatologist and a resultant cancer scare was all it took. Turns out it was nothing but it was all I needed to remind myself of the impermanence of my life. That my mother died of cancer aged 44 should have been enough, but as I said, I got lazy...

  


So, here I am sitting in a Starbucks on Day 1 of not having a paycheck for the first time in 15 years, trying to focus on the road ahead and ignoring the nagging, fearful self. Regardless of whether Härnu proves to be a hit or a bust, I'm committed to this state of independence that I eschewed for so long. I owe it to my kids to show them that life is something to be embraced with confidence, not survived in fear.

  


As for Härnu, all four founders are now full-time and there's nothing holding us back except ourselves. We're in the middle of a big redesign, we're rapidly building out our go-to-market plans, and learning as we go.

  


If you're reading this post and you have aspirations of starting your own company, I won't pretend to have any advice for you. All I can say is that honesty with myself is where it begins for me. Every. Single. Day.

  


Good luck!

  


p.s. If yesterday wasn't momentous enough, I became the Foursquare Mayor of the India Gate restaurant in Eastgate. What a way to end my Eastside commute!   


  

