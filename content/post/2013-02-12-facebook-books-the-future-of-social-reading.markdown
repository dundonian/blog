---
author: Jason Gowans
comments: true
date: 2013-02-14
layout: post
title: Facebook Books And The Future of Social Reading
---


Facebook [today announced](http://techcrunch.com/2013/02/12/facebooks-next-big-app-categories-will-be-movies-books-and-fitness/) that their next big priority categories are movies, books, and fitness. Never mind that Amazon and Apple realized those were priority categories years ago, and in the case of Amazon, as early as 1998 with their acquisition of [IMDB](http://en.wikipedia.org/wiki/Internet_Movie_Database). What struck me about the declaration is not that they are late to the game but rather that their definition of success is so modest.  


  


Success for Facebook in any given category is to open it up to mass content sharing - gaming, news, and music were all cited as success stories and indeed, more content --> more engagement --> more ad revenue. The Facebook flywheel is a well oiled machine by now with likes and recommendations. However, i can't help but feel that it could be so much more...

  


  


In the case of books, Facebook seems to have missed an opportunity to innovate with the [acquisition](http://www.pcworld.com/article/237217/why_did_facebook_buy_push_pop_press_.html) of Push Pop Press. Whether that was because of [IP issues](http://appleinsider.com/articles/12/01/19/steve_jobs_rumored_to_have_shot_down_push_pop_press_for_ibooks_author_) is unclear, but instead of solely thinking about recommendations on what books to read, it seems that there's an opportunity to optimize the activity of book reading itself and completely redefine the category. In other words, a recommendation engine focused on intra-book comprehension AND inter-book discovery would be much more useful and valuable.

  


For example, it's well established that people [learn in different ways](http://en.wikipedia.org/wiki/Learning_styles). Further, research shows that our preferred learning style is situationally dependent versus an immutable fact. So, what if the future of books is adaptive where the content is dynamically served, based, in part on the feedback loop we create? Metrics to consider could include;

  * Descriptive Metrics - average words read per minute, dwell time per page, highlights per 1,000 words, average session length, dictionary usage, books read per month, etc.
  * Interactive Metrics - built into the books could be optional polls and pop-up quizzes that test comprehension, the result of which would influence the content to follow.

Since it's Facebook we're talking about, all of the above metrics could of course be shared back out to each user's news feed, so instead of just knowing that Jason is reading _A Scots Quair_, I could also share notable quotes from the book, and publish the results of various quizzes I'm taking as I read. We could go one step further and Facebook could create dynamically assembled book clubs where I can chat with other people who are reading the same book as me at the same time.

  


The other big opportunity is to view interactive books as a launching-off point to dynamically linked 3rd party content. Continuing with the _A Scots Quair_ example, Facebook knows my hometown is Dundee, Scotland which is near the setting of this book. So, when I highlight the word Dundee in the book, I get a set of personalized search results - maybe an offer to buy Dundee University gear or a link to their Facebook page, whereas someone else highlighting the word who's not from Dundee gets more tourist-oriented content.

  


Regardless of monetization opportunities, the key point is that we're talking about an optimized reading experience based, in part, on Facebook's social graph. There are undoubtedly many who would argue that reading should remain a solitary activity, but one only need to look at how Twitter has augmented the TV viewing experience to understand that the future of reading is social and dynamic. My bet is that it will be intra-book in addition to the more generally accepted inter-book experience.
