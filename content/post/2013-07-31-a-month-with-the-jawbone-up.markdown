---
author: Jason Gowans
comments: true
date: 2013-07-31 21:12:05+00:00
layout: post
slug: a-month-with-the-jawbone-up
title: A Month With The Jawbone Up
wordpress_id: 637
categories:
- retail
tags:
- jawbone
- quantified self
- retail
---

In early July, I bought a Jawbone Up for myself, my wife, and our 5 year old daughter. Initially, I was really excited by the possibility of being able to compare ourselves to each other, and I was especially eager to learn more about my daughter given that we spend our day apart, with me at work, and her galavanting around the city.

For example, below is a comparison of our movement over a two week period in July:

[![Jawbone Up Distance](http://jasongowansdotnet.files.wordpress.com/2013/07/distance.png?w=584)](http://jasongowansdotnet.files.wordpress.com/2013/07/distance.png)

I had high hopes that I'd be able to do so much more analysis, but the one glaring observation from this first month is simply the lack of compliance. Specifically, at least one of us often forgets to put the band on, forgets to charge it, forgets to switch it to sleep mode, forgets to switch it to day mode, forgets to input our food intake, etc.

Given that, I think there are three obvious ways to improve the current experience.



	
  1. One is to think about ways to make it more convenient to be wearables-enabled. Must the form-factor be a band? Can we integrate purchase data to make the act of logging food intake easier?

	
  2. The second is to make the band yet more instrumented - GPS-enabled, temperature, pulse, auto-sleep detect, etc.

	
  3. The third is where I think there's the biggest opportunity, and that is that Incentives Drive Behavior - Imagine my employer said that if I agreed to wear the Jawbone Up for 90% of the days in a given year, input my food intake, and share my data with them, I'd be eligible for a $5,000 annual bonus. Compliance would obviously climb dramatically. Whether Jawbone creates its own salesforce or third party companies strike distributor partnerships with Jawbone and then sell into enterprises, it seems an obvious path forward on this front.


The ideal startup that focused on helping companies drive down healthcare costs would have the following capabilities:

	
  1. Statistical modeling & scenario-planning where a company can model conservative, likely, and speculative outcomes based on an improvement in overall health of its employees. An improvement in health would have a positive impact on productivity, sick days, and of course healthcare premiums.

	
  2. Given the above, the enterprise would then be in a position to know how much to incentivize its employees to achieve those outcomes.

	
  3. Manage the distribution and monitoring of the devices - make sure the employees all have a device, and be responsible for tracking compliance and employee performance.


With such a company, the business model could be performance-driven with comp based partially perhaps on a KPI of sick days per employee that's easy to benchmark and track...
