---
author: Jason Gowans
comments: true
date: 2014-08-13 04:47:12+00:00
layout: post
slug: how-big-companies-lose
title: How Big Companies Lose
wordpress_id: 1062
---

The optimistic title of this post would have been, "How Small Companies Win" but I'm Scottish, and so the pessimistic title it shall be.

To some, particularly those in tech, there's a belief that the average big company "doesn't get it." The startup sees the world in a way that the large company does not. The startup sees a large market, a catatonic customer base ready to be awakened, and a host of large incumbents ready to be upended - The fat cats will get their comeuppance and won't know what hit 'em.

However, as with most things, I'd argue it's a bit more nuanced than that. Senior executives (at least the ones I've known) of large companies tend to be keenly aware of the macro conditions surrounding their business. They care deeply about their business. They obsess about the health of their business and they're no less formidably educated, talented, or hungry than any startup counterpart. In fact, more often than not, their experience at large companies has afforded them a network that gives them access to the brightest minds and exposure to the latest ideas and trends in their industry. Given the available resources of most large companies, these executives have the latitude to chart a course that most startups can only fantasize about.

So, given these handicaps, why is it that we see large companies repeatedly stumble and startups make deep inroads into seemingly heretofore impenetrable markets?

I'd argue there are four primary reasons how big companies lose (and some secondary ones too):
	
## Focus
  	
Large companies are often attacking and defending on multiple fronts. In any such approach, it's inevitable that the best resources are diluted across those efforts. With startups, they tend to be (at least initially) singularly focused and that makes them dangerous.

	
## Risk
  	
Startups have everything to gain. Publicly traded companies have everything to lose. Seemingly trivial issues can become major thorns and the tendency is toward risk avoidance, let alone risk mitigation, particularly when you traverse down from the execs and into the broader org at large.

	
## Heritage
  	
Heritage needn't be an abstract term to politely describe those long-since-forgotten values of a bygone era. Rather, heritage can be something tangible. Something you're famous for. Something, when everything is going to hell in a handbasket, you don't panic. You know what you're about, and you're not about to lurch. However, heritage can also mean being encumbered with the multiplicative choices of the big company's forebears, and nobody ever bats 1000...

	
## Talent
  	
Inevitably, within a large org, it's impossible to stack it only with A-players. Or, said more accurately, I've yet to observe a large company with only A-players. Often with no major financial upside, a risk avoidance culture, and perhaps a difficult legacy to be inherited, up-and-comers understandably may be reticent to join the ranks of a big co, and ambitious and talented employees may feel their best chance of success lies outside the big co.


How big companies win is to address these issues head on. Acknowledging that every minute of the day, one or more startups are actively plotting to take their customers, to carve out a large piece of what was once "theirs" is a necessary starting point. That feeling has to be shared not just among the most senior executives but throughout the entire org. After all, the impermanence of success is the only permanent condition.
