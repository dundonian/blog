---
author: Jason Gowans
comments: true
date: 2014-02-28 22:56:44+00:00
layout: post
slug: rampup-2014-recap
title: RampUp 2014 Recap
wordpress_id: 975
tags:
- adtech
- advertising
- digital
- liveramp
- marketing
- rampup
---

Yesterday, I had the opportunity to attend [LiveRamp's](http://liveramp.com/) [ad-tech conference](http://rampup2014.com/), held at the [Computer History Museum](http://www.computerhistory.org/) in Mountain View. Below is a recap of the sessions I attended.

**Opening Keynote**

Google's [$100M dollar man](http://www.businessinsider.com/neal-mohan-googles-100-million-man-2013-4), [Neal Mohan](https://twitter.com/nealmohan), kicked off the day by talking about:

  * **Multi-device** users and the implications that has for ad formats, ad measurement and attribution.
  * Incorporating **user choice** into the ads, citing YouTube's [TrueView](https://www.google.com/ads/innovations/trueview.html) feature in particular, where users have choice over which ads to watch, and advertisers only pay when the ad is actually viewed.
  * Combating **ad fraud** both on the inventory side and on the buy side
  * A focus on **brand measurement**. This one's funny given that search is entirely a performance based advertising medium. However, there's a growing argument being made in Silicon Valley that brands should be measuring their display campaigns not through a direct response lens but instead through a traditional TV advertising lens of things such as brand recall and awareness. It reminded me of Instagram [adopting the same position](http://techcrunch.com/2013/12/19/instagram-ad-results/) last year. This is in contrast with Pat Connolly, the CMO of Williams-Sonoma, who also spoke on a panel and asserted that they're entirely a performance marketer. Will brands actually buy into traditional media metrics for their digital spend? I think that remains an open question.


The one thing that Neal stressed time and again was the focus on trying to do what makes sense for the users. An example was when someone in the audience asked about injecting display ads into messaging apps, to which he gave a measured reply that they'd only consider doing something like that if there was a logical context for doing so.


# **Convergence of Offline and Online Data**


This panel featured [Rick Erwin](http://www.experian.com/marketing-services/leadership-detail.html#rick) of Experian, [Scott Howe](http://acxiom.com/scott-howe/) of Axciom, and [Dave Jakubowski](https://twitter.com/davej95) of Neustar. The main themes that emerged from this conversation were what was referred to as **entity resolution** both in terms of cross-device identification, multi-source 1st party customer data such as sales and customer service, and 3rd party data appends.

Regarding advertising on a particular channel, one of the panelists made the point that brands need to be thinking about owning the experience versus just owning the moment. A good reminder to not think in terms of email, mobile, desktop, etc. but instead think about the customer's use case. I've seen this a lot where there are various vendors out there who'll help with one use case of one channel, and the result is that the customer receives a disjointed experience when interacting across a variety of channels (and a variety of vendors).

TV advertising also cropped up, and particularly around **addressable TV**. Back when I worked on in-store TV networks around 2009, this was something people were beginning to explore and while it's still early, it's almost certainly just a matter of time before digital and TV campaigns are targeted to individual users. This also bleeds into **dynamic creative** which was something of a recurring theme. As more ad inventory becomes programmatic, it stands to reason that TV will eventually follow suit both in terms of RTB and dynamic creative.

Measurement was also something that came up, particularly in terms of **digital advertising's effect on in-store sales**. This was something we did at [DS-IQ](http://ds-iq.com/) circa 2010 and it's strange to hear companies only now starting to develop scalable solutions in this area.


# How Top Brands Use Data OnBoarding Today


This panel featured Brandon Bethea from [Adaptive Audience](http://adaptiveaudience.com/), Nikhil Raj from [Walmart Labs](http://www.walmartlabs.com/), and Tony Zito from [Rakuten MediaForge](http://mediaforge.com/). A couple of things stood out in this talk. The first was the depth of Walmart's planning with their CPG suppliers. Nikhil naturally didn't offer much detail but one could make a reasonable assumption that there's a large amount of **data sharing** that takes place between the retailer and the brands. This has all kinds of advantages in terms of developing an understanding of the customer, and in terms of improving marketing outcomes for both brand and retailer marketing campaigns. It's not clear if there's a formal data exchange platform that's common among Walmart and the brands but that'd certainly make a lot of sense.

The other thing that was discussed was the notion of **lookalike modeling** and also **ad suppression**. Again, nothing new, but simply a reflection of what was on their mind.


# Data-Driven Retail Marketing Strategies


Panelists were Benny Arbel of [myThings](http://www.mythings.com/), [Ryan Bonifacino](https://twitter.com/rbonifacino) of [Alex & Ani](https://www.alexandani.com/), and Jared Montblanc of [Nokia](http://www.nokia.com/us-en/). Of note was some of the interesting things that Alex & Ani is doing around targeting for re-targeting campaigns and custom audience campaigns on Facebook through Kenshoo. Ryan cited Facebook as having been a good vehicle for new customer acquisition.

Jared of Nokia discussed how they evaluate their digital spend through the lens of **Cost per High Quality Engagement**. This makes sense in his world where Nokia is selling their devices through carrier partners. So, when they run a campaign, did a user not just click on a video but actually watch it, for example?


# From the CMO: The Future of Data In  Marketing


This was one of the highlights for me, where [Pat Connolly](http://www.williams-sonomainc.com/company-overview/executive-biographies.html) of Williams-Sonoma talked with [Kirthi Kalyanam](https://www.scu.edu/business/marketing/faculty/kalyanam.cfm) of Santa Clara University.

Observation 1: Connolly is one of those self-effacing humble execs who could easily be dismissed as old school based on appearances, and you'd be drawing the completely incorrect conclusion. The guy has been with Williams-Sonoma for 35 years going back to when they were strictly a catalog retailer, he's demonstrably smart and has obvious command of some pretty technical details. For example, how many CMOs have you heard comfortably discuss technologies such as Hadoop, Teradata, and Aster in one breath and then discuss hazards modeling in the context of attribution in the other breath? To my knowledge, the only vendor doing [survival analysis](http://www.youtube.com/watch?v=OTEadldZmko) at scale is [DataSong](http://www.datasong.com/), and for Connolly to be in the weeds there was impressive.

Speaking of being in the weeds, Williams-Sonoma has a monthly marketing investment meeting that the CEO attends where junior analysts are presenting the details of various marketing campaigns. Talk about alignment - between direct (.com), marketing, and merch. Impressive.

Some other nuggets:

* They can identify 50% - 60% of all web visitors, and aim to serve up recs under 40ms. That Connolly can recite the 40ms SLA made me smile, particularly since this is something we live and breath in the Data Lab.
* They do about $2B in eCommerce and believe they're the most profitable ecommerce retailer in the country.
* There are ~100 variables in their regression models, but just one variable has 70% of the predictive value.
* With a simple A/B test of making their Add to Cart button bigger, they added $20MM in incremental demand.	
* They can currently identify 30% of users across devices with a goal of 60% by the end of the year.
* They consider tablet as their core site experience with desktop being simply a bigger version of tablet.
* They consider their competitive advantage to be org alignment between merch, marketing, and direct. I wouldn't disagree, knowing how difficult this can be.
* They allow ad cost for acquiring new customers to be higher. Was simply a good example of enlightened decision-making where they aren't simply trying to maximize ROAS on _every single_ digital campaign.
* While their paid marketing is entirely performance-driven, their owned media such as their blog is allowed to be more brand focused. Pat cited their [West Elm](http://blog.westelm.com/) brand which rarely features an actual product.




# Measuring Digital Marketing's Impact On In-Store Sales


Michael Feldman of Google, [Gad Alon](http://www.adometry.com/company/management.php) of Adometry, Kirthi Kalyanam of Santa Clara U, and [Ben Whitmer](https://twitter.com/benwhitmer) of StageStores were featured panelists.

Gad mentioned that 40% of in-store demand is driven by digital media. Of that 40%, 70% could be attributed to display. I couldn't find any data on the web to support this claim, and would be interested in hearing from others regarding this.

In-store measurement came up briefly but was surprised this wasn't a bigger topic in the conference. Specifically, I'm talking about the kinds of things [RetailNext](http://retailnext.net/), [Nomi](http://nomi.com/), and a host of others do.

Lastly, Peter Thiel was on deck for the closing talk. A summary of his discussion can be found [here](http://www.forbes.com/sites/roberthof/2014/02/27/peter-thiels-advice-to-entrepreneurs-tell-me-something-thats-true-but-nobody-agrees-with/). There were a couple of salient points he made that I've been thinking about since. Will perhaps write more when I'm done digesting :-)

Final point - the overall quality of the sessions was by and large very good. It's clear that there's a ton of investment in this space and while some might argue strongly that there's not a bubble, I'd at least say that were I starting a company right now, there's no way it'd be in ad-tech - just too crowded and fragmented to be excited about.

Final final point - nice job by LiveRamp putting on this one day conference. Next year, it'd be a lot more powerful if the panelists reflected the audience a bit more i.e. I'm guessing half of the audience was female, yet throughout the day, I saw just one female panelist. This is something that's really got me motivated to do something about.
