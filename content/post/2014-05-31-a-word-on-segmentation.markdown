---
author: Jason Gowans
comments: true
date: 2014-05-31 05:25:16+00:00
layout: post
slug: a-word-on-segmentation
title: A Word On Segmentation
wordpress_id: 1033
tags:
- segmentation
---

This was supposed to be a post on the rising cacophony around segmentation, but instead, the Google Trend of "segmentation" quickly disabused me of any such notion.

[![segmentation](http://jasongowansdotnet.files.wordpress.com/2014/05/segmentation.png?w=584)](https://jasongowansdotnet.files.wordpress.com/2014/05/segmentation.png)

For good measure, I also checked to see how "clustering" is trending, and it's not any better...

[![clustering](http://jasongowansdotnet.files.wordpress.com/2014/05/clustering.png?w=584)](https://jasongowansdotnet.files.wordpress.com/2014/05/clustering.png)

Sadly, even a specific algorithm like k-means isn't enjoying a halo effect from the sexiness surrounding all things data sciencey...

[![kmeans](http://jasongowansdotnet.files.wordpress.com/2014/05/kmeans.png?w=584)](https://jasongowansdotnet.files.wordpress.com/2014/05/kmeans.png)

It's also of little comfort to see that k-means is 5X more popular among our Asian counterparts as compared to the U.S.

[![kmeansregion](http://jasongowansdotnet.files.wordpress.com/2014/05/kmeansregion.png?w=584)](https://jasongowansdotnet.files.wordpress.com/2014/05/kmeansregion.png)

So, what's the point? Well, the point is that when you work in a company and lots of people across the spectrum of technical competence discuss the topic of segmentation, there exists an inevitable gulf between a primarily creative group of practitioners within the company who think of segments in terms of customer demographics and seek to create archetypes / personas to represent those customers, and those who work in a technical capacity and think primarily in terms of customer behavior.

Being able to bridge that gulf in thinking such that the creative parts of an organization can embrace a systematic, behaviorally-driven approach to segmentation has to be an important goal of any data scientist / statistician working on this problem. Sometimes, the ability to describe your work is more important than the work itself...
