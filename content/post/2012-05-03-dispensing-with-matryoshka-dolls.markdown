---
author: Jason Gowans
comments: true
date: 2012-05-03 22:52:00+00:00
layout: post
slug: dispensing-with-matryoshka-dolls
title: Dispensing with Matryoshka Dolls
---

When we initially thought about starting Härnu, it was easy to dimensionalize the social & media landscape to fit within a construct that suggested a new service like ours needed to exist. In fact, in a burst of midnight PowerPointery, my assessment ended up looking something like this:  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/05/6e4d2-picture1.png)](http://1.bp.blogspot.com/-eI6pZwvsbiY/T6Lzoe9e_fI/AAAAAAAAIcc/iIZD308sDxg/s1600/Picture1.png)

  
The point of view expressed above is not that controversial, but with the benefit of hindsight, nor is the situation quite that simple either. Basically, my initial assumptions could be summarized into the headline that **_we need to cut out the middle man_**. That is, the primary ways in which we consume media today have made us more insular, less informed, less empathetic, and big media isn't going to change any time soon.  
  
Our goal from the beginning has been to act as an accelerant for empathy - connect people across the world in an authentic way and you can start to chip away at the biases that our media has helped reinforce. Connect people one-to-one and suddenly a like or a share is exposed as embarrassingly inadequate when trying to express solidarity for our fellow man / woman.  
  
Basically, our solve for this was not to build out more [nested Matryoshka dolls](http://en.wikipedia.org/wiki/Matryoshka_doll) such as a mobile app on top of a social network on top of a relevance engine on top of an aggregator on top of a search engine on top of content for example, but to simply dispense with the nesting paradigm altogether - enable and encourage direct access to people in locales everywhere and anywhere. Simplify. A battle of Turkey versus [Turducken ](http://en.wikipedia.org/wiki/Turducken)if you will.  
  
Want to know what's happening in Syria? Don't just watch CNN or read about it on [insert social network here]. Instead, actually talk to some Syrians and hear from them directly with Härnu translating all the conversations on the fly into each other's preferred language. Having done so, are you now better informed? Are you inspired to act?  
  
Of course, not every interaction is going to result in someone taking the next plane to Damascus or lobbying the White House for action and in fact, a lot of the interactions are often seemingly mundane - where do you live?, what do you eat?, where do you work? etc. However, _all _of those interactions together have the capacity to add up to something more than a [nanoKardashian](http://www.ethanzuckerman.com/blog/2012/05/02/an-idea-worth-at-least-40-nanokardashians-of-your-attention/).  
  
So, those were the original assumptions. Now that we actually have a service out there, I've learned the following three things so far:  
  


  1. As much as we thought we needed to make Härnu completely private to offer a level of comfort to people in countries where censorship is de rigeur, we've realized that sharing features everywhere is simply a must-have for any online community. Perhaps everyone that reads this is saying "duh idiot!" but the point is that people talk about wanting more _privacy _but what they really want is more _control _and those two terms are not the same.
  2. There's actually a lot of good content out there being produced by citizen journalists everywhere. An example would include [GlobalVoices](http://globalvoicesonline.org/) and Twitter is a great way to broadcast that content out into the Internet. However, the trick is still to close the gap between awareness and action. While tools like Twitter and Facebook enable awareness and engagement to some degree, I'd suggest that the missing link is still empathy such that the path to action looks like this: awareness-->engagement-->empathy-->action. More one-to-one conversations can balance the many-to-many speed of light content delivery and consumption tools we have out there today.
  3. There's no substitute for face to face interaction...yet! While Härnu may or may not be able to deliver on its promise of instilling empathy for other cultures in a massive way some day, the 15 minutes I spent last week at the [Seattle Globalist](http://www.seattleglobalist.com/) launch chatting with a Pakistani editor who's now a refugee, gave me enough of a jolt to realize how lucky I am to be talking about building an online service versus fleeing authorities intent on killing me because I had the temerity to speak the truth as a journalist. If our media in this country were half as brave, perhaps we'd have a press more people would be satisfied with. As it is, a lot of brainpower is being spent trying to fix it or circumvent it entirely.
I think that part of the solution involves connecting ordinary citizens everywhere and giving them the power to triangulate multiple perspectives. I don't believe it's simply a binary choice between people versus machine-learning algorithms, but rather that in a world of [filter bubbles](http://www.thefilterbubble.com/) the ability to connect to anyone anywhere as we are to our friends and family today may prove to have merit.  

