---
author: Jason Gowans
comments: true
date: 2013-08-07 17:20:19+00:00
layout: post
slug: the-classy-way-to-shutdown-your-app
title: The Classy Way To Shutdown Your App
wordpress_id: 644
---

This morning I received an email from a mobile app that I'd been using in the past called [Wander](http://wanderwith.us/), and for obvious reasons, I could really identify with the founders' disappointment at having to close the service. What struck me though was that the three founders all still appear to be buddies, and having been through this process myself in the past, I can only tip my hat to them for having pulled off that feat.

If you're ever thinking of shutting down your service and want to keep it classy, see their email below. I'm sure this won't be their one and only shot at shooting for the moon...


<blockquote>Hello Wanderers,

Unfortunately, Wander is shutting down. The app has already been removed from the App Store, and the service will be discontinued on August 15th. We sincerely apologize for the disappointing news. The Wander team tried to avoid this for as long as possible, but have been unable to properly support the app financially or technically for well over a year.

Please take a moment to save any special photos you'd like to keep, as the app will no longer work properly and the server will no longer be available to restore your past conversations and photos after the shutdown. If you have friends on Wander who you'd like to keep in touch with, we recommend using another app to chat (such as LINE, WhatsApp, KakaoTalk, Path), exchanging email addresses, or connecting elsewhere like Facebook. Of course, please remember to be smart about who you share your personal contact information with!

- - - - -

On a personal note, the other founders and I are grateful to each and every one of you who downloaded the app, shared amazing photos and stories, and helped make the world a tiny bit smaller.

Back in the Spring of 2011, [Darien](https://twitter.com/darienbrown), [Jiho](https://twitter.com/jihoya), and [I](https://twitter.com/daronakira) set out to create a simple way for people to discover the world and connect with other cool explorers. Many all-night coding and design sessions later, we released the Wander app, our attempt to capture a small part of the backpacking experience and put it in your pocket. We passionately believed that the most important elements of travel are not only the new sights you see, but the people you meet along the way and the experiences you share together. Hopefully those of you who wandered with us these past few years will agree!

We had big plans for Wander, and hoped to turn the project into a sustainable business to share the experience with as many people as possible. Unfortunately, we've each had to move on to other projects and jobs for financial reasons, and were never able to grow Wander into everything we imagined it had the potential to become. As we were unable to actively support or update the app for over a year and a half, many of you undoubtedly experienced bugs and a decline in the community, as well as a few bad apples who tried to spoil the experience for everyone else. Believe me, this was as frustrating for us as it was for you, and for that we apologize.

Though we were never able to fully deliver on the promise of Wander, we know many of you still had wonderful experiences, and thank you for sharing some of them with us. Many of you made new friends in distant countries; some got their first passports and traveled abroad to visit the very people and places they'd discovered via the app; and at least a few even found love. I for one had the great honor of being the first foreigner a girl in rural China ever met, and also connected with amazing people in Turkey, Brazil, Korea, and Japan, among other places. The stories you shared with us convinced us what we were doing had value and real meaning, and for that we'll always be thankful.

This will be our last email to the Wander email list. Thank you again for being a part of our community. Perhaps our (real or online) paths will cross again someday!
Keep wandering,

[Daron](https://twitter.com/daronakira), [Darien](https://twitter.com/darienbrown), & [Jiho](https://twitter.com/jihoya)

ps. We've included a photo of the team, our final farwell!

[![1436925](http://jasongowansdotnet.files.wordpress.com/2013/08/1436925.jpeg?w=300)](http://jasongowansdotnet.files.wordpress.com/2013/08/1436925.jpeg)</blockquote>
