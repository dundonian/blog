---
author: Jason Gowans
comments: true
date: 2017-01-08
layout: post
title: The Power of Peloton
---

Late last year, my wife started talking about getting a stationary bike to train on at home. My initial reaction was to think instead about a bike trainer so we could use our road bikes to train on. Well, after much research and deliberation, we finally opted for a [Peloton](https://www.pelotoncycle.com/).

Stationary bikes, to me, have always been emblematic of the excesses of consumerism - that thing that you think is a good idea for about 5 minutes when you're determined to get fit, but then ends up as nothing much more than a clothes hanger when the zeal for self-improvement passes and we all regress to our own mean.

So, suffice to say, I was rather skeptical of the whole thing, but quite simply, I was wrong. The combination of the instructor, the leaderboard, the stats, the music, and the intensity, all make for an unexpectedly powerful and emotional experience - one that's got us both hooked.

Ok, so, fine. I'm a believer in the experience, but am I alone in that enthusiasm? Well, no, it appears not. [Peloton expects $150MM in sales in 2016](http://fortune.com/2016/06/18/peloton/) and even counts t-mobile's CEO among its fans.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Back to Seattle from <a href="https://twitter.com/hashtag/uncarriernext?src=hash">#uncarriernext</a> launch in <a href="https://twitter.com/hashtag/LasVegas?src=hash">#LasVegas</a> and it is time to put on <a href="https://twitter.com/RidePeloton">@RidePeloton</a> gear and <a href="https://twitter.com/hashtag/riderideride?src=hash">#riderideride</a> (and then <a href="https://twitter.com/hashtag/eateateat?src=hash">#eateateat</a>) <a href="https://t.co/RR5rdk4zup">pic.twitter.com/RR5rdk4zup</a></p>&mdash; John Legere (@JohnLegere) <a href="https://twitter.com/JohnLegere/status/817547222652854273">January 7, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Given that the bike costs $2K and the monthly subscription costs $39, that $150MM revenue number implies a total targeted number of units of ~60,000. Considering that each unit / bike is likely ridden by multiple family members, we're talking about perhaps upwards of 150K - 250K total user base. The company also [announced this week](https://www.fastcompany.com/3066877/innovation-agents/peloton-wants-you-to-keep-spinning-your-wheels-when-you-travel) that they're going to start selling the bikes in to the hospitality industry. Aside from the obvious revenue opportunity of selling in to hotels, there's also the very real customer acquisition play. [Starwood's SPG program has 23MM members and Marriott has 55MM members](https://qz.com/645364/your-starwood-points-are-as-valuable-to-marriott-as-they-are-to-you/). Of SPG's members, Marriott's own CEO said, "SPG is a powerful program, there�s no doubt about it. It�s got a strong group of elite loyalists."


With a rapidly growing direct to consumer business and now a nascent B2B opportunity in the hotel industry, this will likely keep the company busy executing for a while. However, what other revenue opportunities might the company be thinking about? Peloton's CEO, John Foley, clearly articulated that, "Digital content is the core of our business model, so we�re trying not to be greedy on the hardware."

Right now, that digital content consists of live and on-demand spinning classes. However, if you assume that the average Peloton member spends 2x per week on the bike (in reality, you'd expect usage to be normally distributed), that's perhaps 200K users x 2 rides p/wk x 45mins = 300K hours of streaming activity per week or 15.6MM hours annually. For context, Netflix (market cap $57B) reported [42.5B hours](http://time.com/4186137/netflix-hours-per-day/) of content watched in 2015.

## Brand Sponsorship

Those 15.6MM hours of data and eyeballs are a really valuable asset. An ad-supported model would be silly, but an obvious, albeit smallish, opportunity would be various endemic branding opportunities - the clothing, the hydration, the connected devices, etc. How much might that be worth? My best estimate is that each branding opportunity is probably worth ~$500K at the moment. For example, here's how a sponsoring company might think about pricing the opportunity: 

- 200K riders x 4% expected conversion rate = 8,000 new customers
- $200 CLV * 8,000 = $1.6MM
- CAC:CLV ratio 30% = $480K

Given category exclusivity and the high-end user base, likely a brand would pay a premium, but still, compared to Peloton's $150MM revenue run rate, brand partnerships, although a nice little earner, wouldn't seem to be material to the value of the business for now.

## Adjacent Activities

At the moment, Peloton has a few yoga classes in its digital library, but yoga lacks some of the characteristics of the core Peloton experience that make it what it is - no stats and no competition. Further, it's simply harder for an average person to relate to yoga in a way that we all can to a bike - an activity most of learn as kids.

However, perhaps that's just my own bias discounting this opportunity. After all, there are [reportedly 20.4MM people](http://www.yogajournal.com/uncategorized/new-study-finds-20-million-yogis-u-s/) in the U.S. practicing yoga, spending over $10B annually. Could there be a sizeable enough space carved out for live-streaming, instructor-led yoga classes? I'm sure lots of people might be willing to give yoga a shot from the comfort and security of their own home.

Crossfit is perhaps another such opportunity, but the varied nature of the equipment needed may prove too big a barrier for most. Martial arts? Boxing? Dance? What else? Perhaps non-sporting activities such as cooking? Live and on-demand, in-home, healthy cooking classes?

## Personalized Coaching

With so much data on each user's fitness habits and the strong Peloton brand, this would seem to be a fertile space to explore. How many of the current 200K users would trust Peloton to engage in a personalized training program? We live in the over-played days of, "Uber for x" but life coaching in the U.S. is an [estimated $1B industry](https://www.ibisworld.com/industry/life-coaches.html) and it's highly fragmented, with over 12K firms in existence in the U.S. Similarly, as of 2014, there were [279K fitness trainers](https://www.bls.gov/ooh/personal-care-and-service/fitness-trainers-and-instructors.htm) in the U.S. As Peloton refines its live and on-demand streaming service, expands its library of fitness content, and uses its rider data to baseline users and provide personalized, fitness recommendations, this could serve as a logical jumping off point for Peloton to create a marketplace for certified personal trainers and coaches.

Ok, time to get back on the bike!!

