---
author: Jason Gowans
comments: true
date: 2013-04-26 22:31:21+00:00
layout: post
published: false
slug: the-pareto-principle-is-alive-and-well
title: The Pareto Principle Is Alive And Well
wordpress_id: 148
---

The world of recommendations is an interesting business. Black box providers tout their ensemble learning approach while many businesses themselves construct elaborate strategies in the service of offering up the perfect recommendation to a customer. What's often left lagging behind though is a well thought out approach to optimization. At best, we might see basic A/B tests being run but rare is the case where the entire system is capable of continuous end to end testing, learning, and optimization of the strategies and algorithms themselves with minimal human intervention.

As a developer, the temptation is to jump headlong into the task of writing algorithms, or maybe using something like Mahout, and coming up with the most exotic solution possible. The reality though is that a more effective approach might be to start with a simple collaborative filter

The clusters that you built, the collaborative filtering algos that were used (perhaps in Mahout) were all created at a single point in time. It's important that these are constantly being tuned to improve the quality of the recommendations, right? However, one alternative is to
