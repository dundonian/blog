---
author: Jason Gowans
comments: true
date: 2013-12-05 19:11:04+00:00
layout: post
slug: start-up-where-the-action-is
title: Start Up Where The Action Is
wordpress_id: 938
tags:
- digital marketing
- marketing
- retail
- Startups
---

[![needle_haystack](http://jasongowansdotnet.files.wordpress.com/2013/12/needle_haystack.jpg?w=225)](http://jasongowansdotnet.files.wordpress.com/2013/12/needle_haystack.jpg)

Not a week goes by without hearing of the latest big data analytics startup that's going to optimize a company's marketing spend. While it's true that there's an incredible amount of innovation happening in marketing just now, what's sometimes lost or perhaps ignored is the size of the potential impact of the startup to a retailer's business. Depending on who you talk to, digital marketing as a [percent of revenue is on average at 2.5%](http://www.gartner.com/technology/research/digital-marketing/digital-marketing-spend-report.jsp) with US Retail [spending almost $10B](http://www.emarketer.com/Article/Retail-Industry-Remains-Largest-Spender-US-Digital-Advertising/1010187) a year.

Sounds like a big market, and it is, but keep in mind most of the money is going to the big guys like Google, Facebook, and any number of the big publishers such as AOL, Yahoo!, and MSN.

So, let's consider a hypothetical startup that's in the business of optimizing a company's social engagement. Further, let's assume that they're trying to sell their solution to a retailer doing $1B in revenue. If we accept that this retailer spends 2.5% of revenue on digital, that means they have a total budget of $25MM which is broken out thus, based on the analysis of [how marketers allocate their digital marketing budgets](http://www.gartner.com/technology/research/digital-marketing/digital-marketing-spend-report.jsp):
<table cellpadding="0" width="262" cellspacing="0" border="0" > 
<tbody >
<tr >

<td width="134" style="text-align:left;" height="15" >**Digital Budget**
</td>

<td width="45" style="text-align:left;" >**Percent**
</td>

<td width="83" style="text-align:left;" >**Dollars**
</td>
</tr>
<tr >

<td height="15" >Online advertising
</td>

<td align="right" >12.5%
</td>

<td align="right" >$3,125,000
</td>
</tr>
<tr >

<td height="15" >Content creation
</td>

<td align="right" >11.6%
</td>

<td align="right" >$2,900,000
</td>
</tr>
<tr >

<td height="15" >Corp site
</td>

<td align="right" >10.7%
</td>

<td align="right" >$2,675,000
</td>
</tr>
<tr >

<td height="15" >Search
</td>

<td align="right" >10.7%
</td>

<td align="right" >$2,675,000
</td>
</tr>
<tr >

<td height="15" >Email
</td>

<td align="right" >9.6%
</td>

<td align="right" >$2,400,000
</td>
</tr>
<tr >

<td height="15" >Analytics
</td>

<td align="right" >9.5%
</td>

<td align="right" >$2,375,000
</td>
</tr>
<tr >

<td height="15" >Social
</td>

<td align="right" >9.4%
</td>

<td align="right" >$2,350,000
</td>
</tr>
<tr >

<td height="15" >Mobile
</td>

<td align="right" >7.4%
</td>

<td align="right" >$1,850,000
</td>
</tr>
<tr >

<td height="15" >Commerce experience
</td>

<td align="right" >7.2%
</td>

<td align="right" >$1,800,000
</td>
</tr>
<tr >

<td height="15" >Video production
</td>

<td align="right" >5.9%
</td>

<td align="right" >$1,475,000
</td>
</tr>
<tr >

<td height="15" >Company blog
</td>

<td align="right" >5.3%
</td>

<td align="right" >$1,325,000
</td>
</tr>
<tr >

<td height="15" >Other
</td>

<td align="right" >0.2%
</td>

<td align="right" >$50,000
</td>
</tr>
<tr >

<td height="15" >**TOTAL **
</td>

<td align="right" >**100%**
</td>

<td align="right" >**$25,000,000**
</td>
</tr>
</tbody>
</table>


Taking the oft-cited 400% target ROAS (I'll save the ROAS conversation for another post), this would mean that this startup is in the business of optimizing $9.4MM of this particular retailer's revenue ($2.35MM * 400%). If we further assume that this retailer is running net margins of 10%, this means that social contributes just $940K to this retailer's bottom line. Even if this startup can improve the retailer's social ROAS by 50%, we're still only talking about an incremental $470K to the bottom line.




Now, you compare that to the same $1B retailer whose gross margins are perhaps 40%, and their cost of revenue is $600MM. If you're a startup in the business of optimizing say, merchandise allocation, it's highly likely that your company's potential impact is substantially larger than the $470K this social engagement startup could drive to the bottom line.




To be clear, I'm not saying that being in the business of optimizing digital marketing is a bad idea. Only that, when you set out to create a company, have in mind not just what's hot and can attract funding (and is therefore also massively crowded), but also where there's potential to create truly a huge impact for your target customers, perhaps less competition among other startups, and in an area more ripe for disruption...





[![Enhanced by Zemanta](http://img.zemanta.com/zemified_h.png?x-id=ec101e39-5c0c-4bfa-85a9-563257945190)](http://www.zemanta.com/?px)
