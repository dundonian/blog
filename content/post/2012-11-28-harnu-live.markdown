---
author: Jason Gowans
comments: true
date: 2012-11-28 19:13:00+00:00
layout: post
slug: harnu-live
title: Harnu Live

---

Since we started Harnu, we've gone through a couple of pivots. The first was an early realization that we had no hope of growing Harnu if everything was one to one conversations off the bat. We still believe that empathy has the best chance of taking place in a one to one conversation, but that just can't be the starting point. The second has been a realization that people asynchronously asking questions to other nations is also unlikely to grow into anything meaningful. And so, we've started pivoting Harnu towards something called Harnu Live.  
  
Harnu Live is about giving people the ability to create their own global conversations, invite their own audience, and decide if they want to open it up to a wider audience on Harnu. A good comparison might be Huff Post Live where they host discussions with experts. On Harnu, we're giving that power to the people themselves.  
  
But you have to crawl before you walk and run etc. So, yesterday we hosted our first Harnu Live session ourselves, and what an amazing moment it was. Not sure what to expect, we invited a great student from Tripoli called Asem Mahmod to be our inaugural guest. Suffice to say, multiple global conversations happening simultaneously and hosted by people themselves is our path towards empathy and of course, growth. Without further commentary, here's the transcript (and yes, we need to make it easier to present these transcripts externally.) Baby steps ;-)  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/11/screenshot2012-11-28at11-04-21am.png?w=208)](http://jasongowansdotnet.files.wordpress.com/2012/11/screenshot2012-11-28at11-04-21am.png?w=208)

[![](http://4.bp.blogspot.com/-e2mjP9-lcI4/ULZhA-WgRZI/AAAAAAAAANA/l0028C3wNhs/s1600/2.png)](http://4.bp.blogspot.com/-e2mjP9-lcI4/ULZhA-WgRZI/AAAAAAAAANA/l0028C3wNhs/s1600/2.png)

[![](http://jasongowansdotnet.files.wordpress.com/2012/11/3.png?w=233)](http://jasongowansdotnet.files.wordpress.com/2012/11/3.png?w=233)

[![](http://jasongowansdotnet.files.wordpress.com/2012/11/4.png?w=300)](http://jasongowansdotnet.files.wordpress.com/2012/11/4.png?w=300)

  


And if you read this far, here's a copy with just the text in case you want to cut and paste it:

  


        

**_Harnu Live Transcript with Asem Mahmod from Tripoli_**

  


November 27 2012

  


**@asemmahmod, i want to ask what is the generally perceived idea of a successful person in your country? for example in india, the stereotype is like good job, kids, family and that's it. how's it it there?******

  


I can't deny that we don't have stereotypes here, but not for a successful person. Everyone basically chases their dreams, no matter what they are @saifwilljones

  


**I'll start. Asem - would love to know what the major sports are in Libya. Are there major leagues in the country? ******

@brent Do you have any idea how Brazilians love football/soccer? Well, Libyans are completely in love with football. I'm actually not a fan of this sport, but everyone I know plays football all the time. At school, after school, and even before school! No exaggeration!

  


**What is the music scene like in Libya? and what kind of music do you really love? ******

Stefenymarie, music plays a huge role in our culture. Everybody loves our traditional Libyan music, and we play it every time. When we have weddings, when we celebrate the revolution's anniversary. And pretty much each time we want to have fun and dance. The only kind of music I like is pop. That's not traditional :)

  


**Hi @asem, I have a question. Your parents and elders may remember what life in Libya was before Gaddafi, but you were born in that world so all you may have heard are stories about the old times. After last year's conflict and with Libya taking a new path, What was the impact on a 16 year old soon after the country's sudden change of direction? What kind of opportunities you now see that may have opened (or closed) to someone like you?**

armando, great question! I feel so lucky and blessed after the revolution. It really opened a lot of opportunities to people like me. This change has affected me deeply. I have to say that I was desperate when Gaddafi was ruling the country. There's nothing to do. Young people were basically wasting time hanging out around the corners of streets. I can't respond to that in regards of politics, but in general, I believe that a bright future is waiting for our nation, and I'm very optimistic about that.

  


  


**@asem: U may take this as your third question, How is the study structure there designed??? **

Like, More of the schools are from private sector or government sector??

  
 

Governmental free schools are more popular. Private schools are a little bit expensive. However, private schools have more efficient teachers than public ones. Is that what you asked about syedbarkat?

  


  


**Hi, Asem-Are women encouraged to go to college and seek careers?******

Brentsmom, yes they are. And that encouragement increased even more after the revolution. I have a sister who works and studies at the same time. Women are not forced to stay home or something like that.

  


**Is education an important part of "success" in Libya?******

Tamara, answering your question, I have to say no actually. Many many Libyans here have dropped out of school at a very young age and worked on developing a certain craft and now they are as successful as the educated.

  


**What did the revolution mean to you, or do for you? Did it change your perspective or view(s) on life as a young man in Libya? ******

Tamara, the revolution means a lot to most Libyans. This revolutions stopped a dictator from ruling a country for 42 years. After the revolution, I realized that I can take a part in building the new Libya, and I know I can make a difference, which was impossible under Gaddafi. However, I can't deny that there are Libyans who still support Gaddafi and his regime even though he's dead now.

  


**What is the general feeling in Libya toward the United States?******

I personally love the United States. All I know is that Libyans are looking forward to co-operating with the United States in building the country. And we're all excited about this since the US is probably the most civilized country in the world.

  


**Cześć Asem! Jaki instrument muzyczny jest najbardziej popularny w Libii?******

Andrzej, what musical instrument is most popular in Libya? Hmm.. Do you know what a goblet drum is? Well, that's it. We take it with us everywhere even when we go on picnics!

**Asam, anytime I travel anywhere I like to try out local traditional food. Is there any specific food dish you love or that Libyans love? If so what would that be, I'm just wondering what that is in Libya. ******

My favorite traditional food.. Well, I like "mbakbka", don't waste your time pronouncing this. It's basically pasta but in our own Libyan way. We love pasta here and I think the reason why is because we were colonized by Italians many years ago. There's also Bazeen which is popular also, but a bit difficult to make.

  


**What are the most popular television shows in Libya?******

Brent, that's a hard question. I don't want to generalize, but there's one show that is popular during Ramadan. It's called Bab Al-Hara. It's Syrian :)

  


**@asem:This is a problem faced by me in India,So, I'm interested in this. What r people's perceptions about the friendship between Opposite sexes????******

Syed, friendship with the opposite sex exists, but not publicly. It's just something we talk about with family. Unless they are a family, it's not common to see a boy and a girl walking in the middle of the street and talking. That's simply because of our religion which is Islam.

  


**What do you do during Ramadan? Help us understand what that looks like!******

Ramadan is a 29 to 30 day long month in which we fast from sunrise to sunset. No eating, no drinknig and no sexual relations or any kind of sinful speech and behavior. When the sun sets, the whole family gathers at one table, eats, prays, and have fun!

  


**@Asem, If I want to go to Libya as a tourist, what are the top 2-3 places you would recommend?******

You gotta visit the Old City in Tripoli. The first time I actually visited it was a few weeks ago and I'm still amazed!

  


**Here's one from us Asem - What’s the economy like at the moment? What sort of opportunities are there for young people to get a job in Libya?******

Oh. The economy means oil to me :) All I know is that oil production is increasing and it's even better than before the revolution now. I don't know about jobs in Libya, sorry!

  


**@Asem, Do you have starbucks yet in Tripoli?? :)******

armando, no :( and that sucks! They're actually not even thinking of opening a branch here after I contacted them.

  


**Ok, if no more questions, we’d like to thank everyone once again for coming to our first Harnu Live. I think it's fair to say that for the Harnu team we’ve learned quite a lot on how to make the next ones even better, and really appreciate everyone’s willingness to try this out. If you have ideas for future Harnu Live sessions please let us know by sending a Private Message to the Harnu connection or emailing us at info@harnu.com. **

**

  


**Special thanks to Asem for agreeing to be our first guest. Really appreciate the chance to learn even just a little about Libya. Thank you! By the way, you can always keep in touch with Asem by sending him a private message on Harnu. **

**  
 

I appreciate this chance so much. I'm glad I taught people from all over the world a little bit on Libya, and that only happens on Härnu. Thank you everyone!
