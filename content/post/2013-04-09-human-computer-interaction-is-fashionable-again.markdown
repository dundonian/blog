---
author: Jason Gowans
comments: true
date: 2013-04-09 16:55:10+00:00
layout: post
slug: human-computer-interaction-is-fashionable-again
title: Human Computer Interaction is Fashionable Again
wordpress_id: 187
tags:
- curation
- fashion
- human-computer interaction
- machine learning
- retail
---

At the recent [Strata conference](http://strataconf.com/strata2013/public/schedule/detail/27190) I attended, one presentation in particular stood out. Eric Colson, Chief Analytics Officer of [StitchFix](https://stitchfix.com/), gave a quick 10 minute talk about his company's approach to product recommendations. Stitchfix is a company that sends its customers 5 hand-picked items and the customer only pays for what she decides to keep. The success of this company is wholly dependent on its ability to recommend the right items - _thoughtful consideration_ is how he characterized it. While that's interesting in and of itself, what struck me most was that Colson, a former VP of Data Science at Netflix, has adopted a pragmatic approach of algorithms + human stylists to create a complementary and reinforcing system.

While Amazon has of course paved the way with machine learning in a commercial environment, more companies are waking up to the fact that they are sitting on untapped human intelligence within their organizations, and the path forward to competitive advantage is predicated on weaving that human intelligence into the fabric of the organization.

Whether it's Twitter [using humans to adjudge the context of fast-moving keyword trends](http://www.nytimes.com/2013/03/11/technology/computer-algorithms-rely-increasingly-on-human-helpers.html?pagewanted=all&_r=0), search engines incorporating social search results (which I [wrote about last year](http://startupdad.blogspot.com/2012/09/the-trouble-with-social.html)), retailers [leveraging the voice of their merchants](https://www.linkedin.com/today/post/article/20130408095900-12921524-one-big-mistake-retailers-are-making-online-not-thinking-like-their-customers?_mSplash=1), or CD Baby creating their [listening algorithm](http://sivers.org/hi), there's an undeniable trend emerging of human + computer interaction.

In the data lab at Nordstrom, our thinking around machine learning and scalable systems is heavily influenced by challenging ourselves to think about how we can incorporate 112 years of human intelligence built up around this company. However, if we think of our stylists and merchants as consumers, the question always has to be what value we're giving back to them. In the same way that Facebook provides a service to consumers in exchange for mining their data that's in turn monetized, so too must we always be thinking about the service we're providing back to the merchants and stylists in exchange for mining their intelligence.
