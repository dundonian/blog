---
author: Jason Gowans
comments: true
date: 2013-10-16 05:39:28+00:00
layout: post
slug: amazon-and-closed-loop-publishing
title: Amazon And Closed Loop Publishing
wordpress_id: 883
tags:
- amazon
- ebooks
- ereading
---

 

[![Image](http://jasongowansdotnet.files.wordpress.com/2013/10/book_burning.png?w=650)](http://xkcd.com/750/)

Last week, Amazon [purchased a company](http://www.geekwire.com/2013/amazon-buying-tenmarks-jumping-education/) called Ten Marks that provides personalized Math curriculum, and it reminded me of the promise of eBook readers. By now, Amazon is likely sitting on a goldmine of reading habits, and the question is what to do with it. It seems that the best way forward for Amazon to be in every classroom in the world is to offer the promise of an optimized reading experience. Whereas when I first wrote about this, I was [thinking of the readers](/2013/02/14/facebook-books-the-future-of-social-reading/), I now realize that the opportunity is more likely with the publishers.

Just as Amazon is trying to [optimize the business of 2 million sellers on its marketplace](http://techcrunch.com/2013/08/31/how-amazon-is-tackling-personalization-and-curation-for-sellers-on-its-marketplace/), so too can it help publishers write better books. Amazon knows all about trying to optimize conversion and could easily extend that to optimize for comprehension and completion of texts. Knowing that a sizable percentage of the readers of a given book abandon at a particular point, Amazon could alert the publisher to that fact with a view to improving it next time. In fact, what's to say publishers couldn't A/B test different versions of the same book, perhaps starting with limited distribution advanced copies?

eBooks have so far brought more efficient distribution and storage, but surely closed loop publishing is just around the corner...
