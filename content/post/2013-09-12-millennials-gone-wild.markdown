---
author: Jason Gowans
comments: true
date: 2013-09-12 17:25:31+00:00
layout: post
slug: millennials-gone-wild
title: Millennials Gone Wild
wordpress_id: 756
tags:
- empathy
- millennials
- social media
---

[![Image](http://jasongowansdotnet.files.wordpress.com/2013/09/att.png?w=624)](http://jasongowansdotnet.files.wordpress.com/2013/09/att.png)

Yesterday, AT&T crassly tweeted a reference to 9/11 while conveniently plugging their phones. However, they weren't alone in scoring these own goals. In the quest for elusive likes, favorites, comments, and retweets brands everywhere threw decorum out of the window and vied for attention with their faux reverential posts.

However, while it's easy to [vilify these evil brands](http://www.fastcompany.com/3017252/fast-feed/att-gets-twitter-shamed-into-deleting-its-awful-9-11-commemorative-tweet), what seemed to be missed is the fact that most brands are staffed by younger workers who "get" social - millennials. [The Me Me Me Generation](http://www.professorbernstein.com/Home/MA_Assignments_files/The%20Me%20Generation%20--%20Printout%20--%20TIME.pdf). Could it be that yesterday's social media maelstrom was a function of a) a lack of historical perspective and b) a general decline in empathy ?

Consider that in 2001, those now manning the social media handles of brands were probably not yet teenagers. However painfully seared into the memory of millions of Americans, it's worth considering that for millions of younger Americans, it's a sad event that happened a long time ago, but only in a conceptual and abstract sense - not in a visceral, first-hand, sense of the experience. For example, how many people today have a true sense of empathy for the millions of Polish people who were caught up in the horrors of World War II? It's a historical event to be studied and read about, but it's difficult to feel anything approaching empathy for an event that has no bearing on our day to day lives in 2013.

[Roman Krzanaric](http://www.romankrznaric.com/wp-content/uploads/2011/12/Wired-World-in-2013-Empathy.pdf) asserts that we're about to embark on an empathic revolution. However, we are now spending at least [1 in 5 online minutes](http://www.comscore.com/Insights/Press_Releases/2011/12/Social_Networking_Leads_as_Top_Online_Activity_Globally) on social media and we are increasingly experiencing events through a digital filter. In fact, kids are now spending upwards of [10 hours per week](http://globalkidsstudy.com/2013/02/14/how-digital-and-tech-savvy-are-todays-global-youth/) on a computer / digital device.

So, as more of our experiences shift to digital and less face-to-face, first hand experiences, might it be possible that the younger generation finds it increasingly difficult to empathize with others and the potential for more serious mishaps correspondingly rises?
