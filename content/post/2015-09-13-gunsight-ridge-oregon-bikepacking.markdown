---
author: Jason Gowans
comments: true
date: 2015-09-13
layout: post
title: Oregon Bikepacking - Gunsight Ridge
---
Earlier this year, I bought [Specialized AWOL Comp](http://www.specialized.com/us/en/bikes/adventure/awol/awol-comp) that I've mostly been using to get to and from work, with one exception, when I rode from [Seattle to Snoqualmie Pass](http://ridewithgps.com/trips/5850988). However, I've been itching to take go a bit further afield, and push myself a bit more. I was especially inspired by the [Tumblr blog](http://wearegoingawol.tumblr.com/) of AWOL adventures.

So, with this in mind, I set about trying to find a fun bikepacking trip, and together with my friend (and fellow co-founder of [Aerobatic](http://www.aerobatic.com/)), David Von Lehman, we started planning a trip. And that's when we came across the amazing [Oregon Bikepacking site](http://www.oregonbikepacking.com/). After a bit of deliberation, we opted for trying out the [Gunsight Ridge route](http://www.oregonbikepacking.com/portfolio-posts/east-sider/), and man, what an amazing adventure it turned out to be!

The site describes a 153 mile route with over 16,000 feet of elevation gain across some of the best dirt roads in Oregon, including the historic [Barlow Road](https://en.wikipedia.org/wiki/Barlow_Road).

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/88440133@N07/21370961915/in/album-72157658140805680/" title="Gunsight Ridge - Oregon Bikepacking"><img src="https://farm6.staticflickr.com/5641/21370961915_39c1386b08_k.jpg" width="2048" height="1536" alt="Gunsight Ridge - Oregon Bikepacking"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

We drove the 3 hours down from Snoqualmie Pass to The Dalles in Oregon, and had a late breakfast at an excellent restaurant there called Petite Provence. While there, I idly asked one of the wait staff where we could park our car for a few days in town, and given the 48 hour limit on parking in town, she then called around and the pastor of the nearby Harvest Foursquare church said it'd be ok to park in his lot. With that kind of great start to the day, we set off around lunchtime and started making our way towards the town of Dufur across some lovely gravel roads. At least, it would have been lovely had David not endured the ignomy of 3 flat tires on the way there! Here he is at the side of the road pumping his tire up, right before he broke the pump.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/88440133@N07/21344526516/in/album-72157658140805680/" title="Gunsight Ridge - Oregon Bikepacking"><img src="https://farm1.staticflickr.com/658/21344526516_3eae2af7d9_k.jpg" width="2048" height="1536" alt="Gunsight Ridge - Oregon Bikepacking"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Thankfully, we made it to Dufur and managed to buy a new pump at the local hardware store. With the mechanical mishaps behind us, we headed towards Friend and its abandoned grocery store.

By now, it was getting late and the sun was starting to go down, so we pressed ahead, hoping we'd run across a decent place to camp. Just as we were about ready to give up, we stumbled across an incredible site just past mile 46, off Friend Road. We awoke in the morning to find some roving cows muscling in on our camp spot to drink from the nearby Jordan Creek.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/88440133@N07/21370817595/in/album-72157658140805680/" title="Gunsight Ridge - Oregon Bikepacking"><img src="https://farm1.staticflickr.com/574/21370817595_6c24b7b588_k.jpg" width="2048" height="1536" alt="Gunsight Ridge - Oregon Bikepacking"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

With some breakfast, we set off at 9am from our camp spot, and at first were heading down what felt like our own private paved driveway. Little did we know, the fun was just getting started. We soon found ourselves on dirt roads, carefully following the GPS tracks laid out in the Oregon Bikepacking site. Our immediate goal was to make it to Rock Creek Reservoir. While the reservoir was more or less dry, amazingly the spigot was still functioning and we slaked our thirst there for a while. This was after we'd bushwhacked our way through the "trail", narrowly avoided an angry rattlesnake, and hiked up past Badger Creek campground, trying to decide if the lone camper there was a serial killer or a meditative hipster. So far for the day, we'd traveled 21 miles in 3 or so hours and overall, were 67 miles into the trip.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/88440133@N07/21370887525/in/album-72157658140805680/" title="Gunsight Ridge - Oregon Bikepacking"><img src="https://farm1.staticflickr.com/770/21370887525_dd89961e10_k.jpg" width="2048" height="1536" alt="Gunsight Ridge - Oregon Bikepacking"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

From Rock Creek, we continued to follow the GPS tracks along the old Barlow Road which was truly an incredible experience. At mile 89, we came across a bridge that had been washed out and we had to push our bikes across the creek. The next 5 miles took us almost an hour...

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/88440133@N07/21344681836/in/album-72157658140805680/" title="Gunsight Ridge - Oregon Bikepacking"><img src="https://farm1.staticflickr.com/765/21344681836_62504fc315_k.jpg" width="2048" height="1536" alt="Gunsight Ridge - Oregon Bikepacking"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Undeterred, we carried on, hoping to make it to the much vaunted Jean Lake. By now, we were fairly exhausted. The combination of rutted dirt paths, carrying our bikes over felled trees, and the sheer elevation gains had taken their toll. At around mile 113 (67 for the day), we finally called it quits and once again, stumbled across a nice camping spot just off the trail at the end of some gravel road. It had been a long day, but we managed to get the tent up and boil some water just before the sun set.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/88440133@N07/21370989555/in/album-72157658140805680/" title="Gunsight Ridge - Oregon Bikepacking"><img src="https://farm1.staticflickr.com/677/21370989555_a7028e07bc_k.jpg" width="2048" height="1536" alt="Gunsight Ridge - Oregon Bikepacking"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Day 3, we awoke around 6:45am and were on the trail by 8am. We did eventually see the fabled Jean Lake but opted to keep going. Luckily, we also found a small spring where we were able to fill up our water bottles. Eventually, we made it to the top of the ridge and thus began the most incredible downhill that lasted more or less all the way back to The Dalles. The struggles of Day 2 were soon forgotten as our bikes chewed up miles and miles of dirt and gravel as fast as we'd dare. I did take one spill and despite a ripped shirt and some scrapes, it wasn't enough to wipe the smile off our faces. I cannot empahsize enough how incredible the downhill was. Epic doesn't begin to describe it. I'd love to come back with my Transition Covert and blast some of those trails in full downhill mode. As it was, me and my AWOL made the most of it!

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/88440133@N07/21371025415/in/album-72157658140805680/" title="Gunsight Ridge - Oregon Bikepacking"><img src="https://farm1.staticflickr.com/585/21371025415_0635d071c0_k.jpg" width="2048" height="1536" alt="Gunsight Ridge - Oregon Bikepacking"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

A little over 48 hours after we set off from The Dalles, we were back at the car, ready for one final meal at Petite Provence before driving back to Snoqualmie Pass. It was hard to describe to each other how we felt, but in the end, no words were really necessary. This was a trip that neither of us will soon forget.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/88440133@N07/21182787770/in/album-72157658140805680/" title="Gunsight Ridge - Oregon Bikepacking"><img src="https://farm1.staticflickr.com/754/21182787770_fd87923e4f_k.jpg" width="1536" height="2048" alt="Gunsight Ridge - Oregon Bikepacking"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

### GPS Route
<iframe src="//ridewithgps.com/trips/6473637/embed" height="500px" width="100%" frameborder="0"></iframe>

### Trip Details
- GPS at [http://ridewithgps.com/trips/6473637](http://ridewithgps.com/trips/6473637)
- Photos on Flickr at [https://www.flickr.com/photos/88440133@N07/albums/72157658140805680](https://www.flickr.com/photos/88440133@N07/albums/72157658140805680)

### Some of My gear
- Specialized AWOL Comp
  - Schmidt Dynamo Hub
  - Supernova S3 front light
  - Supernova rear light
  - Supernova The Plug
- Revelate Designs Viscacha seat bag
- Revelate Designs Ranger frame bag XL
- Revelate Designs Gas Tank bag
- Pass & Stow front rack
- Ortlieb Front-Roller Plus panniers
- Hilleberg Nallo 3 GT tent
- Western Mountaineering Summerlite sleeping bag
- Therm-a-Rest NeoAir XLite Sleeping Pad
- Katadyn Vario Water Filter
- Garmin 510

