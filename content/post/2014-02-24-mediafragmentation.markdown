---
author: Jason Gowans
comments: true
date: 2014-02-24 23:31:27+00:00
layout: post
slug: mediafragmentation
title: What Media Fragmentation Can Teach Retail
wordpress_id: 964
---

In a week where Facebook bought WhatsApp for $19B, many pundits latched on to 450MM WhatsApp users that Facebook had just acquired, with particular emphasis given to WhatsApp's penetration in International markets.

Realistically though, given Facebook's penetration, this is not about acquiring acquiring unique users but simply _retaining and acquiring more of existing users' attention_. Ben Evans astutely noted in [his post](http://ben-evans.com/benedictevans/2014/2/19/whatsapp-and-19bn), and there's also a paper authored in 2012 by [Professor James Webster](http://www.soc.northwestern.edu/webster/index.php) of Northwestern University and [Thomas Ksiazek](https://www1.villanova.edu/villanova/media/facultyexperts/asexperts/thomas_ksiazek.html) of Villanova University entitled [The Dynamics of Audience Fragmentation: Public Attention in an Age of Digital Media](http://www.soc.northwestern.edu/webster/pubs/Webster%20&%20Ksiazek%20(2012)%20Audience%20Fragmentation.pdf) that offers some insights on this.

In the paper, the authors took a network analysis approach to measure the degree to which audiences overlap between different media outlets. Among the top 236 media outlets in their study, almost all overlapped with each other among audiences. Interestingly, the authors also note that in a world of expanding choice, users will bias towards the highest quality and most popular media content.

So, in a world of expanding retail choices, what can retail learn from media?


## Competing For Attention


Richard Lanham, in his book, [The Economics of Attention](http://www.amazon.com/The-Economics-Attention-Substance-Information/dp/0226468674), said,


<blockquote>Assume that, in an information economy, the real scarce commodity will always be human attention and that attracting that attention will be the necessary precondition of social change. And the real source of wealth.</blockquote>


[http://www.press.uchicago.edu/Misc/Chicago/468828.html](http://www.press.uchicago.edu/Misc/Chicago/468828.html)

Just as Facebook and any other media property is competing for attention, so too are retailers. This suggests that while programmatic ad buying will of course continue to dominate marketing, there's also a case to be made that retailers need to create more engaging digital experiences - stories that inform and entertain, a point of view...reasons deserving of attention.

Amazon offers their own [unique approach](http://online.wsj.com/news/articles/SB10001424052702303636404579395443152132198) to commanding customer attention by aiming to expand the number of products available from their store-front.


## The Case For Differentiation


There are obvious parallels between retail and media, where there's been an explosion of new eCommerce entrants in recent years. Additionally, this expansion of choice is likely to also materialize in the brick and mortar world if the likes of JC Penney, Sears, and Barnes & Noble continue their decline. With a glut of cheap commercial real estate coming available in the near future, there'll be yet more opportunities for new retail entrants (with some of those same eCommerce companies going multi-channel).

In an era of pricing parity being the cost of doing business, customers will seek out differentiated, high-quality retail experiences. To deal with this, existing large retailers will have to focus on providing a best-in-class retail experience for _every single category_ in which they choose to compete, because if they don't, it's a guarantee that a Warby Parker equivalent will fill the void, offering differentiation to overlooked categories that retailers take for granted.


## The Persistence Of Popularity


Just as customers will seek out differentiated experiences, there's also a bias to go with already popular choices because a) it's already part of most people's retail repertoire, b) it offers a sufficiently high quality experience, and c) there's a social aspect to doing things that others are doing that offers opportunity for conversation around common topics.

Large retailers that are already popular have an opportunity to remain popular, provided they can retain a high quality customer experience. Easier said than done as [foot traffic in malls is declining](http://www.businessinsider.com/shopping-malls-are-going-extinct-2014-1). Will retailers have the staying power to invest in differentiated IRL experiences?


## The Importance Of Personalization


Given a world of essentially unlimited choice, the task of guiding customers will take on significantly more importance. In media, Webster and Ksiazek call recommenders and search systems, "user information regimes" where much of the media a user consumes is the result of recommendations. In retail, we're still in the infancy of personalization with many retailers not offering much more than basic product recommendations. As retailers increasingly compete for attention, the marriage of storytelling and personalization offers some interesting opportunities to engage, inform, and inspire.
