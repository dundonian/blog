---
author: Jason Gowans
comments: true
date: 2013-05-03 15:22:27+00:00
layout: post
slug: a-show-and-tell-culture
title: A Show and Tell Culture
wordpress_id: 208
---

At work, my favorite day is unquestionably Thursday. That's when we round off the week with a group show & tell where each team member has a chance to showcase an idea they've been working on to the rest of the team. For me, there are at least three major reasons why every team should adopt this practice:



	
  1. A  Forcing Function - Knowing that Thursday is around the corner always gives me a target to shoot for. It's also a good reason to be mindful of the Pareto Principle. Just get something good enough for the time being.

	
  2. Storytelling - What's the problem you're trying to solve? Why does this matter? How can you get the audience to take a mental leap of faith from the crude prototype you have built all the way to the end state that you've imagined?

	
  3. A Picture Is Worth... - Telling someone you've built a recommender system is not nearly as powerful as simply showing it in action, however crude it might be in its formative stages. Take a little time to build a front-end that shows your idea in action.


On a personal level, this week I've had an absolute blast learning to work with a Python-Instagram library and showing off a trite little app to the team. It doesn't matter that my coding skills are miniscule compared to the devs on our team. What matters is that something was built - we show, not just tell...

So, regardless of where you are in a corporation's hierarchy, whether you're an admin, an analyst or a VP, learn to code, even just a little bit, bring your ideas to life, and inspire those around you to do the same!
