---
author: Jason Gowans
comments: true
date: 2012-11-23 07:08:00+00:00
layout: post
slug: why-startups-need-a-narrative
title: Why Startups Need a Narrative
---

The mobile web has given us the power to answer almost any question in real-time. However, forming an opinion is usually something that takes a bit longer than a quick Google or Siri search, particularly so if it's a complex topic.  


  


Take for example the recent violence between Israel and Palestine. Who's to blame? What really happened this time? Why now? What's the history of this conflict? Is there any hope for a long-term peaceful solution? If you live in America, chances are you'll read a version of this conflict in mainstream media that strongly supports Israel with the narrative being that they are defending themselves against terrorists.

  


Against this backdrop, when we originally thought of Härnu, one of the canonical use cases we imagined was the scene of a teenager sitting at the Thanksgiving dinner table having to listen to _that_ drunk uncle who has an opinion on everything, drone on about _those_ arabs and muslims in less than friendly terms. In our fantasy world of Härnu, our teen pulled out her phone and rebutted her uncle's baseless assertions by recounting conversations she'd had with her Egyptian, Palestinian, and Pakistani friends she'd met on Härnu. Take that Fox News!

  


Fast forward a year and millions of Americans are sitting down to dinner where inevitably some of those tables will include _that_ uncle and unfortunately for those teens (and for us), our ambition for Härnu remains unfulfilled. In the past year, we've formed a company, we've created a product, and we've built a community in more than 100 countries. However, we've also dealt with more than our fair share of setbacks as we've worked so hard to build momentum.

  


I've come to learn that what most people warn is in fact true - building a consumer-facing startup is unbelievably difficult. Indeed, if it wasn't for our steadfast belief in our vision for the world, it's quite possible we'd have given up long ago.

  


So, on this Thanksgiving, despite the difficulties that we've faced so far and the many obstacles that lie ahead of us, I'm thankful that the Härnu narrative has persisted. Maybe next year, if we continue to work hard, we'll realize our ambition of American teens everywhere being able to chat with anyone around the world, gain a whole new world of perspective, and perhaps shut that uncle up once and for all.

  


  

