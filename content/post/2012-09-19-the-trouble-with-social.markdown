---
author: Jason Gowans
comments: true
date: 2012-09-19 07:52:00+00:00
layout: post
slug: the-trouble-with-social
title: The Trouble With Social
---

When Seattle hip-hop duo, [Common Market](http://en.wikipedia.org/wiki/Common_Market_(band)), penned [Trouble Is](http://youtu.be/wwLXfHMqvZU), they defiantly asked "...this is hustle biz, what the trouble is?" While for them, the "...trouble is love don't want you," in social, the trouble is the hustle has been focused on all the wrong things. [Paul Kedrosky](https://twitter.com/pkedrosky) said it best in a recent tweet and judging by the number of favorites and retweets, it obviously struck a chord.  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/09/dc9be-screenshot2012-09-18at7-31-15pm.png)](https://twitter.com/pkedrosky/status/248214180803526656)

  


Whether the payload is news, apps, games, or search results, the prevailing approach today is to mine someone's friends, apply some simplistic model that presupposes that we are like our friends, and call it good. While it's well established that [similarity breeds connection](http://aris.ss.uci.edu/~lin/52.pdf), it's worth remembering that our homophily merely _occurs at a higher rate_ than between dissimilar people. In other words, while our friends are similar to us in many ways, we all have outliers, and it's these outliers that social search has done a really poor job of addressing so far. This is true of not just friends but also interests.

  


The question though is how hard someone like Paul is willing to work for his content? Most techies believe that curation is the answer and the frustration expressed by Paul is solvable with more data and better algorithms. However, what they tend to discount is that curation is always going to be imperfect because no algorithm can anticipate my need without fail. Hypothetically, if such an algorithm were possible, would a tech company even implement it? If we found what we wanted 100% of the time on Google's first page of results, what would happen to their revenue? Could any increased CPC compensate for the loss of impressions on subsequent results pages? I doubt it. I believe Google's in the business of optimizing, but not too much. Speculation aside, the problem is that we're complex and sometimes unpredictable. Our needs are fluid and we push ourselves to discover the unknown.

  


So, if curation nor search are the silver bullets, might social search be? Facebook would have the world believe that we are like our friends because that's the sandbox they currently have to play with and they need brands to believe that. Almost everyone else in the space is following them with their friend based recommendation engines. Bing, anyone? At [Harnu](http://www.harnu.com/), we're taking a different approach by trying to encourage new connections. Want to know what's going on in Syria right now? Talk to a Syrian. Get a local's perspective. I don't know about you, but my friends are not useful for this class of discovery. That's why we built Harnu.

  


For us, while we may be taking a novel approach to social discovery, our uphill battle is going to be achieving scale for the foreseeable future. Facebook obviously has the scale. The question is whether they'll take a novel approach to social search or serve up the same old friend recommendations as everyone else. If they expand results to consider people that are not friends and let people connect with these strangers, then folks like Paul Kedrosky may get their wish as a massively long tail of people and content starts to come into play with the trade-off that Paul would have to do a some content sifting himself.

  


Most likely though, this would have the potential to alienate a lot of Facebook users unless they specifically opted in to that sort of feature. However, it'd also have the potential to dramatically expand Facebook's social graph. In my case, instead of the 460-something friends I have today, I'd set out to add at least one friend from every country in the world - something I'm able to do today on Harnu in the quest for perspective and knowledge that none of my friends is talking about.

  


As an example, on Harnu, I learned from someone in Belarus last week that it's now illegal there for anyone to gather information without accreditation. Crazy but true...

  


<blockquote>В последнее время дошло до того, что в стране запрещено незаконно собирать информацию. Только при аккредитации. Вам будет смешно, но это так.</blockquote>
