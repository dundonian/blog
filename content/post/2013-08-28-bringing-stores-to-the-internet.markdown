---
author: Jason Gowans
comments: true
date: 2013-08-28 05:17:16+00:00
layout: post
slug: bringing-stores-to-the-internet
title: Bringing Stores to the Internet
wordpress_id: 647
tags:
- recommendations
- retail
- technology
---

When I was a kid in high school, I used to sell women's shoes at a department store called [Arnott's](http://en.wikipedia.org/wiki/House_of_Fraser) back home in Dundee, and the thing I remember the most aside from the many gnarled feet of Scottish women was the mad dash to the back room to find the shoes and get back before she took off. In the case of Arnott's, we sold perhaps 40 different styles of shoes on the floor, and during the course of a busy Saturday, I came to know exactly which styles in which sizes and colors we had in the back, and where every single one of the shoe boxes were (hint: they weren't always where they were supposed to be). 

As retailers and startups focus on bringing the Internet into the stores, the question that should be obvious but isn't always fully answered is whether or not some new tech introduces friction into the selling or buying process. Whichever side of the equation your tech lives on, the value proposition has to be so unbelievably compelling either for the salesperson or the customer, otherwise it's just gratuitous and unhealthy friction being inserted into the buy flow. In the case of selling shoes, anything that causes a salesperson to be fumbling with an app while they're scrambling to serve a customer is going to be a tough sell.

What's perhaps easier and less fraught with friction is bringing the stores to the Internet. Obviously most retailers by now are selling online, and we're also now more frequently seeing omni-channel tactics play out such as BOPIS (Buy Online, Pick-up In-Store). However, for the most part, we're still not seeing stores' entire assets being surfaced online. Whether you're Home Depot, Whole Foods, or Victoria's Secret, these stores are filled with expert sales associates whose knowledge remains mostly in their heads, and whose scale is limited to however many customers they can serve in the store during the course of a shift. The trick is in curating that knowledge in a way that does not involve new friction and scaling it beyond the four walls of the store they happen to work in.

So, while retailers work on bringing the Internet into the stores, those who can successfully bring the stores to the Internet stand to win huge. 
