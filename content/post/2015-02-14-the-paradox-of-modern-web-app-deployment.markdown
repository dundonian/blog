---
author: Jason Gowans
comments: true
date: 2015-02-14 05:47:51+00:00
layout: post
slug: the-paradox-of-modern-web-app-deployment
title: The Paradox of Modern Web App Deployment
wordpress_id: 1080
---

In the world of web development, such is the pace of innovation that barely a week goes by without an announcement of a new JavaScript framework. At the time of writing, there are more than 1,500 job descriptions in the U.S. on LinkedIn containing the word “AngularJS” and 750 containing the word “Ember.” Major tech companies have been at the vanguard of the single page app adoption curve, but increasingly, we’re now seeing large corporations embrace this trend be it Angular, Ember, or more recently React.

So, it comes as something of a surprise that while we’re seeing widespread adoption of modern web app development, there’s not more innovation when it comes to deployment of those web apps.

Indeed, the paradox of deploying these web apps is that most deployment options today are both overkill and inadequate at the same time. They’re overkill in terms of both price and complexity since many still entail provisioning an entire virtual machine for what amounts to mostly serving static content, and they’re inadequate with regards to the features endemic to this style of development. I would contend that this holds true whether one's talking about deploying internally within an enterprise, or externally using a third party service.

For public deployment, Heroku remains a default choice for many web developers. However, while that choice made perfect sense 5 years ago when everyone was building end to end rails apps with Postgres backends, that’s increasingly not the situation for most web apps being built today. Indeed, with the availability of BaaS such as Firebase and Parse, as well as the proliferation of developer APIs for everything, a modern front-end developer can now build full-featured apps without ever needing to roll any server-side code of their own.

So, if we don’t need VMs for our web apps, what are the choices? Well, more enterprising developers can deploy to an [AWS S3 bucket](http://docs.aws.amazon.com/gettingstarted/latest/swh/website-hosting-intro.html) or [Github Pages](https://pages.github.com/) can be a good choice for quick public projects, and best of all it’s free. However, with Single Page Apps on S3 or Github Pages, there’s a common set of features that devs need that either aren’t available or come at a cost of increased complexity such as SEO snaphots, API proxy, OAuth, blue/green deployments, and more.

For public deployments, [Aerobatic](www.aerobatic.com) (the company where I'm a co-founder) and [Divshot](www.divshot.com) are legitimately focused on these web apps and provide many of those relevant features. For the enterprise though, what choice do they have?

Instead of VMs or scripts to provision infrastructure on AWS for these web apps, would it not be better to rethink the whole application deployment scenario entirely? Meteor is onto something when they imagine a [multi-tenant deployment platform](https://www.meteor.com/blog/2012/07/25/meteors-new-112-million-development-budget) that can run inside an enterprise.


<blockquote>"Galaxy will be a product that the operations department at a large company might buy. It'll be an enterprise-grade, multi-tenant hosting environment for Meteor apps. In other words, it'll let you run a private, centrally controlled "meteor deploy"-like service for your company, on your own hardware. You'll be able to manage how your apps are distributed across your datacenters, perform capacity planning, and enforce controls and policies that are appropriate to your organization. Google and Facebook have these tools — why shouldn't your organization?"</blockquote>


However, this presumably would depend on a developer using the full Meteor stack and the reality is that most companies are a heterogeneous mashup of lots of different javascript, services, APIs, and databases. Seems that enterprises would want to have a deployment platform that can play well with whatever frameworks and databases its developers are using. The whole point is that web apps are supposed to be composed of services where the backend can be developed independent of the front-end. [Nicholas Zakas talked about that very notion back in 2013](http://www.nczonline.net/blog/2013/10/07/node-js-and-the-new-web-front-end/).

On Meteor, perhaps Tom Dale said it best in this tweet:


<blockquote>"Meteor as an idea is good, but thinking 1 org can build the best view layer, best data layer, best pkg manager, etc takes a lot of hubris.""
 — Tom Dale (@tomdale) [August 29, 2014](<a href="https://twitter.com/tomdale/status/505175048643440641">https://twitter.com/tomdale/status/505175048643440641</a>)
</blockquote>




And again in a [recent blog post](http://tomdale.net/2015/02/youre-missing-the-point-of-server-side-rendered-javascript-apps/):


<blockquote>"Unfortunately, when most people think of client-JavaScript-running-on-the-server, they think of technologies like Meteor, which _do_ ask you to write both your API server (responsible for things like authentication and authorization) and your client app in the same codebase using the same framework. Personally, I think this approach is a complexity and productivity disaster, but that deserves its own blog post."</blockquote>


So, if not Meteor, then what or who? The company that can upend the world of enterprise application deployment and make it easy for everyone...well that's one worth building, don't you think? That's the dream we're chasing here at [Aerobatic](www.aerobatic.com). More on that soon.
