---
author: Jason Gowans
comments: true
date: 2012-07-15 18:47:00+00:00
layout: post
slug: savehollis
title: '#savehollis'
wordpress_id: 392
---

As I was waiting for my wife to finish work on Friday, I sat on a plush sofa in her large office building's lobby. Below is my live tweeting of someone being let go by two HR people as I sat there.  
  
This was the first time I'd live-tweeted anything but it did cause me to think how powerful it is when people are doing that in real-time around events that really matter. The unfiltered, stream of conscious text goes some way towards making us feel like we're experiencing the event - much more so than a carefully crafted newspaper article...  
  
Tweets are in reverse chronological order, so start from the bottom.  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/07/hollis2.png?w=257)](http://jasongowansdotnet.files.wordpress.com/2012/07/hollis2.png?w=257)

  


[![](http://jasongowansdotnet.files.wordpress.com/2012/07/savehollis1.png?w=213)](http://jasongowansdotnet.files.wordpress.com/2012/07/savehollis1.png?w=213)

  
  
  

