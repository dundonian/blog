---
author: Jason Gowans
comments: true
date: 2013-06-24
layout: post
slug: is-the-future-of-retail-in-finance
title: High Frequency Clienteling - Is The Future Of Retail In Finance?
wordpress_id: 324
tags:
- clienteling
- recommendations
- retail
---

The recent [news](http://www.nature.com/news/google-and-nasa-snap-up-quantum-computer-1.12999) of Google and NASA buying a quantum computer reminded me of the crazy world of high frequency trading. In case you ever wondered, Wired wrote a [great piece](http://www.wired.com/business/2012/08/ff_wallstreet_trading/all/) on that last year, although some are now predicting that we've reached [peak HFT](http://www.advancedtrading.com/algorithms/have-we-hit-peak-hft/240156447) and Business Week recently charted its [rise and fall](http://www.businessweek.com/articles/2013-06-06/how-the-robots-lost-high-frequency-tradings-rise-and-fall).

What's fascinating about HFT to me is that the players seek to exploit market inefficiencies quicker than anyone else and for a while, it was insanely profitable. However, over time, they became a victim of their own success. As HFT became more prevalent, volatility declined, and so too did profits of HFT firms to the point where the CEO of one of the largest HFT firms recently declared that “Speed has been commoditized."

In the world of retail, most companies are only now beginning to embark on a real data-driven journey where their business is run in large part on the strength of their ability to predict the future better than their competitors - will that customer click on the recommendation, will this product sell well, what will be the key trend for next year? Right now much of the attention is focused on computational power, but we're already starting to see greater attention towards things like real-time recommendations where the computation itself happens real-time, not simply the serving up of the recommendation...

So, instead of high frequency trading, perhaps it's only a matter of time before we see High Frequency Clienteling (HFC)? Instead of retailers interacting directly with customers, it's the customer app that contains a customer's parameterized rules talking with one or more retailer apps in real-time to make buying decisions on behalf of the customer. Mixed into this customer app is the Internet of Things dynamic where it knows the status of any recurring household item in the fridge and elsewhere in the home, and can autonomously make buying decisions on behalf of the family within some sensible set of parameters. Think Google Shopping meets Priceline and it's not hard to imagine what might be around the corner for retailers in many categories. On the retailer side, perhaps their app is pinging every single customer's app in near real-time to sell an item at an optimal price while stocks last. Regardless of whether it's a push or pull model, or some variant thereof, does High Frequency Clienteling seem that far fetched? And if HFC does come to pass, once speed becomes commoditized, what then?

Of course, the romanticism of shopping is absent in that scenario but for many, it was never there to begin with...
