---
author: Jason Gowans
comments: true
date: 2013-06-13 07:51:47+00:00
layout: post
slug: technology-has-killed-the-average-customer
title: Technology Has Killed The Average Customer
wordpress_id: 268
tags:
- analysis
- CLV
- customer lifetime value
- marketing
---

The first time I read about [Simpson’s Paradox](https://en.wikipedia.org/wiki/Simpson's_paradox), the lesson that stuck with me was the need to “de-average” as one of my former biz dev colleagues used to call it.

Today, digital advertising across most platforms has become so sophisticated that even the least mathematically inclined MBA intern can run a targeted ad campaign on Facebook across numerous dimensions such as age, gender, location, and interest.

While any marketer expects to be able to run a targeted ad campaign, it’s not always evident that they can articulate exactly why they are targeting a certain group of people, and it’s also not always the case that they can actually track the resultant performance of that cohort once the focus shifts from the publisher’s performance affidavit to the internal anals of the org. In other words, the ability of ad publishers such as Google and Facebook to target discrete groups of people has far outstripped the average organization’s ability to measure the response for those same discrete groups of people.

For example, suppose that a company with 1MM customers and $20MM annual revenue has reached the point that they want to know how much they should bid for certain Google keywords or Facebook clicks.

One of the first steps should be to understand how much a customer is worth...

[![Image](http://jasongowansdotnet.files.wordpress.com/2013/06/clv.png?w=522)](http://jasongowansdotnet.files.wordpress.com/2013/06/clv.png)

In the example above, the average customer spends $20 p/yr. Does that mean that we should be willing to spend up to $19 to acquire them? Well, probably not. For a start, we need to consider the gross margin of our business. In this example, we've assumed 20%. So, does that now mean we should only be willing to spend up to $4 to acquire new customers? Again, the answer is likely no. We now need to consider how long we can reasonably expect them to be a customer and given their current spend, how much that future cash is worth to us in today's dollars. So, we set about calculating a [Customer Lifetime Value](http://blog.kissmetrics.com/how-to-calculate-lifetime-value) and we determine that we're willing to spend something less than $33 to acquire a new customer. The problem of course is that when we look at our customers in the aggregate, we're missing the real story, which in this case is that 20% of our customers drive 80% of our revenue, and our biggest spenders are also our most loyal customers (a not uncommon aspect of many companies customer composition). Remember the [Pareto Principle](http://en.wikipedia.org/wiki/Pareto_principle)? :-)

So, as we think about CLV, it's important to "de-average" that number, look at it within discrete segments of our customer base and really try to understand what are the defining attributes of those segments. Only then can we begin to make informed decisions about whom to target on Facebook or any other marketing channel, and ultimately, how much we're willing to spend to acquire them. The technology is no longer the limiting factor, only our old ways...
