---
author: Jason Gowans
comments: true
date: 2013-12-06 22:01:46+00:00
layout: post
slug: an-industry-association-for-social-data-the-big-boulder-initiative
title: 'An Industry Association for Social Data: The Big Boulder Initiative'
wordpress_id: 959
---

Looking forward to rolling up my sleeves and working with these amazing people on the Big Boulder Initiative.
