---
author: Jason Gowans
comments: true
date: 2012-04-25 06:20:00+00:00
layout: post
slug: net-makeing-world-one-world
title: net makeing world one world
---

If you're vigilant, you'll have noticed the typo in the headline. Read on and you'll understand why...  
  
  
I had what I can only describe as a bit of a watershed moment last night in Härnu land. We had our very first user from Egypt trying to sign up for the first time. He / she was having trouble and sent a message to our Facebook page (which I'm monitoring incessantly right now). The thread started innocuously enough with me on one end trying to troubleshoot and our user gamefully trying to communicate with me in English. Not able to get on to the site, we quickly got to a declaration that the site was not good:  
  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/04/68cdb-chat1.png)](http://jasongowansdotnet.files.wordpress.com/2012/04/chat1.png)

  
  
We both stuck with it though, and once the user got onto Härnu, our Facebook message thread soon moved to this:  
  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/04/c67c1-chat2.png)](http://jasongowansdotnet.files.wordpress.com/2012/04/chat2.png)

  
  


In a matter of minutes, we'd gone from "no good site" to "very good idea. i can making friends. **net makeing world one world**."  
  
  
As we were messaging back and forth, I had goose pimples the entire time. After all, this was the scenario we had dreamed about - people in the Middle East talking with people in the West, learning and sharing with each other. I couldn't help but let my mind wander to how viruses spread and hoping that our mystery user was the Egyptian equivalent of Ashton Kutcher and from that would flow millions of Egyptians ready to connect and share with Americans and other people around the world.  
  
  
Well, here we are the next day, and we still have only one user from Egypt, but one user we have, and that user likes what we've got. Enough to have logged in today and engaged.  
  
  
If ever I had any doubts about what we're trying to achieve, they were erased in a haze of frantic back and forth with our Egyptian friend last night. We had a moment of panic when we wondered if it was too good to be true, but Google Analytics revealed that yes, in fact we had a session from Egypt.  
  
  
Whether Egyptian Härnu User #1 turns out to be a [Lonely Boy](http://vimeo.com/32543029) instead of Ashton Kutcher is immaterial. We made contact and we're convinced that from here it's all about execution.  
  
  
I expect that most start-ups have many watershed moments along the way. In our brief prologue, we've met our first and it was awesome!  
  
  
  
  

