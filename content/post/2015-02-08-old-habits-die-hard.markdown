---
author: Jason Gowans
comments: true
date: 2015-02-08 07:17:09+00:00
layout: post
slug: old-habits-die-hard
title: Old Habits Die Hard
wordpress_id: 1066
categories:
- Amazon
tags:
- amazon web services
- cloud computing
---

Last Fall, I attended the [AWS Re:Invent](https://reinvent.awsevents.com/) conference along with 13,000 other techies from around the world. During the event, Amazon went to great lengths to showcase the very large enterprises and government organizations that have adopted cloud computing.

Indeed, a good proportion of attendees were on the infrastructure teams of those large corporations, responsible, in part, for figuring out this cloud computing thing, and ultimately transitioning their company's infrastructure onto AWS.

Undoubtedly these corporations will benefit in the long-run from elastic computing resources, but what could throttle those benefits are old processes that are being ported over to the new environment. For example, as an application developer in a large corporation, the typical process for starting a new project is to submit a ticket, wait for an infrastructure engineer / architect to be assigned to your project, discuss your needs, do some capacity planning, provision a virtual machine or buy some servers, maybe give you root access, but more likely, have you tell them what you need installed, they'll take care of it for you, and eventually your environment is ready to go after a few back and forths. I've personally seen this end to end process take more than 100 days...

The cloud is supposed to end that inefficient process though, right? Well, not necessarily. While it's true that the corporation will probably reduce their infrastructure costs by moving to the cloud, it's not a guarantee that the newly empowered application developer will be able to move so much faster. I mean, there are only so many devs that know how to set up a cloud computing environment.

[![aws-console](https://jasongowansdotnet.files.wordpress.com/2015/02/aws-console.png?w=584)](https://jasongowansdotnet.files.wordpress.com/2015/02/aws-console.png)

For example, expecting a front-end developer whose skills lie with HTML, CSS, and JavaScript to now learn the myriad AWS tools to deploy their apps is simply not realistic. In fact, as Amazon continues to innovate, it's likely the role of AWS architect will become ever more specialized - the AWS security architect, the AWS networking architect, the AWS analytics architect, the AWS database architect, the AWS storage architect, etc.

So now, we're right back to where we started where an application developer needs an environment, but they don't know the whole AWS stack, and they're now working with their company's AWS architect whose job now is to provision AWS resources instead of the old on-prem resources they used to do. Granted, ordering the server has been cut out of the equation but the flow could be better. Much better.

Over the next few years, we're going to see more startups focus on specialized areas of the typical enterprise IT department to provide simplified, abstracted AWS-based services that will be composed of multiple AWS tools. Of course, AWS themselves are doing this to some extent but right now their major opportunities lie simply in getting these huge enterprises onto the cloud. As they further penetrate the mid-market, those abstracted services will become even more important where the "IT department" can sometimes be just a handful of folks.

In the meantime, corporate IT infrastructure teams would be wise to think not only about ditching their on-prem servers, but how the developers within their organization will actually provision and use those cloud services. At [Aerobatic](http://www.aerobatic.com/), the company I co-founded with [this guy](https://github.com/dvonlehman), we spend a lot of time thinking about how we can make the lives of front-end developers easy, but that's only half the story. The other half is thinking about how to make the lives of the infrastructure architect easy too, such that, in the end, everyone's happy, and those build-deploy-test cycles can keep pace with the business.
