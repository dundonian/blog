---
author: Jason Gowans
comments: true
date: 2012-08-20 22:27:00+00:00
layout: post
slug: random-acts-of-kindness-call4palestine
title: Random Acts of Kindness (#Call4Palestine)
---

I was going to write this post about a recent mis-step we made and tell you what we learned.  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/08/eidul-fitr.png?w=287)](http://jasongowansdotnet.files.wordpress.com/2012/08/eidul-fitr.png?w=287)

Instead though, I'd rather write about something much more important, and much more uplifting. That something is a trending topic on Twitter called [#Call4Palestine](https://twitter.com/#!/search/%23call4palestine). What's it all about? Well, at the end of Ramadan, there's a muslim holiday called [Eid al-Fitr](http://en.wikipedia.org/wiki/Eid_ul-Fitr).  
  
Being a random Scottish guy in Seattle, how do I know this? Well, someone from Indonesia told me about it on [Härnu](http://www.harnu.com/) (see image to the left).  
  
#Call4Palestine is simply an idea for people to randomly call numbers in Palestine and pass good wishes on to those on the other end during Eid al-Fitr:  
  
_"Happy Eid, next year in a free Palestine, from the river to the sea!" _  
  
You can read more about the details [here](http://beyondcompromise.com/2012/08/15/telephone-falasteen/). There's also a [Facebook page](https://www.facebook.com/Call4Palestine) chronicling stories of people making the calls and the resultant conversations.  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/08/45909-soundcloud.png)](http://www.harnu.com/q/966)

Well, not speaking Arabic, and fearful of offending anyone, I was a bit hesitant to actually call someone there. So, I did what any geek would do - I made a [Soundcloud recording](http://www.harnu.com/q/966) and then sent that message to Palestine via the map on Härnu for any and all Palestinians to discover it! Härnu will then auto-translate the text into Arabic if that's their preferred language. Of course, they're stuck with my Scottish brogue on the Soundcloud recording :-)  
  
I have no idea if anyone from Palestine will hear it, but I really hope so, and it'd be great if more people here in the West would do this too. I'm not talking about politics or religion here - I'm talking about humanity.  
  
A random act of kindness from a stranger goes a long way to restoring our faith in each other. So, go ahead, what are you waiting for?!  
  
** Update **  
  
Any doubt I had about what I was doing just evaporated with this awesome message from someone in Nablus, Palestine (which is a twin city of Dundee, Scotland by the way :-)  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/08/d834c-screenshot2012-08-20at6-00-56pm.png)](https://twitter.com/daigoor/status/237710747935846400)

  

