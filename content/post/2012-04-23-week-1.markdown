---
author: Jason Gowans
comments: true
date: 2012-04-23 04:48:00+00:00
layout: post
slug: week-1
title: Week 1
---

  


So, we're at the end of the first week of launching [Härnu](http://www.harnu.com/) into beta, and overall, it's been a decent one I think. On the plus side, we managed to get users from more than 20 countries, though when you hear about sites like Pinterest gaining 11M uniques inside of 2 months, it's hard not to wonder if you've got the right formula.

  


Everything we discussed, imagined and hoped for has now fallen into the realm of idle conjecture, replaced by an unfeeling and unforgiving empirical truth. Of course, at this point, we’re still operating from a place of [truthiness ](http://www.colbertnation.com/the-colbert-report-videos/24039/october-17-2005/the-word---truthiness) because until we have a critical mass of users, it’s hard to infer much with such a small sample size and a longitude that spans not quite 7 days yet.

  


Do we have the right content? Is the UX intuitive enough? Why are my Facebook network effect forecasts woefully off-base? Are the limited re-tweets, likes, and shares a reflection of the site or payback for bad karma I somehow brought upon us? Why hasn’t the New York Times contacted us yet? The answer in all of this I think is that at this point in time, we’re simply one of [thousands](http://techcrunch.com/2011/11/28/cambrian-explosion-startups/)of ambitious start-ups with a good idea, a product approaching [MVP](http://theleanstartup.com/), and enough goodwill from friends that we can start to grow a user base organically, albeit slowly in the early going. The fact that we’ve got to this point entirely boot-strapping it is something we feel really good about, though I’m sure having the right Angel / VC money would surely juice the user acquisition curve.

  


On user acquisition, I have to admit the week started out a bit shaky. For example, on the inaugural invitation email I sent to ~100 or so friends, the text to the site was correct ([www.harnu.com](http://www.harnu.com/)) but the underlying hyperlink was not, resulting in some errors and our CTO quickly coding a redirect from the wrong URL to our home page. 

  


Then, I managed to get our [@harnu account](http://www.twitter.com/harnu) on Twitter temporarily suspended for replying to tweets on keywords I had set up searches for. Against the [rules](https://support.twitter.com/articles/69214-rules-and-best-practices) I’ve come to learn! [#rookiemistake](https://twitter.com/#!/search/%23rookiemistake). Obviously I was mortified and it was a humbling introduction to the world of user acquisition via Twitter. My takeaway from that is that there’s no shortcut to building a legion of followers – just post good content smattered with an occasional plug for the core service itself and followers ought to gravitate to you. That’s what I plan to test this week anyway.

  


However, despite the missteps, we still found time to high five ourselves when people from places like Thailand, Peru, Anguilla, and Zimbabwe all joined this week – People that I don’t think were actually one of our friends!

  


So, as we enter week two, I’m encouraged by the fact that we’ve got interaction like this one below going on and we’re ever so slowly starting to rock the boulder back and forth towards what will hopefully someday soon be an avalanche of users. Then again, as a former boss often told me, “Hope is not a strategy.” Back to the salt mines we go! 

  


  


[![](http://jasongowansdotnet.files.wordpress.com/2012/04/b32de-qofweek.png)](http://www.harnu.com/q/461)

  


  


p.s. As I write this, we just had our first user from the Bahamas join our community!

  

