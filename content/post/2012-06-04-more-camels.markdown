---
author: Jason Gowans
comments: true
date: 2012-06-04 17:38:00+00:00
layout: post
slug: more-camels
title: More Camels
---

Right now, we're in the middle of a complete redesign of Härnu based on early feedback (both implicit and explicit) from our users. So, as the guy on point for bringing users, there's not a whole lot for me to do but plan assiduously and sit tight for the next iteration.   


  


Having said that though, I've been dabbling in posting Twitter and Facebook updates on behalf of Härnu and what I've learned won't surprise anyone - **photos are gold**!

  


####      A word on virality

Before I get into details, Facebook defines virality as "_The percentage of people who have created a story from your page post out of the total number of unique number of people who have seen it._" In other words, it's the number of people "_Talking about this_" divided by the "_Reach_" as defined by Facebook.

  


So, with that out the way, here's a quick rundown of what I've learned so far:

####      

####    

####      Photos Trump Text

One day recently, I posted a Facebook update about a mother using Härnu to find her adopted son's biological brother in Ethiopia. This was a use case we could never have imagined but it was one that we thought was incredible and were really pleased that our efforts could play a small part in helping. This post was all text.

  


[![](http://jasongowansdotnet.files.wordpress.com/2012/06/1b8fd-ethiopia.png)](http://www.harnu.com/q/677)

  


  


The next day, I posted a quick Facebook update about Indonesian users now on Härnu. I included a stock photo of Bali to highlight my point.

  


[![](http://jasongowansdotnet.files.wordpress.com/2012/06/fabb1-indonesia.png)](http://2.bp.blogspot.com/-msGJBpLBhpE/T8zqkFVk96I/AAAAAAAAIuQ/L57b_hFi-48/s1600/indonesia.png)

  


  


  


We saw a virality score of 2.41% for the post about the mother and a 4.55% score for the post about Indonesian users. In other words, the post about Indonesian users was 1.9X more viral than the one about the mother looking for her adopted son's brother in Ethiopia.

  


**Hypothesis #1: **_Posts with embedded links and / or photos are more viral than those without_

  


Easy and obvious enough right? What came next though was a much better illustration of that point. One of our users from the Philippines whom I've been chatting with, sent me a picture of him and a camel (he's living in Saudi Arabia just now). He also told me that people eat camel meat there. In retrospect, not very surprising. Kind of like horse being on the menu in much of France right? I thought this was pretty interesting, so I posted this to our Härnu Facebook page (note I started with an attention grabbing question):

  


[![](http://jasongowansdotnet.files.wordpress.com/2012/06/cce5f-camel.png)](http://3.bp.blogspot.com/-WhuHXf-PeAA/T8zsyFr3CII/AAAAAAAAIuY/YYm9oRPXU-k/s1600/camel.png)

  


This post had a virality score of 12.1%. Our most successful one yet and 2.65X more viral than the post about Indonesia and 5X more viral than the one about the mother.

  


**Hypothesis #2**: _Photos that are more personal and authentic are more interesting than stock photos_

_  
_

**Hypothesis #3:**_ Posts framed as questions are more viral than simple text updates_

_  
_

Note I'm using the term "hypothesis" here and not yet "conclusion" - we're still learning and a sample of one does not a trend make. That said, I followed up this last post about a camel with another one with a photo, posed as a question, and tried to piggyback on the Queen's Jubilee in England.

  


[![](http://jasongowansdotnet.files.wordpress.com/2012/06/0c7f8-royal.png)](http://1.bp.blogspot.com/-p8EyvDJhzxk/T8zvdCdThsI/AAAAAAAAIuk/I-tD56rRtUM/s1600/royal.png)

  


How viral was this one? The answer is not at all. Turns out no-one cares about the Queen, or at least, none of our users does.

  


**Conclusion:** While we have yet to achieve Konyesque virality, in the meantime, our winning formula appears to be MORE CAMELS ;-)

  


For the experts out there, what kind of content has worked best for you on Facebook & Twitter updates? What kind of virality scores do you typically see? Do you think it even matters?
