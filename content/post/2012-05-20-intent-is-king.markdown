---
author: Jason Gowans
comments: true
date: 2012-05-20 20:40:00+00:00
layout: post
slug: intent-is-king
title: Intent is King
---

When Härnu's [Facebook page](http://www.facebook.com/harnuapp) reached 100 Likes, Facebook offered us a $50 credit to try out Facebook ads. Now that we've dipped our toes in the water, I thought I'd share some thoughts on the experience.  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/05/first.png?w=260)](http://jasongowansdotnet.files.wordpress.com/2012/05/first.png?w=260)

Wanting to get a really firm idea of not just clicks of the ad on Facebook but actual conversion as measured by new Härnu user sign-ups, I started off with a really niche ad - For the image, I selected our logo, wrote some text in English and then targeted it to users in Norway and Sweden. I layered in some demographic constraints such as wanting to target people between 18 - 35 but that was about it. Oh, instead of having users click thru to our Facebook page, I set it up to have the ad click thru to Härnu's [home page](http://www.harnu.com/).  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/05/swedish.png?w=256)](http://jasongowansdotnet.files.wordpress.com/2012/05/swedish.png?w=256)

Well, after running it for about a week and paying $0.67 per click we didn't observe any incremental user sign-ups attributable to the ad. Next, I thought maybe I should try writing the ad in Swedish to make it feel more localized. So, I did and this time restricted the ad to just Sweden, dropping Norway (sorry Norway!). Well, in the past month, I've seen 50K impressions, 18 clicks, a clickthru rate of 0.035%, and zero incremental users attributable to the tactic.  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/05/english.png?w=256)](http://jasongowansdotnet.files.wordpress.com/2012/05/english.png?w=256)

Thinking of ways to spruce up the ad, I replaced the logo image with a screenshot, I re-wrote the ad in English trying to be more direct and impactful, and this time I targeted it to NZ, Australia, and Canada. The thinking there was that maybe it was a cultural thing and English speaking countries might be more apt to try the service out. Well, this time I've observed a click-thru rate of 0.030% and zero incremental users attributable to the tactic.  
  
**Conclusion - Facebook ads are not working for us.**  
  
I'm sure there are ways we can make this better, and for a new entity, it doesn't help having so little name recognition. However, while I'm certainly not a creative expert, we could see a 10X improvement and it'd still only be a 0.3% click-thru. I've read that [average click-thru on Facebook](http://www.quora.com/What-is-the-average-CTR-on-Facebook-Ads) is somewhere around 0.04% - 0.05%. Contrast that with [Google AdWords](http://www.quora.com/Google-AdWords/What-is-the-average-CTR-of-Google-AdWords) average of ~2% and I'm left scratching my head asking what am I missing?  
  
Just this week, [GM decided to cut bait](http://www.reuters.com/article/2012/05/18/gm-facebook-idUSL1E8GHFAE20120518) with paid advertising on Facebook and I have to say I can't blame them. For a while now, we've seen Sheryl Sandberg tout the virtue of brands being a part of a user's life on Facebook, while shying away from metrics like CTR, never mind conversion metrics such as sign ups or sales lift:  
  
_"Facebook executives argue that the click-through numbers are not that meaningful; they say that people remember ads better and are more likely to make purchases when their friends endorse products." - _[Business Week, May 2011](http://www.businessweek.com/magazine/content/11_21/b4229050473695.htm)  
  
If Facebook, with all of the mountains of data at its disposal can't hang its hat on hard metrics, and instead has to rely on some qualitative intercept study talking about recall, then they have a huge problem.  
  
Frankly, I've been one of those sales guys representing ad inventory (not at Facebook) that every metric suggests is not working and it's your job to torture the data and come up with a story to get brands to buy. When it's a new venture, it's easy to make claims that this is a whole new class of ad inventory, and for a while, you feel like you've convinced the brands, but eventually every publisher has its day of reckoning and if the ads aren't working, the media agencies and brands eventually figure that out.  
  
You might still be the smartest guy in the room, but at some point it just doesn't matter anymore. Large amounts of money are being spent and brands want results. You can talk brand equity, you can talk reach, you can even put some new spin on it around building fans but it eventually has to come back to sales, and that's where Facebook is stumbling pretty hard right now in my opinion.  
  
When I am in product research mode, I go to Google or Bing - not Facebook. I triangulate my options using search engines and may subsequently check with one or more friends if it's a big ticket item. However, Facebook is now surfacing those friend recommendations off-Facebook [via Bing](http://www.pcworld.com/article/255669/bings_social_search_a_handson_tour.html)! In other words, what Google likes to call the [Zero Moment of Truth](http://www.zeromomentoftruth.com/) simply never happens on Facebook.  
  
If Facebook's future contemplates the bulk of its revenue continuing to come from ads, surely it must wrest control of those ad dollars away from Google, and it's going to have to find a way to pivot its site to capture those moments of intent.  
  
As brands shift their perception of Facebook from newfangled thing that they're are willing to experiment with to a mature part of the ad spending eco-system, no amount of squishy story telling by its ad execs, even if (s)he's a modern-day version of [Hans Christian Andersen](http://en.wikipedia.org/wiki/Hans_Christian_Andersen),  is going to convince major brands that Facebook deserves to be part of its marketing mix unless it can demonstrate that those dollars are resulting in incremental behavior.  
  
Right now, this little advertiser is not convinced. Losing GM is one thing. Losing the long-tail of advertisers would be quite another...  
  
  
  
  

