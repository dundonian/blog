---
author: Jason Gowans
comments: true
date: 2014-08-08 03:35:08+00:00
layout: post
slug: time-to-level-up
title: Time To Level Up
wordpress_id: 1052
categories:
- Soundcloud
- Work
---

Earlier this year, I had the good fortune to meet [Marc Strigel,](https://twitter.com/paynovation) COO of [Soundcloud](https://soundcloud.com/). Marc's one of those rare folks that immediately strikes you as perpetually curious, always deconstructing and reframing what he thinks he knows, stress testing those constructs in conversations with others, giving you a lot to think about...During one of our conversations, he described the culture at Soundcloud, and one of those aspects stuck with me throughout the summer - the notion of **leveling up**.

From their [site](http://soundcloud.com/jobs), here's how Soundcloud describes it:

<blockquote>"We use a term to describe how we’re constantly seeking to improve: “Level Up”. It’s about being self-motivated and challenging each other at every moment."</blockquote>

In America at least, we lionize people who push themselves to the absolute limit. Indeed, I used to be one of those people myself. In college, I ran division 1 track, and the mind games I played to convince myself that I was stronger than my opponents, willing to push myself past the point of exhaustion to prevail, were, in hindsight, the product of unadulterated obsession. Over time, this mentality comes to define who you perceive yourself to be and how you outwardly project yourself. The willingness to endure pain becomes a fact of life and as your ability progresses, so too does your pain threshold, and the addictive nature of it just takes over and the cycle repeats ad infinitum.

...But, that was 20 years ago. As I ran along the Ship Canal in Seattle last night, I ran with nothing but love and happiness. Did I push myself to the limit? Not even close. Indeed, I think now, I'm not looking to push any single part of my life to the obsessive limit. Instead, I'm trying to push all parts of my life to the point of happiness, or equilibrium, and that goes for my professional life too.

This week, I found happiness in figuring out how to write some site scraping code of all things. I found happiness in getting back the results of a formative product my team has been working on and seeing the enthusiasm it generated within the organization. I found happiness in a really interesting [project](http://www.aerobatic.io/#!/) I've been contributing to with a friend. I found happiness in writing.

At the time I spoke with Marc about Leveling Up, I mostly thought about the pressure for individuals of an organization to constantly push themselves to the limit and the potential for that to negatively manifest itself. Soundcloud is right though - the key really is _self-motivation_ as opposed to external, organizational pressure or an internal fear of failure. The role of a leader in such an environment is not to create a culture of negative up-or-out energy, but to create the positive conditions that allow and encourage everyone to push themselves to the point of happiness. And real happiness comes not with rote repetition, stagnation, or fear, but with enlightened growth in all areas of your life that are important to you.

This weekend, I'll be out there again teaching myself to fish for trout in some mountain lake, fumbling around with lures, casting bubbles, and leader lines - stuff I honestly had no clue about 3 weeks ago, but my kids will surely appreciate it, and that makes it important to me. Time to Level Up!

[![Jason-at-LSU](http://jasongowansdotnet.files.wordpress.com/2014/08/jason-at-lsu.jpg)](http://jasongowansdotnet.files.wordpress.com/2014/08/jason-at-lsu.jpg)
