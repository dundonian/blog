---
author: Jason Gowans
comments: true
date: 2013-02-14 18:38:00+00:00
layout: post
slug: barnes-and-noble-an-omni-channel-fumble
title: Barnes and Noble Omni-Channel Fumble
---

This morning I was looking for a reference book on Hadoop. My go-to source always starts with Amazon. I'm an Amazon Prime customer, their search is already filtered / optimized to help me find relevant books, their prices are always competitive, and usually the reviews are very useful.  
  


[![Amazon Book Price](http://jasongowansdotnet.files.wordpress.com/2013/02/5ba21-screenshot2013-02-14at10-09-02am.png)](http://4.bp.blogspot.com/-lvpBH9rSvKU/UR0o9UM3zcI/AAAAAAAAAN0/h3cv6kjg0L0/s1600/Screen+Shot+2013-02-14+at+10.09.02+AM.png)

  
However, with some spare time today, I wanted to start reading it today. My choices therefore were to download it to my Kindle or to buy at a brick and mortar retailer. I usually prefer to have a physical copy of technical reference books, so over I popped to Barnes & Noble and found the same book at a comparable price:  
  


[![Barnes and Noble Book Price](http://jasongowansdotnet.files.wordpress.com/2013/02/e59e3-screenshot2013-02-14at10-06-19am.png)](http://1.bp.blogspot.com/-mPxOo3j9G80/UR0pb3SNNrI/AAAAAAAAAN8/GGIVve_uP9g/s1600/Screen+Shot+2013-02-14+at+10.06.19+AM.png)

  


Given Amazon's usual lock-in, this was a golden opportunity for B&N to earn a piece of business that would 9 times out of 10 go to Amazon. The big font "Pick Me Up" told me the item was in stock at my local store. So, I was ready to buy it and head downtown right now. The problem arose, however, when I clicked through to buy it:

  


<table cellpadding="0" align="center" style="margin-left:auto;margin-right:auto;text-align:center;" cellspacing="0" class="tr-caption-container" ><tbody ><tr >
<td style="text-align:center;" >[![Barnes and Noble Store Pick Up](http://jasongowansdotnet.files.wordpress.com/2013/02/dc585-screenshot2013-02-14at10-17-15am.png)](http://1.bp.blogspot.com/-eFW6jPDG278/UR0qUuzKfbI/AAAAAAAAAOE/85fIqeVXl3c/s1600/Screen+Shot+2013-02-14+at+10.17.15+AM.png)
</td></tr><tr >
<td style="text-align:center;" class="tr-caption" >  

</td></tr></tbody></table>

The price had reverted to $49.99. Now as much as I want to buy this book today, I'm not prepared to pay a 44% surcharge for that convenience, particularly when I have free two day shipping with Amazon via my Prime membership. While I can understand that B&N carries the additional real estate overhead and needs to somehow pay for those storefronts, frankly that's not the customer's problem. I'd be just as happy if they were flogging the books from the back of a flatbed off the side of the road if it meant I could avoid this levy.

  


So, here we have a situation where the brick and mortar retailer has done so much right to grab that sale in a way that Amazon cannot - immediate gratification. Barnes & Noble should win this head to head every single time. Instead, it's treating me differently based on where its inventory is ultimately being picked and is losing a sale in the process.

  


I saw a similar situation a few weeks ago when my downtown Banana Republic had a 40% off sale on everything, but didn't have my size on a particular item. When I got home, I was able to order the item online in the correct size but at a 30% discount. In that case, I went ahead with the sale even though I was a bit annoyed that they were extending different discounts across channels.

  


As retailers commit to omni-channel, the customer has to remain foremost in the decision-making process, particularly in a world where Amazon has done such a great job of locking-in most categories. In the case of Barnes & Noble, their business model is hobbled by the legacy costs of what's essentially a commoditized category and as much as I'd like to see them remain in business, if only to ensure that Amazon has competitors, it's hard to envision a scenario where they can compete effectively, particularly when they score own-goals with an otherwise loyal Amazon customer.

  


Update:

Curious to see if the in-store folks at B&N would do anything if I pointed out their online price, I went to the store and after pointing out the price discrepancy to the cashier, he said he wishes they'd _raise_ the prices online to eliminate confusion in-store!! Upon telling him such a policy was doomed to fail, he pointed to the long line behind me (there was only one cashier on duty) and said that plenty people don't seem to mind such pricing discrepancies. Not exactly a very enlightened attitude but about what I expected.

  

