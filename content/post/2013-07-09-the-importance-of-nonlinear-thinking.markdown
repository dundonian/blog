---
author: Jason Gowans
comments: true
date: 2013-07-09 18:03:53+00:00
layout: post
slug: the-importance-of-nonlinear-thinking
title: The Importance of Nonlinear Thinking
wordpress_id: 595
categories:
- retail
tags:
- '

  education'
- '

  nonlinearity'
- retail
---

Last week I met with a financial advisor to discuss college funding for our kids (ages 5 yrs, 3 yrs, and 8 weeks). During our meeting, he informed me that it'd cost more than $700,000 to see all 3 kids through 4 years of college at an in-state university.  This was based on an assumption of the current going rate of $20K per year with projected 7% increases annually...

![Projected Costs of College](http://jasongowansdotnet.files.wordpress.com/2013/07/screen-shot-2013-07-08-at-11-06-14-pm.png?w=228)

Naturally, I left that meeting feeling somewhat inadequate, trying to rationalize the circumstances by which it might be ok to not fund my kids education - show some tough love by expecting them to pay it themselves, the likelihood of a sports scholarship, slink back to Europe and scrounge a free education there, etc. I mean, $700K?? $230K per kid for a 4 year degree...Seriously? So, I forgot about it for a few days until this morning I read about the [first millionaire online teacher](http://pandodaily.com/2013/07/08/lessons-from-the-first-millionaire-online-teacher/) and for the first time, really understood deeply that the world of education is undergoing such thorough disruption that to think linearly about projected costs of college education in 2026 is myopic in the extreme. Yes, of course we have Coursera, Khan Academy, and lots of other online options, but it wasn't until today that I truly believed that education will be fundamentally different 15-20 years from now.

As I think about my own day to day work, taking the easy way out is drawing straight lines up and to the right when we're talking about the future, even when those lines fit so nicely with everything that we've known until this point. It's much harder to not draw any line at all, and to instead challenge yourself to think hard about what the future will be like, particularly when technology being invented _right now_ is re-rendering much of our world that was once familiar in completely new and nonlinear ways.

So, what are the necessary conditions to suggest that a nonlinear outcome is on the horizon? How can we better anticipate nonlinear outcomes? Well, in the case of education, what we're seeing are three major trends;



	
  1. Rising costs & diminishing returns

	
  2. Increasing availability of credible substitutes

	
  3. A shift in cultural norms


The third trend is the most fascinating to me in that there's a growing number of people who believe that a college education is not worth it, and it'll be interesting to see how employers evaluate a growing number of candidates with non-traditional education backgrounds. Once this trend becomes mainstream, it'll completely devalue the four year education institution and perhaps then will we see wholesale innovation from within the annals of academia.

You can see this same pattern playing out in the automotive world where Elon Musk is playing a nonlinear game, and for us in retail, there are enough signals to suggest that our future will be anything but linear.
