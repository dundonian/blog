---
author: Jason Gowans
comments: true
date: 2012-04-29 21:50:00+00:00
layout: post
slug: hot-stacey-goes-global
title: Hot Stacey Goes Global
---

No, it's not the name of some adult movie. Rather, it's the name my daughter gave to the female version of [Flat Stanley](http://en.wikipedia.org/wiki/Flat_Stanley). At pre-school, they're using the character Flat Stanley to take photos of their environment for class show and tell. The female version is called Flat Stacey, but as my daughter explains, she left Flat Stacey outside and she got hot, so now she's simply called Hot Stacey.  
  
So, with that as a backdrop, I've been experiencing a lot of mixed emotions this week as we wade ever deeper into our beta test. From bugs affecting users signing up, to inevitable performance tuning issues, to banning users for being jerks (a story for another time), it's been a whirlwind the past few days.  
  
However, just today I got a reply from a connection in the UK where I'd sent the following question:  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/04/c6552-q.png)](http://jasongowansdotnet.files.wordpress.com/2012/04/q.png)

  
What did I get back? Only a photo of Hot Stacey at the Olympic Park in London!  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/04/80210-harnudoll.jpg)](http://jasongowansdotnet.files.wordpress.com/2012/04/harnudoll.jpg)

  


  


(Full disclosure - my Härnu connection that sent this is in fact a friend of mine. However, the kindness of friends is just as welcome as the kindness of strangers!)  
  
As I write this, my daughter is taking an afternoon nap. When she wakes up though, I can't wait to show her this photo and field the many questions she's sure to ask about London and the Olympics.  
  
As we get started with Härnu, I'm buoyed by the thought of all the cool use cases people will use our platform for, and it's gratifying to know we can count on our friends both near and far to to help jump-start the task of building a community that hopefully will bring about some empathy for each other and a bit of levity along the way!  
  
Jerks need not apply.  
  
  

