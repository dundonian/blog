---
author: Jason Gowans
comments: true
date: 2015-07-08
layout: post
title: How To Publish Jekyll Drafts
---
Having recently moved my blog to Jekyll and hosted it on [Aerobatic](http://bitbucket.aerobatic.com/), I needed the ability to publish draft posts to share with others before being visible on my main <code>index.html</code> page. 

I played around with the <code>_drafts</code> option but that wasn't exactly what I was looking for, so here's what I eventually did.

In my draft post markdown file, I created a draft tag:

{{< highlight ruby "linenos=inline">}}
---
title: some title
subtitle: some subtitle
layout: post
date: 2015-07-25
description: some description.
tags:
- draft
---
{{< /highlight >}}

Next, in my index page that lists all of my posts, I added an unless statement to qualify which posts are actually listed.

{{< highlight ruby "linenos=inline">}}
{% for post in paginator.posts %}
  {% unless post.tags contains 'draft' %}
    <div class='post-preview'>
    	<a href='{{ post.url | prepend: site.baseurl }}'>
    		<h3 class='post-title'>{{ post.title }}</h3>
    	</a>
    	<p>{{ post.description }}</p>
    </div>
    <hr>
  {% endunless %}
{% endfor %}

{{< /highlight >}}

And that's it! Hope this helps.