---
author: Jason Gowans
comments: true
date: 2014-03-19 05:10:23+00:00
layout: post
slug: per-unit-economics-dogs-per-night
title: 'Per Unit Economics: Dogs Per Night'
wordpress_id: 1024
---

Last week, I read an [article](http://www.geekwire.com/2014/rover-com-fetches-12m-plans-dog-sitting-marketplace-expansion/) about [Rover.com](http://www.rover.com/), a dog-sitting marketplace receiving a third round of funding of $12MM, bringing their total funding to date to $25MM.

I didn't give this much thought until later that night when I started to think a bit more about the per unit economics of such a business. The article talked about their growth from 10,000 sitters to 25,000 sitters in 2013, it discussed their eightfold increase in revenue between 2012 and 2013, and how sitters charge between $20 - $40 per night per dog, with Rover taking a 15% cut.

When you look at a marketplace like that, Rover has quite a lot of levers it can pull:



	
  * Growth

	
    * Grow the number of sitters

	
    * Grow the number of dog owners

	
    * Increase the number of active users

	
    * Expand into other pet categories




	
  * Revenue Optimization

	
    * Charge more than $20 - $40

	
    * Take more than a 15% cut





At this stage of the company, and with $25MM in funding, presumably most of their focus is on growing the service in their core category. So, what might that look like?

[![2012 Rover Revenue](http://jasongowansdotnet.files.wordpress.com/2014/03/chart_2.png)](http://jasongowansdotnet.files.wordpress.com/2014/03/chart_2.png)

Well, if 1% of Rover's 10,000 sitters in 2012 were active on any given night, and they each sat one dog, Rover would have earned between $110,000 - $220,000 on an annualized basis. If we swag Rover's run rate of $7MM annually*, at 40% - 60% active sitters in 2012, they'd have 4,000 - 6,000 dogs per night and Rover  would hit break-even, depending on whether sitters are charging closer to $20 or $40 per night.

So, as Rover grows throughout 2014 and beyond, one metric I expect they're paying attention to is dogs per night. At what point does the business look primed for an exit? My guess is somewhere around 15,000 dogs per night mark. With [an estimated 153,000 dogs](http://www.seattlemag.com/article/seattles-dog-obsession) in Seattle alone and 47% of [households nationwide](http://www.humanesociety.org/issues/pet_overpopulation/facts/pet_ownership_statistics.html) owning at least one dog, 15,000 dogs per night nationwide seems plausible, doesn't it?

------------

*So, what's Rover's run rate? Short answer, I don't know. Speculative answer? Well, if we take Rover's 43 employees and assume that, fully burdened, they cost an average of $100K per annum (though perhaps it's more - it's Seattle after all), then that puts Rover's payroll  ~$4.3MM. Then we have to add in other operating costs and of course the cost of acquisition of sitters and pet owners. All told, I'm guessing their run rate is between $6MM - $7MM, which would mean this latest round of funding gave them another couple of years to work towards an exit...
