---
author: Jason Gowans
comments: true
date: 2013-02-26 08:45:39+00:00
layout: post
slug: next-in-data-science-simplification
title: 'What''s Coming Next in Data Science: Simplification'
wordpress_id: 17
tags:
- apache
- big data
- data science
- hadoop
- mahout
- retailers
---

The rise of [Hadoop](http://hadoop.apache.org/) has been interesting to watch as it has moved from an obscure open source project being worked on by the tech vanguard to mass adoption by Fortune 500 companies.

This adoption curve is attributable to four primary forces:



	
  1. _Celebrity endorsements_ - The digital celebrities of our generation such as Google, LinkedIn, and Facebook have built their business on the back of Big Data, and large companies seek to emulate that success when it comes to data and analytics.

	
  2. _Simplification_ - New market entrants such as [Cloudera](http://www.cloudera.com/content/cloudera/en/home.html) and [AsterData](http://www.asterdata.com/) made Hadoop more accessible and less complex to deploy, manage, and query a Hadoop environment. 

	
  3. _Sales enablement_ - Traditional enterprise software companies then bought some of the new entrants and armed their large field sales teams to sell these new products into IT shops around the world.

	
  4. _The Amazon effect_ - All retailers now understand that the traditional competitors in their retail category pale in significance to the threat represented by Amazon. The same holds true for other industries where new companies will emerge with data at their core and incumbents that can't adapt will be gone.


The next area poised to take this same path seems to be data science. Whereas Hadoop unlocked new data assets, data science promises to unlock competitive advantage through disciplines such as data mining and machine learning.

What's interesting though is how the simplification process for enabling data science is different from enabling core Big Data tools like Hadoop. While bringing new tools like Hadoop into a large enterprise IT shop is familiar ground for the staffers there, enabling machine learning is decidedly foreign to most. That's why Analytics As a Service providers with point solutions are gaining traction all over the place. From product recommendations to email marketing, for many, the solution is to give us your data, we'll do our algo magic in our black box, and then send you the answers. All IT needs to do in that scenario is pipe data back and forth. No need to worry about the hard math.

However, is this really the best solution for large enterprises? With the maturation of [Apache Mahout](http://mahout.apache.org/), an open source library of machine learning and data mining algorithms, perhaps we'll see a new breed of companies emerge that go some way towards abstracting the complexity of data science for enterprises without making it completely opaque.

When Jeff Bezos started Amazon, he could well have declared that he was in the business of selling books and outsourced all non-core functions. Instead, he hired a bunch of Ph.Ds, data science became Amazon's core business, and the rest is history. Enterprises that aren't willing to make that same kind of commitment when it comes to their data assets and the data science involved in extracting value will probably find themselves on the wrong side of that history.
