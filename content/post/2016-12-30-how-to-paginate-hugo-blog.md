---
author: Jason Gowans
comments: true
date: 2016-12-30
layout: post
title: How To Add Pagination To Your Hugo Blog
---
I recently moved my blog from [Jekyll](https://jekyllrb.com/) to [Hugo](https://gohugo.io), using the [Hugo Zen](http://themes.gohugo.io/hugo-zen/) theme. While I love the simplicity of the theme (and its use of [Skeleton](http://getskeleton.com/) instead of Bootstrap), it doesn't come with pagination already baked in. So, I decided to add some of my own. For reference, the Hugo docs do indeed cover [pagination](https://gohugo.io/extras/pagination/), but it wasn't totally intuitive to me. So, here's how I tackled it:

In the `layouts/partials folder`, I created a new file called `pagination.html` 

{{< highlight ruby "linenos=inline">}}
{{ $pag := $.Paginator }}
{{ if gt $pag.TotalPages 1 }}
    <nav class="pagination" role="navigation">
        <span class="page-number">Page {{ $pag.PageNumber }} of {{ $pag.TotalPages }}</span>
        {{ if $pag.HasPrev }}
            <a class="newer-posts" href="{{ $pag.Prev.URL }}">&larr; Newer Posts</a>
        {{ end }}
        {{ if $pag.HasNext }}
            <a class="older-posts" href="{{ $pag.Next.URL }}">Older Posts &rarr;</a>
        {{ end }}
    </nav>
{{ end }}
{{< /highlight >}}

Next, in my `index.html`, I called the Paginate object:

{{< highlight ruby "linenos=inline">}}
{{ partial "header.html" . }}

<main role="main">
    <ol class="post-list">
        {{ $pag := .Paginate (where .Data.Pages ".Params.hidefromhome" "!=" "true") }}
        {{ range $pag.Pages }}
            <article itemscope itemtype="http://schema.org/Blog">
            <h2 class="entry-title" itemprop="headline"><a href="{{ .RelPermalink }}">{{ .Title }}</a></h2>
            <span class="entry-meta"><time itemprop="datePublished" datetime="{{ .Date.Format "2006-01-02" }}">{{ .Date.Format "January 02, 2006" }}</time></span>
        </article>
        {{ end }}
    </ol>

    <div class="post-navigation">
        {{ partial "pagination.html" . }}
    </div>
</main>

{{ partial "footer.html" . }}

{{< /highlight >}}

And that's it! Hope this helps.