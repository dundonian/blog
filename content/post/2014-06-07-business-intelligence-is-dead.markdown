---
author: Jason Gowans
comments: true
date: 2014-06-07 02:43:43+00:00
layout: post
slug: business-intelligence-is-dead
title: Business Intelligence Is Dead
wordpress_id: 1042
tags:
- business intelligence
- closed loop
- reporting
---

[![](http://jasongowansdotnet.files.wordpress.com/2014/06/finger.jpeg)](https://jasongowansdotnet.files.wordpress.com/2014/06/finger.jpeg)The human brain [constitutes about 2% of our body mass, but consumes as much as 20% of our energy](http://www.economist.com/news/science-and-technology/21602988-human-beings-are-brainy-weaklings-muscled-out). As an intermediary in decision-making, not only is the human brain inefficient, it’s also often wrong as discussed at length in Daniel Kahneman’s book [Thinking Fast & Slow](http://en.wikipedia.org/wiki/Thinking,_Fast_and_Slow).

At the same time, computation is faster and cheaper than ever with no end to improvement on the immediate horizon. Combined with the rise of the Open Source movement, this has brought us to the point where machine learning and other artificial intelligence techniques are no longer the domain of obscure research papers but are now table stakes for most apps being built today.

In light of these advances and our inherent human fallibility, any business software that does not currently “close the loop” between analysis and action is essentially dead – a piece of zombie code used by people living in the past that rely on the output of these systems to prop up their vainglorious assertions with a convenient percentage here and a tortured average there.

The promise of Business Intelligence was always supposed to be about better decision-making, but more often than not, it provided nothing more than a prop for people to continue to decide emotionally and justify intellectually - a conversation piece, and an expensive one at that.

Consequently, the value of today’s Business Intelligence tools are essentially being discounted down to zero because of the humans that consume the output, belabor its meaning, and at some point long after the fact, make a decision that has a not insignificant probability of being wrong.

For enterprise SAAS companies that are being founded today, the basic act of reporting is a service that they will give away for free, because they understand that gathering & integrating data, labeling data, creating metadata, and performing transformations are only an intermediary step towards action within a closed loop system which is where the real value lies. Within such services, reporting of aggregate descriptive data is a convenient by-product and one that can be given away for free with relatively little incremental cost. Just as big-box retailers have their door-busting deals on soda, SAAS companies can afford to lead with free reporting knowing they’ll make it up and then some with much more valuable services (and ones that lend themselves better to pay-for-performance subscriptions).

If you are in the reporting business be it sales data, social data, web data, or any other variety of data, and you haven’t transitioned towards something further up the value chain, I would be very worried for what’s about to come. BI is already dead and it’s just a matter of time before the markets acknowledge that en masse and value it accordingly.

p.s. I spent a fantastic 4.5 years of my career  with MicroStrategy in the mid-2000s. During my time there, I would often hear Michael Saylor, the CEO of MicroStrategy make sweeping declarative statements about the future delivered with such conviction that they were taken as fact by most in attendance. With much affection, this post is partly for him. What’s up Mike?!
