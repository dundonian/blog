---
author: Jason Gowans
comments: true
date: 2012-09-14 23:42:00+00:00
layout: post
slug: ask-the-hard-questions-before-integrating
title: Ask The Hard Questions Before Integrating
---

I love [Etsy](http://www.etsy.com/). That might sound strange coming from a guy who cringes every time he steps into a store, but I do. I love the site's aesthetic, I love the community they've empowered, and I love the brand they've created for themselves. What I don't love, however, was [Harnu's](http://www.harnu.com/) integration with Etsy.  
  
This statement has nothing to do with Etsy's technology or any other short-coming, and instead has everything to do with our not fully thinking through the implications of that integration.  
  
When we launched Harnu in August, we had built a web app that showed Etsy products from around the world on the map. Anyone could then find products from around the world and start a discussion with people in the country of the product's origin. This followed the same format that we have done with news that enables you to seek a local's opinion, or with music that enables you to get a local's recommendations.  
  
Upon completing the integration, our app was listed in the Etsy gallery - on the front page, actually.  
  
What happened next should not have been a surprise, but we were pushing hard to just get Harnu launched, and although we were not crazy about the Etsy integration, we thought it was kind of cool, and would give our users another way to learn about the world. However, instead of our users discovering Etsy products, the opposite happened - the sellers on Etsy discovered our users. Upon launching the app, we were seeing quite a number of new user signups that were obviously Etsy shop names. Initially, we thought that'd be good so they could participate in the conversations around their products. Our opinion quickly soured though when the Etsy shop owners started creating posts that basically said, "Visit my shop!" with a link to their shop.  
  
For any new service that's building a community and relies on user-generated content, it was a nightmare. We tried at first to just coach them that our users would find their products and to instead participate in the conversations about their country. The coaching didn't work though, and we continued to see these random posts advertising Etsy shops. Eventually, we shut the whole integration down, and de-listed our app from the Etsy gallery.  
  
Contrast that experience with this week, where Harnu is now chock full of students and educators posting content, finding collaboration partners, sharing music, and discussing the world, and it's fair to say we've learned a lot in recent weeks.  
  
I don't fault Etsy sellers at all. They have their own motivations which is to sell their products, and that I think is the key point when you're seeding a community.  
  
Take a moment to ask yourself the right questions:  
  


  * Who are you targeting and why?
  * What are the use cases?
  * What are the actors' motivations?
  
  
Not exhaustively examining these questions will almost certainly result in unintended consequences that could set your startup back. The list above is just an example. You of course need to ask a whole host of other things not least of which is acquisition strategy for that channel and monetization opportunities  
  
As for us? We quickly recovered, and we're having a blast seeing people share their favorite sounds and genuinely expressing a sincere curiosity to learn more about each other's culture.  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/09/19c4c-music.png)](http://www.harnu.com/m/gm/1178)

  
  
  
  
  

