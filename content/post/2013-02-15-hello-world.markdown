---
author: Jason Gowans
comments: true
date: 2013-02-15 07:39:19+00:00
layout: post
slug: hello-world
title: Hello World
---

A new year, a new job, and now, a new blog. When I was starting Harnu, I sometimes posted at [http://startupdad.blogspot.com/](http://startupdad.blogspot.com) . Now that I'm not involved with [Harnu](http://www.harnu.com) on a daily basis (perhaps a subject for a future post once the scabs heal), I wanted to have a space that wasn't tied to any particular company or job position. And so, here it is. On this blog I intend to write mostly about retail, analytics, digital marketing, and tech in general, with the occasional random post thrown in for good measure.

I'll close by mentioning that next week I start as Nordstrom's first Director of Advanced Analytics. It'll be scrappy, it'll involve lots of data discovery, and hopefully lots of high impact answers. Really couldn't be happier to dive in, learn a ton, and work with some great people.

Up and to the right!
