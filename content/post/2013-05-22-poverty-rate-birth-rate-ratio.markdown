---
author: Jason Gowans
comments: true
date: 2013-05-22 08:25:54+00:00
layout: post
slug: poverty-rate-birth-rate-ratio
title: 'Poverty Rate : Birth Rate Ratio '
wordpress_id: 221
tags:
- d3.js
- learning
- visualization
---

One of the things I like about my job is that everyone in the lab is always learning new skills. As a dad with limited time though, I often worry that my knowledge is stagnating while the rest of the team presses on. So, with a bit of time tonight, I took a stab at creating my first D3.js visualization, inspired by one of my co-workers. There's an excellent [blog post](http://bost.ocks.org/mike/map/) on how to get everything set up and make your first map. From there, I thought I'd try to create a choropleth similar to [this one](http://bl.ocks.org/mbostock/4060606).

What you are looking at below is still a work in progress, but it aims to show the ratio of Birth Rate to Poverty Rate for each county in the U.S. in 2011.  The data I mashed up is from the [US Dept of Agriculture's Economic Research Center](http://www.ers.usda.gov/data-products/county-level-data-sets/download-data.aspx#.UZx83iv45Ds). It's worth noting that there were 2 counties in the U.S. with no births and no poverty estimates. Those counties in question were Kalawao in the State of Hawaii (est. pop. 90) and Loving County in Texas (est. pop. 92). The county with the largest ratio of Poverty Rate to Birth Rate is [Crowley County in Colorado](http://en.wikipedia.org/wiki/Crowley_County,_Colorado) where its poverty rate of 48.1% is 93 times larger than its birth rate of ~0.5%. This could have something to do with Crowley County's distinction of having the highest percentage of incarcerated prisoners in any county in the U.S. See if you can spot it on the map below (hint: it's the darkest spot on the map).

[![Image](http://jasongowansdotnet.files.wordpress.com/2013/05/birthpoverty.png?w=487)](http://jasongowansdotnet.files.wordpress.com/2013/05/birthpoverty.png)

With a bit more time, I plan to finesse the color scale using the quantize scale technique [explained here](https://github.com/mbostock/d3/wiki/Quantitative-Scales#wiki-quantize). I found using d3.js to be far easier than building maps using [QGIS](http://www.qgis.org/), which is a tool I dabbled with in the past, but certainly never mastered. All in all, a fun way to spend the evening, albeit on a pretty sobering topic.
