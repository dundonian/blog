---
author: Jason Gowans
comments: true
date: 2012-07-31 18:25:00+00:00
layout: post
slug: foster-the-people
title: Foster the People
---

Last weekend, I spent time with an old friend who's the founder of a successful and growing primarily online specialty retailer with a smaller brick & mortar footprint.  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/07/33e34-img_1303.jpg)](http://jasongowansdotnet.files.wordpress.com/2012/07/img_1303.jpg)

After an evening of paddle-boarding around a peaceful bay in Puget Sound, we rustled up some dinner,  cracked open a bottle of plonk, and animated conversations ensued.  
  
With the inevitable catching up on how everyone was doing out of the way, the topic turned to retail and specifically Amazon's recent foray into [outdoor sports](http://www.geekwire.com/2012/amazoncom-outdoorsy-rei-job-posts-sports-site/) (the same space my friend works in). When asked if he was concerned about this development, my friend responded that, in his opinion, any retailer, whether they're online or offline, that hasn't gone to the trouble of building a community around their brand will die since there is no point of differentiation.  
  
Rather than fret about Amazon's encroachment into his space, he's betting that they'll clean it up and at one end of the spectrum, you'll have Amazon - the online behemoth vending outdoor equipment, and at the other end of the spectrum you'll have companies like his that have spent years building a brand that's 100% focused on a community of action sports enthusiasts. Everyone else who sits in the middle - neither an absolute lowest-cost provider nor a brand that people care about will be gone.  
  
I should point out that his assertion that people care about the experiential aspect of buying products does not come at the expense of eschewing the benefit of analytics. Rather, analytical prowess for any retailer is just table stakes these days. If you don't have that competency you don't exist. Simple as that.  
  
What we both agree though is that unless you're Walmart or Amazon, your point of differentiation has to center around your customers or users. So many retailers these days are struggling because they want to compete on price, they want to compete on local assortment, they want to compete on analytics, etc. The list goes on and on. In the end, they lose on all fronts against more disciplined competitors.  
  
Coming back to tech - the parallels are just as relevant for us as we build the Härnu brand. That algorithms will automate more of our lives is inevitable. Their utility in helping to remove friction is tremendous. However, while the chorus of geeks imagining a fully-automated world surrounded by contextually relevant data cues grows daily, I instead imagine something a bit more nuanced - a world where dispassionate algorithms give us what they've been programmed to think we want in some situations, and in other situations we'll seek out the messy, irrational, and fun experience of human-led interactions.
