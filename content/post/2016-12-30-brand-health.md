---
author: Jason Gowans
comments: true
date: 2016-12-30
layout: post
title: Measuring Brand Health in 2016
---

For the past three years, I've been perfectly spoiled working on personalization on Nordstrom.com. Everything we built could be easily A/B tested, and impact quantified and understood.

Now that I'm focused entirely on Marketing, I've found myself having to think not just about direct response tactics, but also longer term branding campaigns. Seeking to measure our "Brand Health," I naively thought we could just continuously measure this stuff, run some geo tests et voila, I'd come out the other end having some pithy quantitative perspective like, "...every 1% lift in brand health translates to a .1% lift in EBIT," or something to that effect.

Well, alas, measuring brand health in 2016 isn't quite so simple, at least as far as my research has thus far revealed. So, below are some thoughts on how to approach measuring brand health. Feedback welcomed!

## What should we measure?

Some might say that [Net Promoter Score (NPS)](https://www.netpromoter.com/know/) is the definitive way to measure brand health. Its simplicity to calculate has no doubt contributed to its mass adoption, but it only considers existing customers. I need to measure prospects as well as existing customers.

That brings us to consider other metrics like Brand Awareness. My mate, [Will McInnes](https://twitter.com/willmcinnes), and his team over at Brandwatch wrote a [good piece on measuring brand awareness](https://www.brandwatch.com/blog/marketing-how-to-measure-brand-awareness/). In the back of my mind, I was also thinking about the McKinsey piece on the [Consumer Decision Journey](http://www.mckinsey.com/business-functions/marketing-and-sales/our-insights/the-consumer-decision-journey). Building on this, and in conjunction with an excellent  book by [Byron Sharp](http://www.marketinglawsofgrowth.com/) of the [Ehrenberg-Bass Institute for Marketing Science](https://www.marketingscience.info/), called [How Brands Grow: Part II](https://global.oup.com/academic/product/how-brands-grow-part-2-9780195596267?cc=us&lang=en&), I started to think about a measurement framework.

First, some background though on How Brands Grow - The [law of double jeopardy](http://www.marketinglawsofgrowth.com/blog_files/3444403041d5fa8c9c07a9ae636b39b9-39.html) provides clear evidence that larger share brands have more customers who buy them more often than smaller share brands. Research also shows that a brand’s heaviest customers are already likely to be heavy buyers of the category - growing the existing base has only so much upside. Instead, major growth needs to come from increased market penetration and this strategy is dependent on building mental availability and physical availability, two constructs discusssed at length in How Brands Grow.

### Mental Availability

To be bought, a brand must first be thought of. The breadth (how many) and strength (how strong) of the brand’s links to relevant cues determine the chance of this happening. Cues come from common experiences that buyers in the category share. The cues are useful category entry points (CEPs). Attaching the brand to these memory structures will increase the chance the brand will come to mind in buying situations. An example of a CEP at Nordstrom might be, “Where the gifts are.”

### Building Physical Availability
There are three components of physical availability; 

* Presence - A retailer's stores and online and mobile offerings obviously provide a physical presence.
* Relevance - Is our brand buyable? Two tasks can help here: mapping category variants to check coverage across key buying occasions, and uncovering any barriers to purchase to make the brand easier to buy.
* Prominence – Is our brand easy to spot? Easy to buy also means being easy to find.

Putting it all together, I came up the following measurement framework:

## Brand Health Measurement Framework

[![](/images/brand-measurement-framework.png)](/images/brand-measurement-framework.png)

The thought here is that we should be measuring brand health in each phase of the consumer journey. 


### Initial Consideration

Just as with the [fragmentation of media](/post/mediafragmentation/), the exploding choice of retail shows that there is an inherent bias to go with already popular choices because a) it’s already part of most people’s retail repertoire, b) it offers a sufficiently high quality experience, and c) there’s a social aspect to doing things that others are doing that offers opportunity for conversation around common topics.

In our growth strategy, increasing market share relies on driving purchase behavior among non-buyers and light buyers. To increase market share, building mental availability starts with measuring unaided awareness. 

Next, for those that are aware of the retailer, we want to understand to what extent the consumer would consider buying from the retailer in question. 

From there, we can then explore additional measurements that go into more depth around our Category Entry Points (CEP) and their relation to awareness and consideration. (see pg 76 of How Brands Grow, Part II)

* Mental Market Share – Percentage of CEP associations of the total CEP associations for our brand and competitors.
* Mental Penetration – The percentage of category buyers who link the brand with at least one CEP.
* Network Size – How many CEPs the brand is linked to in the minds of those aware.

### Active Evaluation

In this stage, consumers are actively researching, and the number of brands under consideration may expand. Per the McKinsey study, two-thirds of activities involves the consumer pulling information such as reviews and word of mouth (WOM) recommendations.

People usually give WOM about brands they’ve experienced first hand and when it meets the following conditions: 1) have formed a confident opinion about the brand, 2) have seen the brand’s advertising, and 3) have had an unusual brand experience / story to share.

WOM levels tend to be highly correlated with market share. So, as we execute our growth strategy, we would expect to see WOM grow too. With that in mind, here are the measures we should be tracking in the Active Evaluation phase:

* Direct Load – A [recent survey](http://marketingland.com/amazon-is-the-starting-point-for-44-percent-of-consumers-searching-for-products-is-search-losing-then-145647) found that 44% of consumers start their product searches directly on Amazon, 34% start with traditional search engines, and 21% start on a retailer site. As a retailer grows its brand awareness, one would expect to see direct load increase.
* Search volume data – You can get indexed search data via Google Trends and other sources.
* Social Listening – [Per Brandwatch](https://www.brandwatch.com/blog/marketing-how-to-measure-brand-awareness/); volume of mentions, reach, engagement, and share of voice


### Moment of Purchase
Obviously a retailer would look at the usual operating metrics such as demand, conversion, and average order value. However, with respect to the brand, this is our opportunity understand customer perception along two dimensions. The first, is the functional aspects of a retailer's core offering; price, selection, convenience, experience, etc. The emotional dimension would attempt to measure how much consumers actually like shopping with you.

From that, we can then seek to estimate an expected market share based on how much consumers like the experience of shopping with a retailer compared to our actual market share. We'd then end up with something like the following 2 x 2 matrix:

[![](/images/brand-2x2.png)](/images/brand-2x2.png)

### Post Purchase

If the customer has had a positive experience with the brand, we would expect to see growing engagement and advocacy. As such, we could be measuring the following:

* Engagement - You'd ideally want to see repeat buyers grow over time.
* App downloads – If a retailer has a mobile app, a more engaged customer base should show growth of app downloads as a percentage of total customers.
* Loyalty – If the retailer has a loyalty program, are the number of rewards sign ups as a percentage of first time purchases growing over time?
* Net Promoter Score

## How Should We Measure?

So, if those are the metrics, what options do we have for actually measuring all of those KPIs? Forrester wrote a paper earlier this year called, [The Emerging Revolution In Brand And Experience Measurement](https://www.forrester.com/go?objectid=RES101701). Forrester doesn't permit sharing the article, but they do note that, "The brand measurement vendor landscape has polarized between traditional, strategic brand advisors and increasingly sophisticated social listening providers."

In the brand advisory category, they further delineate between generalists and specialists. Among the generalists are GfK, Interbrand, Ipsos, Milward Brown, and Landor Associates. In the specialist camp are firms such as BAV Consulting, Brand Keys, Lippincott, Prophet, and Siegel+Gale.

In the social listening category, they list traditional players (their terminology), including Brandwatch, Sprinklr, and Netbase. The sub-category of branding experts (again, Forrester's terminology) includes Networked Insights and Brand VO2.

Not sure why, but Forrester neglected to mention a few other options like [Google](https://www.google.com/analytics/surveys), [SurveyMonkey](https://www.surveymonkey.com/mp/audience/), and [YouGov](http://www.brandindex.com/). An ideal provider would be one that;

* Permits continuous measurement of the KPIs with ability to easily slice and dice across a number of dimensions.
* Provides ability to set multiple screening questions e.g. income, gender, age, etc. 
* Enables in-depth studies / profiles around the functional and emotional aspects of the brand.
* Has a developer API to pull down the raw data programmatically.
* Allows survey respondent data to flow into a [Data Management Platform (DMP)](http://digiday.com/platforms/what-is-a-dmp-data-management-platform/) where I can then do further modeling and targeting.

I have my own thoughts on which provider comes closest to checking all the boxes, but I'll leave it to you to decide what works best for you.







