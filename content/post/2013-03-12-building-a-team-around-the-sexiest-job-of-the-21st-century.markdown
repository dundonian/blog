---
author: Jason Gowans
comments: true
date: 2013-03-12 16:07:58+00:00
layout: post
slug: building-a-team-around-the-sexiest-job-of-the-21st-century
title: Building a Team Around The Sexiest Job of the 21st Century
wordpress_id: 79
tags:
- data science
- data scientist
- recruiting
---

Building a data science team isn't easy, particularly when the team you're creating will largely be comprised of what the Harvard Business Review calls "[The Sexiest Job of the 21st Century](http://hbr.org/2012/10/data-scientist-the-sexiest-job-of-the-21st-century/ar/1)." LinkedIn currently reveals 747 open jobs for "Data Scientist," 38 of which are in the Greater Seattle area. Unless you're a pre-IPO sure-thing startup, those are not great odds for finding the right people. So, how can a 112 year old fashion retailer improve those odds?

Well, it's worth remembering that [Hope Is Not a Strategy](http://www.amazon.com/Hope-Not-Strategy-Winning-Complex/dp/0071418717) as a former colleague at MicroStrategy used to remind me. Whether you want to close a sale or build a team, you have to hustle. So, after I came back from the recent [Strata Conference](http://strataconf.com/strata2013/public/content/home), I contacted some of the speakers whose talks I found interesting, asking them if they might know anyone interested in what we're doing. The thought was simply that if I admired the work they were doing, chances are, they might know people in their circles of similar skill.

Of the 5 people I contacted, one gave me a polite but firm rejection on account of the fact that we're attempting to hire similarly skilled people in a somewhat adjacent space. One gave me a referral, another said he didn't know anyone, the fourth ignored me entirely, and the fifth mistook me for a recruiter thinking I was trying to hire him, and made fun of it on Twitter!

https://twitter.com/gerbille/status/310273351912402944



So much for the hustle! However, setting aside the ignominy of being the object of a data scientist's ridicule, a 20% referral rate has made this exercise worth it in my book so far. I'd love to hear how others are finding success in building their teams, and if you're a data scientist reading this post, save your ridicule until after you've met me ;-)
