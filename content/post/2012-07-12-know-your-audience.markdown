---
author: Jason Gowans
comments: true
date: 2012-07-12 08:18:00+00:00
layout: post
slug: know-your-audience
title: Know Your Audience
---

As the team cranks hard on creating the next version of [Härnu](http://www.harnu.com/), I decided to spend my time researching the world of, well, world affairs bloggers. Specifically, I researched those that write for [Global Voices](http://globalvoicesonline.org/) since they're exactly the kind of people that we hope will give [Härnu ](http://www.harnu.com/)a whirl once we start featuring their content.  
  
  
I thoroughly enjoyed researching who these people are, where they're from, what they write about, and what they do. Some of course stood out more than others, particularly a guy called [Hossein Derakshan](http://en.wikipedia.org/wiki/Hossein_Derakhshan) whom the Iranian authorities jailed for 19.5 years simply for writing about his country. I was ashamed I'd never heard of him until now and I'm asking myself why the world isn't doing more to fight for his release. I'm also asking myself what a regular person like me could do to help. If you have any ideas, do please let me know.  
  
Aside from Hossein, there were also a number of bloggers that back in '05 or '06 were just getting their start on Global Voices and have since grown into full-fledged celebrities such as the wildly popular Saudi comedian [Fahad Albutairi](https://twitter.com/Fahad) as well as notable CEOs such as [Rashmi Sinha](https://twitter.com/rashmi) - founder of Slideshare, [Ory Okolloh](https://twitter.com/kenyanpundit) - founder of Ushahidi, and many other successful entrepreneurs. Make no mistake, these people are smart, articulate, connected, and highly successful in their chosen field of endeavor.  
  
However, the pleasure in reading about them wasn't restricted to just celebs and entrepreneurs. Instead, it was the unexpected that caught my attention - a lovely [YouTube video](http://youtu.be/Uj-1_lao7ug) of a Hungarian band courtesy of a blogger called [Marietta](http://lemarietta.wordpress.com/), the discovery of the word "homophily" courtesy of a blogger called [Dan Wescott](http://itsnotalecture.blogspot.com/), and countless other great nuggets of life I'd never have found any other way.  
  
So, who are these amazing people that are literally changing the world? Well, to start poking around that question, I compiled a list of 593 bloggers from the [author section](http://globalvoicesonline.org/author) of the Global Voices site. I omitted those whose primary role seemed to be translators simply because I was most interested in the writers. I also probably missed a few here and there, so this crude analysis is by no means infallible. I then searched for their own personal blogs and Twitter accounts if they had them (and I was actually able to find them). All told, I spent about a lot more time than I'd planned on this little diversion but felt it was important to get more familiar with this community.  
  
And here are some of the factoids I uncovered:  
  


  * As near as I can tell, Global Voices has ~134 countries covered though it's a really long tail. What I mean by that is that there are 12 countries with 10 or more bloggers, 102 countries have 5 or less bloggers and fully 38 countries have just 1 blogger.
  * Like most [UGC ](http://en.wikipedia.org/wiki/User-generated_content)sites, the [Pareto Principle](http://en.wikipedia.org/wiki/Pareto_principle) applies and then some. Of the ~76K posts, 56K of them come from just 20 bloggers. Said differently, 74% of the content comes from 3% of the bloggers. (Would be interesting to know if Global Voices themselves agrees / disagrees with that finding. Where there may be an issue is if a blogger has a dual role of author & translator and I've credited them for all of those posts...)
  * The authors have an average (median) of 8 posts published. 
  * Their average (median) tenure on Global Voices defined as their last post minus their join date is basically 1 year (342 days if you care about details). 
  * The average (median) post frequency is 24 days per post. In other words, they are blogging about once every 3.5 weeks on Global Voices. However a cursory glance of their own personal blogs suggest they write much more frequently than that. 
  * Global Voices seems to be adding about 85 - 95 new bloggers each year.
  * So far in 2012, 217 different bloggers have posted on Global Voices - about 37% of all bloggers I researched.
  * Of 593 bloggers, I was able to find a personal blog for 61% of them which suggests I need to improve my Googling skills...
  * Same thing re: Twitter - 65% of them have a Twitter account that I could find with an average (median) following of 993.
  * Of those with a Twitter account, 10% have a follower count > 10,000.
  * The most followed blogger is a lady called [Beth Kanter](https://twitter.com/KANTER), with [Fahad Albutairi](https://twitter.com/Fahad) in 2nd spot.

  


While the stats may be mildly interesting to some, what's most important is that the bloggers are passionate, they are writing, and their content is being read. My hope is that as we begin featuring their content on [Härnu](http://www.harnu.com/), they'll welcome the increased engagement from our users and our users will welcome the alternative perspectives from the bloggers.

  

