---
author: Jason Gowans
comments: true
date: 2013-09-11 04:22:09+00:00
layout: post
slug: why-twitter-is-a-big-deal-for-advertisers
title: Why Twitter Is A Big Deal For Advertisers
wordpress_id: 697
tags:
- retail
- social
---

Tonight I read a [fantastic summary](https://medium.com/on-startups/e3ffbf8f3cd8) by [Antonio Garcia](https://twitter.com/antoniogm) on where Twitter is going with ads and why the [MoPub](http://www.mopub.com/) deal is a game-changer. While I whole-heartedly agree with his assertion that,


<blockquote>"...Twitter is making the big, bold bets on the future, while Facebook is playing internal politics, and wallowing in their incumbent position.",</blockquote>


I couldn't help but think about the advertiser side of the equation, and particularly multi-channel retailers.

In the scenario where,


<blockquote>"...knowing that the same person who is browsing for a pair of shoes on Zappos.com on their work computer is the same person who’s whiling away a few minutes on their iPhone later that day while waiting for a friend. And also knowing it’s that same person who’s checking Twitter on their iPad when they get home that night.",</blockquote>


multi-channel retailers can track all of those experiences too. They've got the web site, they've got an iPhone app, they've got an iPad app, and they've got brick and mortar stores, and some of them have the wherewithal to stitch together all of those experiences.

Further, using social media data providers, those multi-channel retailers can get access to the Twitter firehose and also have the potential to know,


<blockquote>"whom you’ve followed, what you’ve Tweeted, as well as what pages on the Web you’ve browsed.",</blockquote>


and can just as readily construct an interest graph with the added bonus of being able to join it to customer purchase data and the intent data expressed through customer interactions on any one of the retailer's channels.

So, why does this matter? Well, in a RTB world where,


<blockquote>"if Twitter can find that person for you while they’re playing Candy Crush Saga, that changes everything. Suddenly that Candy Crush Saga ad impression goes from some bargain basement $0.20 per thousand ad impressions to $20, you have your lead again, and the entire mobile ads world changes.",</blockquote>


an advertiser would rather pay closer to $0.20 than $20 and that's where savvy retailers can try to come at this from the beginning position of knowing everything their customers have bought and browsed on their properties and attempting to append the social layer as much as possible themselves. Doing so has the added benefit of not only using the data to bid on specific user and interest targeted campaigns on Twitter and elsewhere, but can also be used to further personalize the customer experience with the retailer across its channels.

In the race for the major players such as Google, Facebook, and Twitter to create a persistent identity layer across all digital experiences, it perhaps goes mostly unnoticed that many advertisers are working quietly to do the same for their own customers. While it may have been true a few years ago that,


<blockquote>"...there’s a tiny minority of companies who can even do this, but Twitter is one of them (Google, Apple, Amazon, and Facebook are the others).",</blockquote>


the world of social media has evolved enough now that many advertisers can see past the vanity metrics of followers and likes, and are at least making headway on attaining the same targeting capabilities that forward-looking companies like Twitter are.
