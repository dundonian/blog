---
author: Jason Gowans
comments: true
date: 2012-07-24 18:54:00+00:00
layout: post
slug: the-war-of-art
title: The War of Art
---

I spent much of last week skulking around Seattle area coffee shops intending to be productive, trying to be productive, willing myself to be productive but mostly failing. Our much anticipated (at least among us founders) new version of [Härnu](http://www.harnu.com/) is inching closer to production and instead of unbridled excitement, I was mostly being consumed by fear. Specifically, fear of failure. That thing that I've allowed to hold me back for so long crept into my sphere of optimism and paralyzed my ability to think and work unhindered.  
  
  
I found myself accepting defeat - that the next version will fail, that the idea is iffy, that entrenched incumbents offer a good enough alternative, that I'm not competent enough etc. The list goes on and on.  
  
Talking to people about it didn't help. The natural reaction of loved ones is to help you find a solution - try this..., have you thought about..?, what about...? The self-pitying response from me was always that yes, I have thought of that and here's 10 reasons why that won't work.  
  
Breaking out of these despondent episodes can be tough but I don't think they should be ignored entirely. Rather than roll over, I spent the time trying to get at the core of my angst, and asking myself what the alternatives are if indeed things don't go as we hope. The [Lean Startup](http://theleanstartup.com/) principles have an answer for all of this, but in among the dispassionate multi-variate A/B tests and subsequent pivots are imperfect, emotional, and sometimes irrational people that have laid it all on the line to build something of their own and [find meaning](http://pandodaily.com/2012/07/24/the-search-for-meaningful-work/).  
  
Steven Pressfield wrote a book called the [War of Art: Winning the Inner Creative Battle](http://www.amazon.com/War-Art-Through-Creative-Battles/dp/0446691437/) for those trying to deal with what he calls the Resistance. We may be trying to build a company, but it has always been a product of our imagination, and its future depends on our creativity. For Pressfield, the answer to combatting the resistance is to do the work, and that's exactly what I intend to do.  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/07/d248e-galland.jpeg)](http://jasongowansdotnet.files.wordpress.com/2012/07/galland.jpeg)

So, here we are - another week, a new version rolling out to production very shortly, a set of plausible alternatives, AND a desk in a downtown Seattle office gratis courtesy of my next-door neighbor who's a co-founder of a [successful startup](http://flowroute.com/) and took pity on the old guy shooting for the moon.  
  
No more morose coffee shop music to get me down - an all-night bug-bash awaits. It's [time to dance](http://open.spotify.com/track/58fuoKn0L1RxCgVQid6wAn) ;-)
