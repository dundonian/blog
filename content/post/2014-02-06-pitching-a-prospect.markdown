---
author: Jason Gowans
comments: true
date: 2014-02-06 07:55:22+00:00
layout: post
slug: pitching-a-prospect
title: Pitching A Prospect
wordpress_id: 960
---

For the past year, I've been in the fortunate position where many companies - startups, consultants, and enterprise software companies alike would like to do business with my employer. As such, I've had the chance to meet a lot of interesting people, and learn about lots of innovation in the so-called "Big Data" space.

However, for much of my career, I sat on the side of the software vendor, aiming to sell our solution in to prospective customers. Now that I'm on the opposite side, I'm generally exposed to three types of pitches from prospective vendors;



	
  1. The easy mark

	
  2. The fishing expedition

	
  3. The professional


The first type of pitch is where the salesperson hasn't done their homework, and they perceive the prospective customer as an easy mark. That is, their assumed starting point is that the prospective customer is uninformed, unsophisticated, and incapable of innovating. In these types of pitches, the salesperson asks next to no questions and is fixated only on getting through their deck and / or demo, which usually begins with a ridiculously simplified starting point, and a not insignificant amount of arrogant hand-waving. They're so into their pitch that they don't take the time to read the room, ask questions, and adjust the tone and / or depth of content to tailor the audience.

The second type of pitch is really not much of a pitch at all. In this scenario, the salesperson has (usually) a cursory understanding of the product they're selling and as soon as they encounter anything resembling an objection or comment that deviates from their expected script, they embark on something that reminds you of the jilted partner who refuses to accept you've broken up with them. That is, the conversation turns into them trying too hard to be what they think you want / need. In these situations, better to switch gears and use this as a valuable data point by acknowledging there appears to not be an opportunity and instead asking relevant questions that can better prepare you for future prospect conversations.

The last type of pitch is, for lack of a better term, the professional. They know their product or service, they know your industry and company (and perhaps your area of responsibility), and most importantly, they're confident enough in their read of the situation that sometimes the answer is no (and they'll even initiate that realization), and that's ok. They're not about to waste their own time chasing a hopeless cause. If there's an opportunity, then let's talk some more. If not, let's do each other a favor and acknowledge that fact.

The last type of salesperson is one that usually gets folks' respect, if not necessarily always the business. So, before you get ready to pitch your prospect, ask yourself what kind of salesperson are you? Are you gearing up to treat your prospect as an easy mark? Are you about to walk in there on an undirected fishing expedition? Or, are you prepared to do your homework ahead of time, bring forth an actual solution (as opposed to a piece of software), and actively listen to what your prospect is communicating?
