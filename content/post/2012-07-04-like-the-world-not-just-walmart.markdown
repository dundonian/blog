---
author: Jason Gowans
comments: true
date: 2012-07-04 17:19:00+00:00
layout: post
slug: like-the-world-not-just-walmart
title: Like the World, Not Just Walmart
---

Foreign news reporting is on the decline - that's no secret. As traditional media has struggled to adapt to the online world, the reaction has been largely to cut costs instead of innovating. For example, according to a recent [AJR study](http://www.ajr.org/article.asp?id=4985) 18 major U.S. newspapers and 2 chains have completely shut all their foreign news bureaus since 1998 – newspapers such as the Boston Globe and the Chicago Tribune are prime examples. Foreign news now accounts for [less than 5%](http://www.ajr.org/article.asp?id=4998) of all news found in a typical newspaper and this trend is not limited to American media - it's also happening in places such as [the U.K.](http://www.guardian.co.uk/media/organgrinder/2010/nov/01/world-news-foreign) where foreign news reporting has declined by 40% over the past three decades.  
  
**Declining Interest in Foreign News**  
As foreign news reporting has declined, so too has American interest in foreign affairs. According to a recent [Pew Center study](http://www.people-press.org/2012/06/06/interest-in-foreign-news-declines/), only 17% of Americans are following the economic crisis in Europe and only 12% are following events in Syria - about the same percentage that were following the John Edwards trial. This is compared to 2011, when 39% of Americans were following the situation in Egypt.  
  
**Massive Investment in Hyper-Local News**  
It'd be easy to say that foreign news has declined while local news has flourished, and indeed, a lot of investment has been made in that direction, but that too appears to be a [fools errand](http://www.businessweek.com/articles/2012-05-31/aols-patch-big-losses-on-hyperlocal-news) as hyper-local display advertising has declined, likely attributable to more accountable alternatives like Groupon and LivingSocial as well as the ubiquitous Google AdWords and now Facebook advertising and a whole host of "so-lo-mo" apps and services targeting the hyper-local market.  
  
**Internet Growth and Citizen Reporting in Developing Nations**  
With no sign that traditional media is going to pull out of this death spiral anytime soon, it'll be left to citizens to fill that void. Sites like [Global Voices](http://globalvoicesonline.org/) have risen to the challenge of reporting on the world and that reporting is going to get a whole lot more amplified when 2B new Internet users join the connected society, all from developing countries in Africa, Asia, and S. America. At the moment, there is [more online content about Germany than Africa and S. America combined](http://www.guardian.co.uk/global-development/poverty-matters/2012/jan/09/networked-world-geography-of-information-uneven?CMP=twt_gu)! That is all about to change rapidly, especially when you understand the implication of [high-speed cable](http://upload.wikimedia.org/wikipedia/commons/d/d4/Cable_map18.svg) being laid all over Africa.  
  
**New Age of Discovery**  
So, at a time when major Western nations such as the U.S. and the U.K. are becoming more insular, not by choice, but instead thanks to declining foreign news reporting and social networks that don't encourage us to seek out alternative perspectives beyond our friends, we have the rest of the world coming online and embarking on a new digital [Age of Discovery](http://en.wikipedia.org/wiki/Age_of_Discovery) - one that will be noted for enlightened bi-directional sharing, collaboration, and learning instead of the previous age of selfish nation-state exploitation.   
  
The question is, will Americans be an integral part of this new Age of Discovery, or will we be content to spend our time checking in to the local coffee shop, getting on the free wi-fi, checking our friends' latest status, and liking Walmart (as 17MM Americans apparently do)?
