---
author: Jason Gowans
comments: true
date: 2012-08-17 05:14:00+00:00
layout: post
slug: ride-like-everyone-is-trying-to-kill-you
title: Ride Like Everyone is Trying to Kill You
---

[![](http://jasongowansdotnet.files.wordpress.com/2012/08/6f61b-ducati018.jpeg)](http://jasongowansdotnet.files.wordpress.com/2012/08/ducati018.jpeg)

Until we had kids, I used to get around on a motorbike - it was a lot cheaper & convenient than a car, although in Seattle, it was also a lot wetter :-) For a while, I had my dream ride - a Ducati Multistrada. It was an incredible machine and I absolutely loved that bike. However, as a motorbike rider, I followed two simple rules; 1) Never impair my senses by mixing alcohol and riding, and 2) Ride like everyone is trying to kill you.  
  
Whether it was dumb luck or adherence to those two cardinal rules, I fortunately never had an accident. And so it goes with startups. We could get knocked off our metaphorical bike at any time by a wayward driver in a big SUV. More likely though, it's not a large competitor that'll kill us, it's that no-one gives a shit. At this stage, we have no competitors - we may as well not exist. My cardinal rule for this startup is that no-one cares - we're invisible and could get killed at any second.  
  
When you're bootstrapping and you're a first time entrepreneur, you're flying under the radar. There are no celebrity launches, no tech press fawning over your insanely cool idea, no Twitterati re-tweeting your every brain fart acting like your the prophet incarnate, and no legion of third-party developers queuing up to partner with you. In fact, even people that have been friends with you forever just don't give a shit. That's not indicative of the value of your friendships. It's just a hard reality of life. Everyone's got their own stuff going on, and if your product isn't good enough, it's not going to break through the noise. Period. Even if it is good enough, it's not going to break through the noise of 90% of the people that encounter it for the first time.  
  
So, you push your product out there and all the academic conversations you've had with your co-founders about your startup having the potential to change the world, get x million users, or being worth this that and the other have just had a cold dose of reality poured all over them. Now what? You start executing your user acquisition plan and it's not panning out like you had computed on your spreadsheet. Now what? You make tweaks to the product based on what limited data you have and maybe a few anecdotal conversations with respected friends that tried it out. Still the mythical hockey stick eludes you. Now what? You attend some startup incubator's talk on Mechanical Turk thinking that might be what'll get you there but that turns out to not be the silver bullet you seek either. Now what? I'll tell you what...  
  
You grind it out.  
  
Plain and simple. Don't bitch and whine about not being afforded the benefit of the doubt. You don't deserve it. Everything you've accomplished in your professional life until this point is in the past. Value you've created elsewhere is not transferable to the startup that you just helped hatch.  
  
Fortunately for me, this is the game I know. Always been the underdog. My belief isn't unshakable but I know how to hustle and that's what we're doing.  
  
You grind it out.  
  
What does that even mean? Well, that means you get to know your target audience as well as you know your wife / husband / partner, and you do your damnedest to convince them that you've got something special. At this point, you can't fake it. You gotta believe that yourself. You email, you call, you Skype, you blog, you comment, you post, you tweet, you strike up a conversation with everyone you meet on the off chance that they might become a convert. That's what it takes.  
  
My co-founder relayed a conversation he had with Andy Liu (he of BuddyTV fame) where Andy told him that at this stage of most consumer startups, there are three options available on the path to minimum viable community (his magical number is 10,000 users).  
  


  1. You Quit - you didn't realize it'd be this tough and you doubt you can get there.
  2. You Panic - you didn't realize it'd be this tough and you try to build your way to 10K users by tweaking the product over and over and over without any sort of meaningful data to back up your bets.
  3. You Work - it's war. Hand-to-hand combat. You work all hours to get everybody you possibly can to try your product and to get them back on it. It takes asking the same people over and over and over again to give it a go.

So, here we are. A week into the redesigned Härnu and the highlight of my day was talking to a blogger / journalist from Brazil on Skype and giving her a demo of [Härnu](http://www.harnu.com/). It was awesome and I wouldn't have it any other way.  
  
Ride like everyone is trying to kill you and take the time to see the situation for what it is - even on your worst days, you're still riding a Ducati. Life ain't that bad...
