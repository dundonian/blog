---
author: Jason Gowans
comments: true
date: 2012-11-14 00:09:00+00:00
layout: post
slug: keep-calm-and-carry-on
title: Keep Calm and Carry On
---

Well, like most things, if I don't maintain a regular schedule, it just doesn't get done. And so it goes with this blog. I had resolved to jot down the lessons along the way. Sometimes though, the lessons are hard to comprehend. To do so requires a period of quiet reflection, and frankly, when you're driven by fear such as I am, any pause to reflect is a chance for my fears to catch up. So, I keep going - moving forward to maintain a sense of progress, regardless of whether that progress is real or imagined.  
  
However, a real leader is not driven by fear. **A real leader is driven by a sense of purpose**. While people driven by fear can accomplish much in their life, their fear holds them back from accomplishing their true potential.  
  
At Härnu, we've managed to gain members from more than 100 countries in a couple of months, we've radically improved the product, and we're ready to take the company to the next level. Right now, we're in the middle of sorting out proper roles among the founders and adjusting the cap table accordingly.  
  
It's a stressful time but I'm sure glad we have a leader on our team who's taking on the role of CEO and has been there and done that. As for me? The answer is clear...  
  


[![](http://jasongowansdotnet.files.wordpress.com/2012/11/ef70d-keep-calm-and-carry-on-original.jpg)](http://jasongowansdotnet.files.wordpress.com/2012/11/keep-calm-and-carry-on-original.jpg)

  

