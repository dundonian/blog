+++

date = "2016-12-28T12:40:11-08:00"
title = "About Me"
hidefromhome = true

+++

I'm Jason Gowans, and I'm originally from Dundee, Scotland (fun fact - did you know people from Dundee are called [Dundonians](https://en.wiktionary.org/wiki/Dundonian)?). I've lived in Seattle since my early twenties and, together with my wife Anna, we live in Queen Anne with our three kids; Maja, Janek, and Kajetan.

I started my career in Sweden as a COBOL programmer :-), and have loved being part of the growth of the Internet from my early days doing web development, to numerous side projects, to now, where I work at [Nordstrom](//nordstrom.com) as Vice President of Marketing Analytics & Technology.

One thing I'm especially proud of is [Aerobatic](http://www.aerobatic.com/), a side project that we bootstrapped into a business with real paying customers from around the world :-) I helped start Aerobatic alongside the most talented and humble developer I've ever worked with, [David Von Lehman](//twitter.com/davidvlsea). 

If you want to know more about my career, you can check out my [LinkedIn profile](//www.linkedin.com/in/jasongowans).

On a personal front, I'm a lifelong [Dundee United](http://dufc.co/) fan and still play soccer, albeit at a bit more sedate pace than I once used to. Also love running (I originally came to the U.S. thanks to a track scholarship to [Nicholls State University](https://www.nicholls.edu/). Most weekends, the family and me can be found up at [Snoqualmie Pass](http://www.summitatsnoqualmie.com/), mountain biking in the Summer and skiing in the Winter. One recent indulgence is [crossfit](crossfitinterbay.com) that I try to do at 5am a few days a week provided my body doesn't protest too much!

Feel free to get in touch with me via [Twitter](//twitter.com/jasongowans). 

Cheers!
/jason
