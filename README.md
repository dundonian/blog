#Jason Gowans personal blog

This is the repo for my blog at [jasongowans.net](http://www.jasongowans.net). It's generated using Hugo and hosted on Bitbucket using the Aerobatic add-on for Bitbucket. This repo also uses Bitbucket pipelines.